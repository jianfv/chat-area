!function () {
    function e(e, t, i) {
        return (t = o(t)) in e ? Object.defineProperty(e, t, {
            value: i,
            enumerable: !0,
            configurable: !0,
            writable: !0
        }) : e[t] = i, e
    }

    function t() {
        "use strict";/*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
        t = function () {
            return i
        };
        var e, i = {}, n = Object.prototype, r = n.hasOwnProperty, a = Object.defineProperty || function (e, t, i) {
                e[t] = i.value
            }, s = "function" == typeof Symbol ? Symbol : {}, o = s.iterator || "@@iterator",
            c = s.asyncIterator || "@@asyncIterator", l = s.toStringTag || "@@toStringTag";

        function u(e, t, i) {
            return Object.defineProperty(e, t, {value: i, enumerable: !0, configurable: !0, writable: !0}), e[t]
        }

        try {
            u({}, "")
        } catch (e) {
            u = function (e, t, i) {
                return e[t] = i
            }
        }

        function d(e, t, i, n) {
            var r = t && t.prototype instanceof y ? t : y, s = Object.create(r.prototype), o = new O(n || []);
            return a(s, "_invoke", {value: L(e, i, o)}), s
        }

        function p(e, t, i) {
            try {
                return {type: "normal", arg: e.call(t, i)}
            } catch (e) {
                return {type: "throw", arg: e}
            }
        }

        i.wrap = d;
        var m = "suspendedStart", g = "suspendedYield", f = "executing", v = "completed", E = {};

        function y() {
        }

        function x() {
        }

        function b() {
        }

        var k = {};
        u(k, o, (function () {
            return this
        }));
        var T = Object.getPrototypeOf, C = T && T(T(S([])));
        C && C !== n && r.call(C, o) && (k = C);
        var D = b.prototype = y.prototype = Object.create(k);

        function w(e) {
            ["next", "throw", "return"].forEach((function (t) {
                u(e, t, (function (e) {
                    return this._invoke(t, e)
                }))
            }))
        }

        function N(e, t) {
            function i(n, a, s, o) {
                var c = p(e[n], e, a);
                if ("throw" !== c.type) {
                    var l = c.arg, u = l.value;
                    return u && "object" == h(u) && r.call(u, "__await") ? t.resolve(u.__await).then((function (e) {
                        i("next", e, s, o)
                    }), (function (e) {
                        i("throw", e, s, o)
                    })) : t.resolve(u).then((function (e) {
                        l.value = e, s(l)
                    }), (function (e) {
                        return i("throw", e, s, o)
                    }))
                }
                o(c.arg)
            }

            var n;
            a(this, "_invoke", {
                value: function (e, r) {
                    function a() {
                        return new t((function (t, n) {
                            i(e, r, t, n)
                        }))
                    }

                    return n = n ? n.then(a, a) : a()
                }
            })
        }

        function L(t, i, n) {
            var r = m;
            return function (a, s) {
                if (r === f) throw Error("Generator is already running");
                if (r === v) {
                    if ("throw" === a) throw s;
                    return {value: e, done: !0}
                }
                for (n.method = a, n.arg = s; ;) {
                    var o = n.delegate;
                    if (o) {
                        var c = A(o, n);
                        if (c) {
                            if (c === E) continue;
                            return c
                        }
                    }
                    if ("next" === n.method) n.sent = n._sent = n.arg; else if ("throw" === n.method) {
                        if (r === m) throw r = v, n.arg;
                        n.dispatchException(n.arg)
                    } else "return" === n.method && n.abrupt("return", n.arg);
                    r = f;
                    var l = p(t, i, n);
                    if ("normal" === l.type) {
                        if (r = n.done ? v : g, l.arg === E) continue;
                        return {value: l.arg, done: n.done}
                    }
                    "throw" === l.type && (r = v, n.method = "throw", n.arg = l.arg)
                }
            }
        }

        function A(t, i) {
            var n = i.method, r = t.iterator[n];
            if (r === e) return i.delegate = null, "throw" === n && t.iterator.return && (i.method = "return", i.arg = e, A(t, i), "throw" === i.method) || "return" !== n && (i.method = "throw", i.arg = new TypeError("The iterator does not provide a '" + n + "' method")), E;
            var a = p(r, t.iterator, i.arg);
            if ("throw" === a.type) return i.method = "throw", i.arg = a.arg, i.delegate = null, E;
            var s = a.arg;
            return s ? s.done ? (i[t.resultName] = s.value, i.next = t.nextLoc, "return" !== i.method && (i.method = "next", i.arg = e), i.delegate = null, E) : s : (i.method = "throw", i.arg = new TypeError("iterator result is not an object"), i.delegate = null, E)
        }

        function M(e) {
            var t = {tryLoc: e[0]};
            1 in e && (t.catchLoc = e[1]), 2 in e && (t.finallyLoc = e[2], t.afterLoc = e[3]), this.tryEntries.push(t)
        }

        function I(e) {
            var t = e.completion || {};
            t.type = "normal", delete t.arg, e.completion = t
        }

        function O(e) {
            this.tryEntries = [{tryLoc: "root"}], e.forEach(M, this), this.reset(!0)
        }

        function S(t) {
            if (t || "" === t) {
                var i = t[o];
                if (i) return i.call(t);
                if ("function" == typeof t.next) return t;
                if (!isNaN(t.length)) {
                    var n = -1, a = function i() {
                        for (; ++n < t.length;) if (r.call(t, n)) return i.value = t[n], i.done = !1, i;
                        return i.value = e, i.done = !0, i
                    };
                    return a.next = a
                }
            }
            throw new TypeError(h(t) + " is not iterable")
        }

        return x.prototype = b, a(D, "constructor", {value: b, configurable: !0}), a(b, "constructor", {
            value: x,
            configurable: !0
        }), x.displayName = u(b, l, "GeneratorFunction"), i.isGeneratorFunction = function (e) {
            var t = "function" == typeof e && e.constructor;
            return !!t && (t === x || "GeneratorFunction" === (t.displayName || t.name))
        }, i.mark = function (e) {
            return Object.setPrototypeOf ? Object.setPrototypeOf(e, b) : (e.__proto__ = b, u(e, l, "GeneratorFunction")), e.prototype = Object.create(D), e
        }, i.awrap = function (e) {
            return {__await: e}
        }, w(N.prototype), u(N.prototype, c, (function () {
            return this
        })), i.AsyncIterator = N, i.async = function (e, t, n, r, a) {
            void 0 === a && (a = Promise);
            var s = new N(d(e, t, n, r), a);
            return i.isGeneratorFunction(t) ? s : s.next().then((function (e) {
                return e.done ? e.value : s.next()
            }))
        }, w(D), u(D, l, "Generator"), u(D, o, (function () {
            return this
        })), u(D, "toString", (function () {
            return "[object Generator]"
        })), i.keys = function (e) {
            var t = Object(e), i = [];
            for (var n in t) i.push(n);
            return i.reverse(), function e() {
                for (; i.length;) {
                    var n = i.pop();
                    if (n in t) return e.value = n, e.done = !1, e
                }
                return e.done = !0, e
            }
        }, i.values = S, O.prototype = {
            constructor: O, reset: function (t) {
                if (this.prev = 0, this.next = 0, this.sent = this._sent = e, this.done = !1, this.delegate = null, this.method = "next", this.arg = e, this.tryEntries.forEach(I), !t) for (var i in this) "t" === i.charAt(0) && r.call(this, i) && !isNaN(+i.slice(1)) && (this[i] = e)
            }, stop: function () {
                this.done = !0;
                var e = this.tryEntries[0].completion;
                if ("throw" === e.type) throw e.arg;
                return this.rval
            }, dispatchException: function (t) {
                if (this.done) throw t;
                var i = this;

                function n(n, r) {
                    return o.type = "throw", o.arg = t, i.next = n, r && (i.method = "next", i.arg = e), !!r
                }

                for (var a = this.tryEntries.length - 1; a >= 0; --a) {
                    var s = this.tryEntries[a], o = s.completion;
                    if ("root" === s.tryLoc) return n("end");
                    if (s.tryLoc <= this.prev) {
                        var c = r.call(s, "catchLoc"), l = r.call(s, "finallyLoc");
                        if (c && l) {
                            if (this.prev < s.catchLoc) return n(s.catchLoc, !0);
                            if (this.prev < s.finallyLoc) return n(s.finallyLoc)
                        } else if (c) {
                            if (this.prev < s.catchLoc) return n(s.catchLoc, !0)
                        } else {
                            if (!l) throw Error("try statement without catch or finally");
                            if (this.prev < s.finallyLoc) return n(s.finallyLoc)
                        }
                    }
                }
            }, abrupt: function (e, t) {
                for (var i = this.tryEntries.length - 1; i >= 0; --i) {
                    var n = this.tryEntries[i];
                    if (n.tryLoc <= this.prev && r.call(n, "finallyLoc") && this.prev < n.finallyLoc) {
                        var a = n;
                        break
                    }
                }
                a && ("break" === e || "continue" === e) && a.tryLoc <= t && t <= a.finallyLoc && (a = null);
                var s = a ? a.completion : {};
                return s.type = e, s.arg = t, a ? (this.method = "next", this.next = a.finallyLoc, E) : this.complete(s)
            }, complete: function (e, t) {
                if ("throw" === e.type) throw e.arg;
                return "break" === e.type || "continue" === e.type ? this.next = e.arg : "return" === e.type ? (this.rval = this.arg = e.arg, this.method = "return", this.next = "end") : "normal" === e.type && t && (this.next = t), E
            }, finish: function (e) {
                for (var t = this.tryEntries.length - 1; t >= 0; --t) {
                    var i = this.tryEntries[t];
                    if (i.finallyLoc === e) return this.complete(i.completion, i.afterLoc), I(i), E
                }
            }, catch: function (e) {
                for (var t = this.tryEntries.length - 1; t >= 0; --t) {
                    var i = this.tryEntries[t];
                    if (i.tryLoc === e) {
                        var n = i.completion;
                        if ("throw" === n.type) {
                            var r = n.arg;
                            I(i)
                        }
                        return r
                    }
                }
                throw Error("illegal catch attempt")
            }, delegateYield: function (t, i, n) {
                return this.delegate = {
                    iterator: S(t),
                    resultName: i,
                    nextLoc: n
                }, "next" === this.method && (this.arg = e), E
            }
        }, i
    }

    function i(e, t, i, n, r, a, s) {
        try {
            var o = e[a](s), c = o.value
        } catch (e) {
            return void i(e)
        }
        o.done ? t(c) : Promise.resolve(c).then(n, r)
    }

    function n(e) {
        return function () {
            var t = this, n = arguments;
            return new Promise((function (r, a) {
                var s = e.apply(t, n);

                function o(e) {
                    i(s, r, a, o, c, "next", e)
                }

                function c(e) {
                    i(s, r, a, o, c, "throw", e)
                }

                o(void 0)
            }))
        }
    }

    function r(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }

    function a(e, t) {
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, o(n.key), n)
        }
    }

    function s(e, t, i) {
        return t && a(e.prototype, t), i && a(e, i), Object.defineProperty(e, "prototype", {writable: !1}), e
    }

    function o(e) {
        var t = function (e, t) {
            if ("object" != h(e) || !e) return e;
            var i = e[Symbol.toPrimitive];
            if (void 0 !== i) {
                var n = i.call(e, t || "default");
                if ("object" != h(n)) return n;
                throw new TypeError("@@toPrimitive must return a primitive value.")
            }
            return ("string" === t ? String : Number)(e)
        }(e, "string");
        return "symbol" == h(t) ? t : t + ""
    }

    function c(e, t) {
        var i = "undefined" != typeof Symbol && e[Symbol.iterator] || e["@@iterator"];
        if (!i) {
            if (Array.isArray(e) || (i = function (e, t) {
                if (e) {
                    if ("string" == typeof e) return l(e, t);
                    var i = {}.toString.call(e).slice(8, -1);
                    return "Object" === i && e.constructor && (i = e.constructor.name), "Map" === i || "Set" === i ? Array.from(e) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? l(e, t) : void 0
                }
            }(e)) || t && e && "number" == typeof e.length) {
                i && (e = i);
                var n = 0, r = function () {
                };
                return {
                    s: r, n: function () {
                        return n >= e.length ? {done: !0} : {done: !1, value: e[n++]}
                    }, e: function (e) {
                        throw e
                    }, f: r
                }
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
        }
        var a, s = !0, o = !1;
        return {
            s: function () {
                i = i.call(e)
            }, n: function () {
                var e = i.next();
                return s = e.done, e
            }, e: function (e) {
                o = !0, a = e
            }, f: function () {
                try {
                    s || null == i.return || i.return()
                } finally {
                    if (o) throw a
                }
            }
        }
    }

    function l(e, t) {
        (null == t || t > e.length) && (t = e.length);
        for (var i = 0, n = Array(t); i < t; i++) n[i] = e[i];
        return n
    }

    function h(e) {
        return h = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
            return typeof e
        } : function (e) {
            return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        }, h(e)
    }

    System.register([], (function (i, a) {
        "use strict";
        return {
            execute: function () {
                var a = Object.defineProperty, o = function (e, t, i) {
                    return function (e, t, i) {
                        t in e ? a(e, t, {enumerable: !0, configurable: !0, writable: !0, value: i}) : e[t] = i
                    }(e, "symbol" != h(t) ? t + "" : t, i), i
                };
                !function () {
                    var e = document.createElement("link").relList;
                    if (!(e && e.supports && e.supports("modulepreload"))) {
                        var t, i = c(document.querySelectorAll('link[rel="modulepreload"]'));
                        try {
                            for (i.s(); !(t = i.n()).done;) {
                                n(t.value)
                            }
                        } catch (r) {
                            i.e(r)
                        } finally {
                            i.f()
                        }
                        new MutationObserver((function (e) {
                            var t, i = c(e);
                            try {
                                for (i.s(); !(t = i.n()).done;) {
                                    var a = t.value;
                                    if ("childList" === a.type) {
                                        var s, o = c(a.addedNodes);
                                        try {
                                            for (o.s(); !(s = o.n()).done;) {
                                                var l = s.value;
                                                "LINK" === l.tagName && "modulepreload" === l.rel && n(l)
                                            }
                                        } catch (r) {
                                            o.e(r)
                                        } finally {
                                            o.f()
                                        }
                                    }
                                }
                            } catch (r) {
                                i.e(r)
                            } finally {
                                i.f()
                            }
                        })).observe(document, {childList: !0, subtree: !0})
                    }

                    function n(e) {
                        if (!e.ep) {
                            e.ep = !0;
                            var t = function (e) {
                                var t = {};
                                return e.integrity && (t.integrity = e.integrity), e.referrerPolicy && (t.referrerPolicy = e.referrerPolicy), "use-credentials" === e.crossOrigin ? t.credentials = "include" : "anonymous" === e.crossOrigin ? t.credentials = "omit" : t.credentials = "same-origin", t
                            }(e);
                            fetch(e.href, t)
                        }
                    }
                }();
                var l = function () {
                    return s((function e(t) {
                        r(this, e), o(this, "richText"), o(this, "vnode"), o(this, "cursorIndex"), o(this, "cursorLeft"), o(this, "needCallSpace", !1), o(this, "VOID_KEY", "\ufeff"), o(this, "ZERO_WIDTH_KEY", "​"), this.richText = t, this.textInnerHtmlInit(), this.richText.focus()
                    }), [{
                        key: "textInnerHtmlInit", value: function () {
                            var e = arguments.length > 1 ? arguments[1] : void 0;
                            if (arguments.length > 0 && void 0 !== arguments[0] && arguments[0] || this.getNodeEmpty(this.richText)) {
                                this.richText.innerHTML = "";
                                var t = this.getGridElm();
                                this.richText.appendChild(t);
                                var i = t.children[0].children[0];
                                e && (i.innerHTML = e, i.setAttribute("data-set-empty", "false"));
                                var n = i.childNodes[0];
                                this.restCursorPos(n, n.textContent === this.VOID_KEY ? 1 : n.textContent.length)
                            }
                        }
                    }, {
                        key: "onceCall", value: function (e) {
                            var t = this;
                            return new Promise((function (i) {
                                var n = t.createChatTagElm(e, "@", "at-user", "user-id");
                                t.replaceRegContent(n), i()
                            }))
                        }
                    }, {
                        key: "onceSearchCall", value: function (e, t) {
                            var i = this;
                            return new Promise((function (n) {
                                var r = i.createChatTagElm(e, "@", "at-user", "user-id");
                                i.replaceRegContent(r, t), n()
                            }))
                        }
                    }, {
                        key: "onceCustomCall", value: function (e, t, i) {
                            var n = this;
                            return new Promise((function (r) {
                                var a = n.createChatTagElm(e, i, "at-tag", "tag-id");
                                a.children[0].setAttribute("data-set-prefix", i), n.replaceRegContent(a, t), r()
                            }))
                        }
                    }, {
                        key: "upDataNodeOrIndex", value: function () {
                            var e, t, i, n = window.getSelection(), r = n.focusNode, a = n.focusOffset,
                                s = n.anchorOffset, o = (null == r ? void 0 : r.parentNode) || void 0;
                            !o || !o.getAttribute || "richInput" !== o.getAttribute("data-set-richType") || (null == (i = null == (t = null == (e = null == r ? void 0 : r.parentNode) ? void 0 : e.parentNode) ? void 0 : t.parentNode) ? void 0 : i.parentNode) !== this.richText || (this.vnode = r, this.cursorIndex = a, this.cursorLeft = s < a ? s : a)
                        }
                    }, {
                        key: "showAt", value: function () {
                            if (this.upDataNodeOrIndex(), !this.vnode || this.vnode.nodeType !== Node.TEXT_NODE) return !1;
                            var e = (this.vnode.textContent || "").slice(0, this.cursorIndex),
                                t = /@([^@\s]*)$/.exec(e);
                            return t && 2 === t.length && "@" === e[e.length - 1]
                        }
                    }, {
                        key: "getRangeRect", value: function () {
                            var e = 0, t = 0, i = window.getSelection();
                            if (i.focusNode.nodeType !== Node.TEXT_NODE) return null;
                            var n = i.getRangeAt(0).getClientRects()[0];
                            return n && (e = n.x, t = n.y), {x: e, y: t}
                        }
                    }, {
                        key: "createChatTagElm", value: function (e, t, i, n) {
                            var r = document.createElement("span");
                            return r.className = i, r.setAttribute("data-".concat(n), String(e.id)), r.contentEditable = "false", r.textContent = "".concat(t).concat(e.name).concat(this.needCallSpace ? " " : ""), this.createNewDom(r)
                        }
                    }, {
                        key: "createNewDom", value: function (e) {
                            var t = document.createElement("span");
                            return t.className = "chat-tag", t.setAttribute("contenteditable", "false"), t.setAttribute("data-set-richType", "chatTag"), e.className += " chat-stat", t.appendChild(e), t
                        }
                    }, {
                        key: "restCursorPos", value: function (e, t) {
                            null == t ? t = e.textContent === this.VOID_KEY ? 1 : 0 : t > e.textContent.length && (t = e.textContent.length);
                            var i = new Range;
                            i.setStart(e, t), i.setEnd(e, t);
                            var n = window.getSelection();
                            n && (this.vnode = e, this.cursorIndex = t, this.cursorLeft = t, n.removeAllRanges(), n.addRange(i))
                        }
                    }, {
                        key: "replaceRegContent", value: function (e) {
                            var t, i = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
                                n = this.vnode.textContent;
                            0 === (t = "boolean" == typeof i ? n.slice(0, i ? this.cursorIndex - 1 : this.cursorIndex) : n.slice(0, i - 1)).length ? (this.vnode.parentElement.setAttribute("data-set-empty", !0), this.vnode.textContent = this.VOID_KEY) : this.vnode.textContent = t;
                            var r = n.slice(this.cursorIndex), a = this.vnode.parentNode.parentNode, s = a.nextSibling;
                            s ? a.parentNode.insertBefore(e, s) : a.parentNode.appendChild(e);
                            var o = e.previousSibling.childNodes[0], c = o.childNodes[1];
                            c && o.removeChild(c);
                            var l = this.getGridElm(!0), h = l.childNodes[0];
                            r && r !== this.VOID_KEY && (h.setAttribute("data-set-empty", "false"), h.innerHTML = r);
                            var u = h.childNodes[1];
                            e.nextSibling ? (u && h.removeChild(u), a.parentNode.insertBefore(l, e.nextSibling)) : a.parentNode.appendChild(l), this.restCursorPos(h.childNodes[0])
                        }
                    }, {
                        key: "batchReplaceRegContent", value: function () {
                            var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [],
                                i = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                            return new Promise((function (n) {
                                var r = e.vnode.textContent, a = r.slice(0, i ? e.cursorIndex - 1 : e.cursorIndex);
                                0 === a.length ? (e.vnode.parentElement.setAttribute("data-set-empty", !0), e.vnode.textContent = e.VOID_KEY) : e.vnode.textContent = a;
                                var s = r.slice(e.cursorIndex), o = "";
                                t.forEach((function (i, n) {
                                    var r = n === t.length - 1 && s && s !== e.VOID_KEY;
                                    o += '\n          <span class="chat-tag" contenteditable="false" data-set-richType="chatTag"><span class="at-user chat-stat" data-user-id="'.concat(i.id, '" contentEditable="false">@').concat(i.name).concat(e.needCallSpace ? " " : "", "</span></span>\n          ").concat(r ? '<span data-set-richType="richMark"><span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="false">'.concat(s, "</span></span>") : '<span data-set-richType="richMark"><span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="true">'.concat(e.VOID_KEY, "</span></span>"), "\n        ")
                                }));
                                var c = document.createElement("div");
                                c.innerHTML = o;
                                var l = [];
                                Array.from(c.children).forEach((function (e) {
                                    l.push(e)
                                }));
                                var h = e.vnode.parentNode.parentNode, u = h.nextSibling, d = l[l.length - 1];
                                if (u) l.forEach((function (e) {
                                    h.parentNode.insertBefore(e, u)
                                })); else {
                                    var p = d.childNodes[0];
                                    "true" === p.getAttribute("data-set-empty") && (p.innerHTML = "".concat(e.VOID_KEY, "<br>")), l.forEach((function (e) {
                                        h.parentNode.appendChild(e)
                                    }))
                                }
                                e.restCursorPos(d.childNodes[0].childNodes[0]), n()
                            }))
                        }
                    }, {
                        key: "switchRange", value: function (e) {
                            var t, i, n, r, a = window.getSelection(), s = a.focusNode, o = a.focusOffset;
                            if (s.getAttribute && "richInput" === s.getAttribute("data-set-richType") && (s = s.childNodes[0]), s.nodeType === Node.TEXT_NODE) {
                                var c = s.textContent.length, l = s.parentNode.parentNode;
                                switch (e) {
                                    case"ArrowLeft":
                                        if (o > 0 && s.textContent !== this.VOID_KEY) {
                                            r = o - 1, n = s;
                                            break
                                        }
                                        var h = null == (t = null == l ? void 0 : l.previousSibling) ? void 0 : t.previousSibling;
                                        if (h) r = (n = h.childNodes[0].childNodes[0]).textContent.length; else {
                                            var u = l.parentNode.previousSibling;
                                            u && (r = (n = u.lastChild.childNodes[0].childNodes[0]).textContent.length)
                                        }
                                        break;
                                    case"ArrowRight":
                                        if (o < c && s.textContent !== this.VOID_KEY) {
                                            r = o + 1, n = s;
                                            break
                                        }
                                        var d = null == (i = null == l ? void 0 : l.nextSibling) ? void 0 : i.nextSibling;
                                        if (d) r = (n = d.childNodes[0].childNodes[0]).textContent === this.VOID_KEY ? 1 : 0; else {
                                            var p = l.parentNode.nextSibling;
                                            p && (r = (n = p.childNodes[0].childNodes[0].childNodes[0]).textContent === this.VOID_KEY ? 1 : 0)
                                        }
                                }
                            }
                            (r || 0 === r) && this.restCursorPos(n, r)
                        }
                    }, {
                        key: "getGridElm", value: function () {
                            var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                                t = document.createElement("span");
                            if (t.setAttribute("data-set-richType", "richMark"), t.innerHTML = '<span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="true">'.concat(this.VOID_KEY, "<br></span>"), e) return t;
                            var i = document.createElement("p");
                            return i.className = "chat-grid-wrap", i.setAttribute("data-set-richType", "richBox"), i.appendChild(t), i
                        }
                    }, {
                        key: "updateGrid", value: function () {
                            var e, t, i, n, r = window.getSelection(), a = r.focusNode, s = a.parentNode;
                            switch (s.getAttribute("data-set-richType")) {
                                case"richAllBox":
                                    if (!(e = a.childNodes[r.focusOffset]) || "chatTag" === e.getAttribute("data-set-richType")) {
                                        var o = this.getGridElm(!0), c = o.children[0];
                                        e ? (c.removeChild(c.childNodes[1]), a.insertBefore(o, e)) : a.appendChild(o), this.restCursorPos(c.childNodes[0]);
                                        break
                                    }
                                    if ("BR" === e.tagName) {
                                        var l = this.getGridElm(!0), h = l.children[0];
                                        a.insertBefore(l, e), a.removeChild(e), this.restCursorPos(h.childNodes[0], h.childNodes[0].textContent.length)
                                    }
                                    break;
                                case"richMark":
                                    var u = s.parentNode, d = Array.prototype.indexOf.call(u.childNodes, s);
                                    if (-1 === d) break;
                                    if (0 === d) {
                                        var p = r.focusNode;
                                        p.setAttribute("data-set-empty", "true"), p.innerHTML = "".concat(this.VOID_KEY, "<br>"), e = p.childNodes[0], this.restCursorPos(e, e.textContent.length);
                                        break
                                    }
                                    var m, g = s.previousSibling;
                                    "chatTag" === g.getAttribute("data-set-richType") ? (m = g.previousSibling, u.removeChild(g), u.removeChild(s)) : (m = s.previousSibling, u.removeChild(s)), (e = m.childNodes[0].childNodes[0]).textContent === this.VOID_KEY && e.parentNode.appendChild(document.createElement("br")), this.restCursorPos(e, e.textContent.length);
                                    break;
                                case"richInput":
                                    if (i = (n = s.parentNode).parentNode, this.getNodeEmpty(s)) {
                                        s.setAttribute("data-set-empty", "true"), i.childNodes[i.childNodes.length - 1] === n && (s.innerHTML = "".concat(this.VOID_KEY, "<br>")), e = s.childNodes[0], this.restCursorPos(e, e.textContent.length);
                                        break
                                    }
                                    if ("true" === String(s.getAttribute("data-set-empty")) && (s.setAttribute("data-set-empty", "false"), s.innerHTML = s.childNodes[0].textContent.replace(new RegExp(this.VOID_KEY, "g"), ""), e = s.childNodes[0], this.restCursorPos(e, e.textContent.length)), (t = s.parentNode.nextSibling) && t.nodeType === Node.TEXT_NODE) {
                                        var f = t.textContent, v = this.getGridElm(!0);
                                        v.childNodes[0].textContent = f, v.childNodes[0].setAttribute("data-set-empty", "false"), t.parentNode.insertBefore(v, t), t.parentNode.removeChild(t), t = v
                                    }
                                    t && "richMark" === t.getAttribute("data-set-richType") && this.markMerge(s.parentNode, t)
                            }
                        }
                    }, {
                        key: "getNodeEmpty", value: function (e) {
                            var t = new RegExp("^(".concat(this.ZERO_WIDTH_KEY, "|<br>|").concat(this.VOID_KEY, ")+$"));
                            return !e.innerHTML || t.test(e.innerHTML)
                        }
                    }, {
                        key: "setWrap", value: function () {
                            var e = window.getSelection(), t = e.focusNode, i = e.focusOffset;
                            if (t.nodeType !== Node.TEXT_NODE) {
                                if (!t.getAttribute || "richInput" !== t.getAttribute("data-set-richType")) return;
                                t = t.childNodes[0]
                            }
                            var n = t.textContent.slice(i), r = t.parentNode.parentNode, a = r.parentNode,
                                s = Array.prototype.indexOf.call(a.childNodes, r),
                                o = Array.prototype.slice.call(a.childNodes, s + 1), c = this.getGridElm(),
                                l = c.children[0].children[0].childNodes[0], h = 1;
                            (n || o.length > 0) && l.parentNode.removeChild(l.parentNode.childNodes[1]), n && n !== this.VOID_KEY && (t.textContent = t.textContent.slice(0, i), l.textContent = (l.textContent + n).replace(new RegExp(this.VOID_KEY, "g"), (function () {
                                return h--, ""
                            })), l.parentElement.setAttribute("data-set-empty", "false")), o.forEach((function (e) {
                                a.removeChild(e), c.appendChild(e)
                            }));
                            var u = a.lastChild.childNodes[0], d = c.lastChild.childNodes[0];
                            if (u.childNodes.length <= 1) {
                                var p = u.childNodes[0];
                                (!p.textContent || p.textContent === this.VOID_KEY) && (u.innerHTML = "".concat(this.VOID_KEY, "<br>"), u.setAttribute("data-set-empty", "true"))
                            }
                            if ("richMark" !== d.parentElement.getAttribute("data-set-richType")) c.appendChild(this.getGridElm(!0)); else if (d.childNodes.length <= 1) {
                                var m = d.childNodes[0];
                                (!m.textContent || m.textContent === this.VOID_KEY) && (d.innerHTML = "".concat(this.VOID_KEY, "<br>"), d.setAttribute("data-set-empty", "true"), l = c.children[0].children[0].childNodes[0])
                            }
                            a.nextSibling ? this.richText.insertBefore(c, a.nextSibling) : this.richText.appendChild(c), this.restCursorPos(l, l.textContent === this.VOID_KEY ? 1 : h), this.viewIntoPoint()
                        }
                    }, {
                        key: "selectRegionMerge", value: function () {
                            var e = window.getSelection();
                            if (!(e.isCollapsed || e.rangeCount <= 0)) {
                                var t = e.getRangeAt(0);
                                if (t.startContainer.nodeType === Node.TEXT_NODE && t.startContainer === t.endContainer) {
                                    var i = t.startContainer;
                                    if (i.length === t.endOffset - t.startOffset) {
                                        var n = i.parentNode, r = n.parentNode === n.parentNode.parentNode.lastChild;
                                        n.setAttribute("data-set-empty", "true"), n.innerHTML = "\ufeff".concat(r ? "<br>" : ""), this.restCursorPos(n.childNodes[0])
                                    } else t.deleteContents()
                                } else if (t.commonAncestorContainer && "richBox" === t.commonAncestorContainer.getAttribute("data-set-richType")) {
                                    var a = t.startContainer.nodeType === Node.TEXT_NODE ? t.startContainer.parentNode.parentNode : t.startContainer,
                                        s = t.endContainer.nodeType === Node.TEXT_NODE ? t.endContainer.parentNode.parentNode : t.endContainer;
                                    t.deleteContents(), a.getAttribute("data-set-richType") === s.getAttribute("data-set-richType") && this.markMerge(a, s)
                                } else if (t.commonAncestorContainer === t.startContainer && t.startContainer === t.endContainer) this.textInnerHtmlInit(!0); else {
                                    var o = function (e) {
                                        if (e.nodeType === Node.TEXT_NODE) return e.parentNode.parentNode.parentNode;
                                        switch (e.getAttribute("data-set-richType")) {
                                            case"richInput":
                                                return e.parentNode.parentNode;
                                            case"richMark":
                                                return e.parentNode;
                                            case"richBox":
                                                return e;
                                            default:
                                                return null
                                        }
                                    }, c = o(t.startContainer), l = o(t.endContainer);
                                    if (!c || !l) return;
                                    t.deleteContents(), this.gridMerge(c, l)
                                }
                                return !0
                            }
                        }
                    }, {
                        key: "gridElmMerge", value: function () {
                            var e = this, t = window.getSelection(), i = t.focusNode, n = t.focusOffset,
                                r = t.isCollapsed;
                            if (n > 1 || !r) return !1;
                            var a = function (t, i) {
                                return (t.parentNode === e.richText || t === t.parentNode.childNodes[0]) && (-1 !== Array.prototype.indexOf.call(e.richText.childNodes, t) ? t : !(i >= 6) && a(t.parentNode, i + 1))
                            }, s = a(i, 0);
                            if (!s || s === this.richText.childNodes[0] || 1 === n && "false" === s.children[0].children[0].getAttribute("data-set-empty")) return !1;
                            var o = s.previousSibling;
                            return this.gridMerge(o, s), !0
                        }
                    }, {
                        key: "delMarkRule", value: function () {
                            var e = window.getSelection(), t = e.focusNode, i = t.textContent, n = t.parentNode,
                                r = n.parentNode, a = r.parentNode;
                            if (!e.isCollapsed || "richInput" !== n.getAttribute("data-set-richType")) return !1;
                            if (i && 1 === i.length && r !== a.childNodes[0] && (0 !== e.focusOffset || i === this.VOID_KEY)) {
                                if (i === this.VOID_KEY) {
                                    var s = r.previousSibling.previousSibling;
                                    a.removeChild(r.previousSibling), a.removeChild(r);
                                    var o = s.childNodes[0], c = o.childNodes[0];
                                    c.textContent === this.VOID_KEY && s === a.lastChild && o.appendChild(document.createElement("br")), this.restCursorPos(c, c.textContent.length)
                                } else {
                                    n.innerHTML = r === a.lastChild ? "".concat(this.VOID_KEY, "<br>") : this.VOID_KEY, n.setAttribute("data-set-empty", "true");
                                    var l = n.childNodes[0];
                                    this.restCursorPos(l, 1)
                                }
                                return !0
                            }
                            if (0 === e.focusOffset) {
                                var h = n.parentNode, u = null == h ? void 0 : h.previousSibling;
                                return !(!u || "chatTag" !== u.getAttribute("data-set-richType")) && (this.delTag(u), !0)
                            }
                        }
                    }, {
                        key: "delTag", value: function (e) {
                            var t = e.previousSibling, i = e.nextSibling;
                            e.parentNode.removeChild(e), this.markMerge(t, i)
                        }
                    }, {
                        key: "gridMerge", value: function (e, t) {
                            var i = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
                            "richMark" !== e.lastChild.getAttribute("data-set-richType") && e.appendChild(this.getGridElm(!0)), "richMark" !== t.childNodes[0].getAttribute("data-set-richType") && t.insertBefore(this.getGridElm(!0), t.childNodes[0]);
                            var n = e.lastChild.childNodes[0], r = n.childNodes[0], a = r.textContent.length;
                            Array.prototype.forEach.call(t.childNodes, (function (t) {
                                e.appendChild(t.cloneNode(!0))
                            })), t.childNodes.length > 1 && n.childNodes[1] && n.removeChild(n.childNodes[1]);
                            var s = n.parentNode.nextSibling;
                            if (s) {
                                var o = s.children[0].childNodes[0];
                                o && o.textContent !== this.VOID_KEY && (n.childNodes[1] && n.removeChild(n.childNodes[1]), r.textContent = (r.textContent + o.textContent).replace(new RegExp(this.VOID_KEY, "g"), (function () {
                                    return a--, ""
                                })), r.parentElement.setAttribute("data-set-empty", "false")), e.removeChild(s)
                            }
                            if ("" === r.textContent && (r.textContent = this.VOID_KEY, r.parentNode.setAttribute("data-set-empty", "true"), a = 1), i) {
                                var c = e.childNodes[e.childNodes.length - 1].childNodes[0].childNodes[0];
                                this.restCursorPos(c, c.textContent.length)
                            } else this.richText.removeChild(t), this.restCursorPos(r, a);
                            this.viewIntoPoint()
                        }
                    }, {
                        key: "markMerge", value: function (e, t) {
                            var i = e.children[0].childNodes[0], n = i.textContent.length;
                            if (t) {
                                var r = t.children[0].childNodes[0];
                                r && r.textContent !== this.VOID_KEY && (i.textContent = (i.textContent + r.textContent).replace(new RegExp(this.VOID_KEY, "g"), (function () {
                                    return n--, ""
                                })), i.parentElement.setAttribute("data-set-empty", "false")), t.parentNode.removeChild(t)
                            }
                            "" === i.textContent && (i.textContent = this.VOID_KEY, i.parentNode.setAttribute("data-set-empty", "true"), n = 1);
                            var a = e.parentNode;
                            i.textContent === this.VOID_KEY && e === a.lastChild && (i.parentNode.appendChild(document.createElement("br")), i.parentNode.setAttribute("data-set-empty", "true"), n = 1), this.restCursorPos(i, n)
                        }
                    }, {
                        key: "setCallSpace", value: function (e) {
                            this.needCallSpace = e
                        }
                    }, {
                        key: "getWrapNode", value: function (e) {
                            var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                            if (e.nodeType === Node.TEXT_NODE) return e.parentNode.parentNode.parentNode;
                            var i = e.getAttribute("data-set-richType");
                            if (t && "chatTag" === i) return e.parentNode;
                            switch (i) {
                                case"richInput":
                                    return e.parentNode.parentNode;
                                case"richMark":
                                    return e.parentNode;
                                case"richBox":
                                    return e
                            }
                        }
                    }, {
                        key: "getMarkNode", value: function (e) {
                            var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                            if (e.nodeType === Node.TEXT_NODE) return e.parentNode.parentNode;
                            var i = e.getAttribute("data-set-richType");
                            if (t && "chatTag" === i) return e;
                            switch (i) {
                                case"richInput":
                                    return e.parentNode;
                                case"richMark":
                                    return e
                            }
                        }
                    }, {
                        key: "getRichTextNodeIndex", value: function (e) {
                            var t = this.getMarkNode(e), i = t.parentNode;
                            return t && i ? {
                                gridIndex: Array.prototype.indexOf.call(this.richText.childNodes, i),
                                markIndex: Array.prototype.indexOf.call(i.childNodes, t)
                            } : {gridIndex: null, markIndex: null}
                        }
                    }, {
                        key: "setWrapNodeByMark", value: function (e) {
                            var t = document.createElement("p");
                            return t.className = "chat-grid-wrap", t.setAttribute("data-set-richType", "richBox"), Array.prototype.forEach.call(e, (function (e) {
                                t.appendChild(e)
                            })), t
                        }
                    }, {
                        key: "setRangeLastText", value: function () {
                            var e = this.richText.childNodes[this.richText.childNodes.length - 1],
                                t = e.childNodes[e.childNodes.length - 1].children[0].childNodes[0];
                            this.restCursorPos(t, t.textContent === this.VOID_KEY ? 1 : t.textContent.length), this.viewIntoPoint()
                        }
                    }, {
                        key: "viewIntoPoint", value: function () {
                            var e = window.getSelection();
                            if (e.rangeCount > 0) {
                                var t = e.getRangeAt(0), i = this.getWrapNode(t.endContainer);
                                if (!i) return;
                                var n = this.richText.parentElement, r = n.scrollHeight, a = n.clientHeight,
                                    s = n.scrollTop;
                                if (r <= a) return;
                                var o = i.getBoundingClientRect().top - n.getBoundingClientRect().top + i.clientHeight + s;
                                if (o < s || o > a + s) {
                                    var c = o - a;
                                    n.scrollTo(0, c)
                                }
                            }
                        }
                    }])
                }(), u = function () {
                    var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : 50;
                    return new Promise((function (t) {
                        setTimeout(t, e)
                    }))
                }, d = function (e) {
                    return "false" !== String(e) && "null" !== String(e) && "0" !== String(e)
                }, p = function (e, t) {
                    var i, n = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
                    return function () {
                        for (var r = arguments.length, a = new Array(r), s = 0; s < r; s++) a[s] = arguments[s];
                        var o = this, c = n && !i;
                        clearTimeout(i), i = setTimeout((function () {
                            i = null, n || e.apply(o, a)
                        }), t), c && e.apply(o, a)
                    }
                }, m = function (e, t) {
                    var i;
                    return function () {
                        for (var n = arguments.length, r = new Array(n), a = 0; a < n; a++) r[a] = arguments[a];
                        i || (e.apply(this, r), i = !0, setTimeout((function () {
                            i = !1
                        }), t))
                    }
                }, g = function (e, t, i) {
                    if (e = e.toLowerCase(), t = t.toLowerCase(), i = (i = i.toLowerCase()).replace(/\s/g, ""), !/^[\u4E00-\u9FFF]+$/.test(e) || !t || -1 === t.indexOf(" ")) {
                        var n = E(e, t, i, {continuous: !1, space: "ignore", insensitive: !0});
                        return !!(n && n.length > 0)
                    }
                    return f(e, t, i)
                }, f = function (e, t, i) {
                    for (var n = (e = e.replace(/\s/g, "")).split("").map((function (e) {
                        return [e, !1]
                    })), r = t.split(" ").map((function (e) {
                        return [e, !1, ""]
                    })), a = [], s = 0, o = function () {
                        var e = i[c];
                        /^[\u4E00-\u9FFF]+$/.test(e) ? n.some((function (t, i) {
                            var n = t[1] || r[i][1];
                            if (i >= s && !n && t[0] === e) return t[1] = !0, s = i, a.push(i), !0
                        })) : r.some((function (t, i) {
                            var n = t[1] || r[i][1], o = t[2] + e;
                            if (i >= s && !n && v(t[0], o)) return t[2] = o, s = i, t[0] === t[2] && (t[1] = !0), a.push(i), !0
                        }))
                    }, c = 0; c < i.length; c++) o();
                    return a.length === i.length
                }, v = function (e, t) {
                    for (var i = !1, n = -1, r = 0; r < t.length; r++) {
                        var a = e.indexOf(t[r]);
                        if (!(a > n)) {
                            i = !1;
                            break
                        }
                        n = a, i = !0
                    }
                    return i
                }, E = function (e, t, i, n) {
                    for (var r, a = [], s = function () {
                        if ("ignore" === n.space && " " === e[o]) return a.push(o), 0;
                        if (e[o] === i[0]) return i = i.slice(1), a.push(o), 0;
                        var r = t.split(" "), s = 0;
                        return r.forEach((function (e) {
                            var t = y(e, i);
                            t > s && (s = t)
                        })), s && (i = i.slice(s), a.push(o)), i ? void 0 : 1
                    }, o = 0; o < e.length && (0 === (r = s()) || 1 !== r); o++) ;
                    if (i) return null;
                    if (n.continuous) {
                        var c = a;
                        if (a.some((function (e, t) {
                            return t > 0 && e !== c[t - 1] + 1
                        }))) return null
                    }
                    return "ignore" === n.space && (a = a.filter((function (t) {
                        return " " !== e[t]
                    }))), a.length ? a : null
                }, y = function (e, t) {
                    for (var i = 0, n = 0; n < e.length; n++) e[n] === t[i] && i++;
                    return i
                };

                function x(e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                        i = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "block";
                    e && (e.className = e.className.replace(/ chat-view-show| chat-view-hidden/g, ""), t ? (e.style.display = i, e.className += " chat-view-show") : (e.className += " chat-view-hidden", e.style.display = "none"))
                }

                function b(e) {
                    var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "block";
                    return e && e.style.display === t
                }

                function k(e, t, i) {
                    e.classList[i ? "add" : "remove"](t)
                }

                var T = function (e, t, i) {
                        return e.forEach((function (e) {
                            if (i in e) {
                                var n = t.indexOf(String(e[i]));
                                -1 !== n && (t[n] = e)
                            }
                        })), t.filter((function (e) {
                            return e[i]
                        }))
                    },
                    C = '<div class="ant-spin ant-spin-spinning" aria-live="polite" aria-busy="true"><span class="ant-spin-dot ant-spin-dot-spin"><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i></span></div>',
                    D = function () {
                        return s((function e(i) {
                            var a = this;
                            r(this, e), o(this, "chat"), o(this, "deviceInfo", function () {
                                var e = navigator.userAgent, t = /(?:Windows Phone)/.test(e),
                                    i = /(?:SymbianOS)/.test(e) || t, n = /(?:Android)/.test(e),
                                    r = /(?:Firefox)/.test(e),
                                    a = /(?:iPad|PlayBook)/.test(e) || n && !/(?:Mobile)/.test(e) || r && /(?:Tablet)/.test(e),
                                    s = /(?:iPhone)/.test(e) && !a;
                                return {isTablet: a, isPhone: s, isAndroid: n, isPc: !s && !n && !i}
                            }()), o(this, "isComposition", !1), o(this, "undoHistory", []), o(this, "redoHistory", []), o(this, "doOverHistory", !0), o(this, "isExternalCallPopup", !1), o(this, "isIMEModel", !1), o(this, "matchKey", 0), o(this, "chatEventModule", {
                                enterSend: [],
                                operate: [],
                                defaultAction: [],
                                atMatch: [],
                                atCheck: [],
                                tagCheck: []
                            }), o(this, "debounceEvents", {
                                recordHistory: function () {
                                }, dialogMoveToRange: function (e) {
                                }, matchPointDialog: function () {
                                }, movePointActiveUserElm: function (e) {
                                }, moveCustomActiveTagElm: function (e) {
                                }
                            }), o(this, "richText"), o(this, "placeholderElm"), o(this, "userList", []), o(this, "userProps", {
                                id: "id",
                                name: "name",
                                avatar: "avatar",
                                pinyin: "pinyin"
                            }), o(this, "targetUserList", []), o(this, "checkboxRows", []), o(this, "needCallEvery", !0), o(this, "maxLength"), o(this, "textLength", 0), o(this, "needDialog", !0), o(this, "asyncMatch", !1), o(this, "copyType", ["text"]), o(this, "uploadImage"), o(this, "customTags", {}), o(this, "wrapKeyFun", (function (e) {
                                return e.ctrlKey && ["Enter"].includes(e.key)
                            })), o(this, "sendKeyFun", (function (e) {
                                return !e.ctrlKey && ["Enter"].includes(e.key)
                            })), o(this, "startOpenIndex", 0), o(this, "isPointSearchMode", !1), o(this, "pcElms", {
                                containerDialogElm: null,
                                pointDialogElm: null,
                                pointDialogCheckElm: null,
                                pointDialogMainElm: null,
                                pointDialogUsersElm: [],
                                pointDialogActiveElm: null,
                                pointDialogLoadingElm: null,
                                pointDialogEmptyElm: null,
                                checkDialogElm: null,
                                checkDialogSearchResultElm: null,
                                checkDialogUsersElm: null,
                                checkDialogSearchInputElm: null,
                                checkDialogTagsElm: null,
                                customTagDialogElms: {},
                                customTagDialogTagKey: "",
                                customTagDialogActiveElm: null
                            }), o(this, "h5Elms", {
                                dialogElm: null,
                                dialogMainElm: null,
                                dialogCheckElm: null,
                                dialogShowElm: null,
                                dialogSearchElm: null,
                                dialogEmptyElm: null,
                                dialogLoadingElm: null
                            }), o(this, "reviseLabels", {
                                callEveryLabel: "所有人",
                                searchEmptyLabel: "没有匹配到任何结果",
                                checkAllLabel: "全选",
                                checkEmptyLabel: "请选择需要@的成员"
                            }), o(this, "winClick", (function () {
                                b(a.pcElms.pointDialogElm) && a.exitPointDialog(), a.pcElms.checkDialogSearchResultElm && x(a.pcElms.checkDialogSearchResultElm), a.pcElms.customTagDialogTagKey && b(a.pcElms.customTagDialogElms[a.pcElms.customTagDialogTagKey]) && a.exitCustomTagDialog()
                            })), o(this, "winKeydown", function () {
                                var e = n(t().mark((function e(i) {
                                    var n, r, s, o;
                                    return t().wrap((function (e) {
                                        for (; ;) switch (e.prev = e.next) {
                                            case 0:
                                                if (i.ctrlKey && "KeyZ" === i.code && i.preventDefault(), a.isComposition) {
                                                    e.next = 45;
                                                    break
                                                }
                                                if (!b(a.pcElms.pointDialogElm)) {
                                                    e.next = 24;
                                                    break
                                                }
                                                if ("ArrowDown" !== i.code) {
                                                    e.next = 5;
                                                    break
                                                }
                                                return i.preventDefault(), a.debounceEvents.movePointActiveUserElm("down"), e.abrupt("return");
                                            case 5:
                                                if ("ArrowUp" !== i.code) {
                                                    e.next = 8;
                                                    break
                                                }
                                                return i.preventDefault(), a.debounceEvents.movePointActiveUserElm("up"), e.abrupt("return");
                                            case 8:
                                                if ("Enter" !== i.code && "NumpadEnter" !== i.code || !a.pcElms.pointDialogActiveElm) {
                                                    e.next = 22;
                                                    break
                                                }
                                                return i.preventDefault(), n = a.pcElms.pointDialogActiveElm.getAttribute("data-set-id"), e.next = 13, u(100);
                                            case 13:
                                                if (!a.isPointSearchMode && !a.asyncMatch) {
                                                    e.next = 18;
                                                    break
                                                }
                                                return e.next = 16, a.matchSetTag(a.userList.find((function (e) {
                                                    return e.id === n
                                                })));
                                            case 16:
                                                e.next = 21;
                                                break;
                                            case 18:
                                                return r = a.targetUserList.find((function (e) {
                                                    return String(e[a.userProps.id]) === n
                                                })), e.next = 21, a.onceSetTag(r);
                                            case 21:
                                                a.exitPointDialog();
                                            case 22:
                                                e.next = 45;
                                                break;
                                            case 24:
                                                if (!a.pcElms.customTagDialogTagKey || !b(a.pcElms.customTagDialogElms[a.pcElms.customTagDialogTagKey])) {
                                                    e.next = 45;
                                                    break
                                                }
                                                if ("ArrowDown" !== i.code) {
                                                    e.next = 28;
                                                    break
                                                }
                                                return i.preventDefault(), a.debounceEvents.moveCustomActiveTagElm("down"), e.abrupt("return");
                                            case 28:
                                                if ("ArrowUp" !== i.code) {
                                                    e.next = 31;
                                                    break
                                                }
                                                return i.preventDefault(), a.debounceEvents.moveCustomActiveTagElm("up"), e.abrupt("return");
                                            case 31:
                                                if ("Enter" !== i.code && "NumpadEnter" !== i.code || !a.pcElms.customTagDialogActiveElm) {
                                                    e.next = 45;
                                                    break
                                                }
                                                return i.preventDefault(), s = a.pcElms.customTagDialogActiveElm.getAttribute("data-set-id"), e.next = 36, u(100);
                                            case 36:
                                                if (o = a.customTags[a.pcElms.customTagDialogTagKey].find((function (e) {
                                                    return e.id === s
                                                })), !a.isPointSearchMode) {
                                                    e.next = 42;
                                                    break
                                                }
                                                return e.next = 40, a.matchSetCustomTag(o);
                                            case 40:
                                                e.next = 44;
                                                break;
                                            case 42:
                                                return e.next = 44, a.onceSetCustomTag(o);
                                            case 44:
                                                a.exitCustomTagDialog();
                                            case 45:
                                            case"end":
                                                return e.stop()
                                        }
                                    }), e)
                                })));
                                return function (t) {
                                    return e.apply(this, arguments)
                                }
                            }());
                            var s = i.elm, c = i.callEveryLabel, h = i.needDialog, p = i.asyncMatch;
                            k(s, "chat-area-".concat(this.deviceInfo.isPc ? "pc" : "h5"), !0), this.richText = document.createElement("div"), this.richText.setAttribute("class", "chat-rich-text"), this.richText.setAttribute("data-set-richType", "richAllBox"), this.richText.setAttribute("contenteditable", "true"), s.appendChild(this.richText), this.placeholderElm = document.createElement("div"), this.placeholderElm.setAttribute("class", "chat-placeholder-wrap"), x(this.placeholderElm, !0), s.appendChild(this.placeholderElm), this.chat = new l(this.richText), c && (this.reviseLabels.callEveryLabel = c), this.needDialog = d(h), void 0 !== p && (this.asyncMatch = d(p)), this.needDialog && (this.deviceInfo.isPc ? this.hasPc() : this.hasH5()), this.registerEvent(), this.updateConfig(i), this.otherInit(i)
                        }), [{
                            key: "registerEvent", value: function () {
                                var e = this;
                                this.richText.addEventListener("keyup", (function (t) {
                                    if (e.needDialog) if (t.stopPropagation(), e.deviceInfo.isPc) 50 === t.keyCode || "Digit2" === t.code || "@" === t.key ? e.ruleShowPointDialog() : -1 !== Object.keys(e.pcElms.customTagDialogElms).indexOf(t.key) && e.showCustomTagDialog(t.key); else {
                                        var i = !1;
                                        switch ("Unidentified" === t.key ? "android" : "ios") {
                                            case"android":
                                                i = 229 === t.keyCode;
                                                break;
                                            case"ios":
                                                i = 50 === t.keyCode || "Digit2" === t.code || "@" === t.key
                                        }
                                        i && (e.userList.length > 0 || e.asyncMatch) && e.chat.showAt() && (e.showH5Dialog(), e.isExternalCallPopup = !1)
                                    }
                                })), this.richText.addEventListener("keydown", function () {
                                    var i = n(t().mark((function i(n) {
                                        return t().wrap((function (t) {
                                            for (; ;) switch (t.prev = t.next) {
                                                case 0:
                                                    if (e.deviceInfo.isPc || "Unidentified" !== n.key || 229 !== n.keyCode) {
                                                        t.next = 3;
                                                        break
                                                    }
                                                    return e.isIMEModel = !0, t.abrupt("return");
                                                case 3:
                                                    if (!e.isIMEModel) {
                                                        t.next = 5;
                                                        break
                                                    }
                                                    return t.abrupt("return");
                                                case 5:
                                                    if (!b(e.pcElms.pointDialogElm)) {
                                                        t.next = 8;
                                                        break
                                                    }
                                                    return ["ArrowUp", "ArrowDown", "Enter", "NumpadEnter"].includes(n.code) ? n.preventDefault() : ["ArrowLeft", "ArrowRight"].includes(n.code) && e.exitPointDialog(), t.abrupt("return");
                                                case 8:
                                                    if (!e.pcElms.customTagDialogTagKey || !b(e.pcElms.customTagDialogElms[e.pcElms.customTagDialogTagKey])) {
                                                        t.next = 11;
                                                        break
                                                    }
                                                    return ["ArrowUp", "ArrowDown", "Enter", "NumpadEnter"].includes(n.code) ? n.preventDefault() : ["ArrowLeft", "ArrowRight"].includes(n.code) && e.exitCustomTagDialog(), t.abrupt("return");
                                                case 11:
                                                    if ("Backspace" !== n.code && "Backspace" !== n.key) {
                                                        t.next = 19;
                                                        break
                                                    }
                                                    if (t.t0 = e.chat.selectRegionMerge() || e.chat.gridElmMerge() || e.chat.delMarkRule(), !t.t0) {
                                                        t.next = 17;
                                                        break
                                                    }
                                                    return n.preventDefault(), t.next = 17, e.richTextInput();
                                                case 17:
                                                    t.next = 34;
                                                    break;
                                                case 19:
                                                    if (!e.wrapKeyFun(n) && (e.deviceInfo.isPc || "Enter" !== n.key)) {
                                                        t.next = 26;
                                                        break
                                                    }
                                                    return n.preventDefault(), e.chat.setWrap(), t.next = 24, e.richTextInput();
                                                case 24:
                                                    t.next = 34;
                                                    break;
                                                case 26:
                                                    if (!e.sendKeyFun(n)) {
                                                        t.next = 33;
                                                        break
                                                    }
                                                    return n.preventDefault(), t.next = 30, u(100);
                                                case 30:
                                                    e.enterSend(), t.next = 34;
                                                    break;
                                                case 33:
                                                    ["ArrowLeft", "ArrowRight"].includes(n.code) ? (n.preventDefault(), e.chat.switchRange(n.code)) : n.ctrlKey && "KeyA" === n.code ? e.isEmpty() && n.preventDefault() : n.ctrlKey && "KeyZ" === n.code ? (n.preventDefault(), e.ruleChatEvent(e.undo, "defaultAction", "UNDO")) : n.ctrlKey && "KeyY" === n.code && (n.preventDefault(), e.ruleChatEvent(e.redo, "defaultAction", "REDO"));
                                                case 34:
                                                    -1 === ["Backspace", "Shift", "Tab", "CapsLock", "Control", "Meta", "Alt", "ContextMenu", "Enter", "NumpadEnter", "Escape", "ArrowLeft", "ArrowUp", "ArrowRight", "ArrowDown", "Home", "End", "PageUp", "PageDown", "Insert", "Delete", "NumLock"].indexOf(n.key) && !n.ctrlKey && !n.altKey && !n.metaKey && e.chat.selectRegionMerge();
                                                case 35:
                                                case"end":
                                                    return t.stop()
                                            }
                                        }), i)
                                    })));
                                    return function (e) {
                                        return i.apply(this, arguments)
                                    }
                                }()), this.richText.addEventListener("input", n(t().mark((function i() {
                                    return t().wrap((function (t) {
                                        for (; ;) switch (t.prev = t.next) {
                                            case 0:
                                                if (!e.isIMEModel) {
                                                    t.next = 3;
                                                    break
                                                }
                                                return e.isIMEModel = !1, e.chat.upDataNodeOrIndex(), e.isComposition || e.chat.updateGrid(), void 0 !== e.maxLength && e.ruleMaxLength(), e.showPlaceholder(), e.triggerChatEvent("operate"), t.abrupt("return");
                                            case 3:
                                                return t.next = 5, e.richTextInput();
                                            case 5:
                                                e.deviceInfo.isPc && !e.isComposition && e.debounceEvents.matchPointDialog();
                                            case 6:
                                            case"end":
                                                return t.stop()
                                        }
                                    }), i)
                                })))), this.richText.addEventListener("copy", (function (t) {
                                    t.preventDefault(), e.ruleChatEvent((function () {
                                        e.copyRange(t)
                                    }), "defaultAction", "COPY")
                                })), this.richText.addEventListener("cut", (function (t) {
                                    t.preventDefault(), e.ruleChatEvent((function () {
                                        e.copyRange(t), e.removeRange()
                                    }), "defaultAction", "CUT")
                                })), this.richText.addEventListener("paste", (function (i) {
                                    i.preventDefault(), e.ruleChatEvent((function () {
                                        var r = i.clipboardData.getData("text/plain");
                                        if ("string" == typeof r && "" !== r) {
                                            if (-1 === e.copyType.indexOf("text")) return;
                                            var a = document.createElement("div");
                                            a.innerHTML = i.clipboardData.getData("application/my-custom-format") || "", e.chat.selectRegionMerge(), a.children[0] && "richBox" === a.children[0].getAttribute("data-set-richType") ? e.insertInsideHtml(a.innerHTML) : (a.innerHTML = r, e.insertText(a.innerText)), a = null
                                        } else {
                                            if (-1 === e.copyType.indexOf("image")) return;
                                            var s = (i.clipboardData || i.originalEvent.clipboardData).items || [];
                                            Array.prototype.forEach.call(s, function () {
                                                var i = n(t().mark((function i(n) {
                                                    var r, a, s;
                                                    return t().wrap((function (t) {
                                                        for (; ;) switch (t.prev = t.next) {
                                                            case 0:
                                                                if (-1 !== n.type.indexOf("image")) {
                                                                    t.next = 2;
                                                                    break
                                                                }
                                                                return t.abrupt("return");
                                                            case 2:
                                                                if (r = n.getAsFile(), !e.uploadImage) {
                                                                    t.next = 10;
                                                                    break
                                                                }
                                                                return t.next = 6, e.uploadImage(r);
                                                            case 6:
                                                                a = t.sent, e.insertHtml('<img class="chat-img" src="'.concat(a, '" alt="" />')), t.next = 12;
                                                                break;
                                                            case 10:
                                                                (s = new FileReader).onload = function (t) {
                                                                    e.insertHtml('<img class="chat-img" src="'.concat(t.target.result, '" alt="" />'))
                                                                }, s.readAsDataURL(r);
                                                            case 12:
                                                            case"end":
                                                                return t.stop()
                                                        }
                                                    }), i)
                                                })));
                                                return function (e) {
                                                    return i.apply(this, arguments)
                                                }
                                            }())
                                        }
                                    }), "defaultAction", "PASTE")
                                })), this.richText.addEventListener("blur", (function (t) {
                                    e.chat.upDataNodeOrIndex()
                                })), this.richText.addEventListener("focus", (function (t) {
                                    e.chat.upDataNodeOrIndex()
                                })), this.richText.addEventListener("click", (function () {
                                    e.chat.upDataNodeOrIndex()
                                })), this.richText.addEventListener("dragstart", (function (e) {
                                    e.stopPropagation(), e.preventDefault()
                                })), this.richText.addEventListener("dragover", (function (e) {
                                    e.stopPropagation(), e.preventDefault()
                                })), this.richText.addEventListener("drop", (function (e) {
                                    e.stopPropagation(), e.preventDefault()
                                })), this.richText.addEventListener("compositionstart", (function () {
                                    e.isComposition = !0
                                })), this.richText.addEventListener("compositionend", (function () {
                                    e.isComposition = !1
                                }))
                            }
                        }, {
                            key: "otherInit", value: function (e) {
                                var t = this, i = d(e.needDebounce), n = function () {
                                    var e = t.chat.getRichTextNodeIndex(t.chat.vnode), i = e.gridIndex, n = e.markIndex;
                                    if (null !== i && null != n) {
                                        var r = {
                                            html: t.richText.innerHTML,
                                            gridIndex: i,
                                            markIndex: n,
                                            cursorIndex: t.chat.cursorIndex
                                        };
                                        t.undoHistory.push(r), t.undoHistory.length > 50 && t.undoHistory.shift()
                                    }
                                };
                                this.debounceEvents.recordHistory = i ? p(n, 200) : n;
                                var r = function (e) {
                                    var i = "0", n = "100%", r = t.chat.getRangeRect();
                                    if (r) {
                                        var a = t.pcElms.containerDialogElm.getBoundingClientRect(), s = r.x - a.x,
                                            o = a.y - r.y, c = e.clientWidth, l = e.clientHeight;
                                        r.x > window.innerWidth - c - 30 && (s = r.x - c - a.x - 16, i = "100%"), r.y < l && (o -= l, n = "0"), e.style.transform = "translate(0, 0)", e.style.transformOrigin = "".concat(i, " ").concat(n), e.style.left = s + 6 + "px", e.style.bottom = "".concat(o, "px"), e.style.opacity = "1"
                                    }
                                };
                                this.debounceEvents.dialogMoveToRange = i ? p(r, 120, !0) : r;
                                var a = function () {
                                    if (t.needDialog) {
                                        var e = t.chat.vnode.textContent || "", i = t.chat.cursorIndex,
                                            n = e.slice(0, i), r = -1, a = -1, s = "userTag";
                                        if (-1 !== n.lastIndexOf("@") && (r = n.lastIndexOf("@")), t.pcElms.customTagDialogTagKey && -1 !== n.lastIndexOf(t.pcElms.customTagDialogTagKey) && (a = n.lastIndexOf(t.pcElms.customTagDialogTagKey)), a > r && (s = "customTag"), "userTag" === s && t.asyncMatch) {
                                            if (r < 0) return void t.exitPointDialog();
                                            t.matchKey++;
                                            var o = t.matchKey;
                                            t.startOpenIndex = r + 1;
                                            var c = n.slice(t.startOpenIndex) || "";
                                            if (/\s/gi.test(c)) return void t.exitPointDialog();
                                            t.updateUserList([]), x(t.pcElms.pointDialogLoadingElm, !0, "flex"), x(t.pcElms.pointDialogEmptyElm), t.showPointDialog();
                                            var l = t.triggerChatEvent("atMatch", c);
                                            l && l instanceof Promise && l.then((function (e) {
                                                if (o === t.matchKey) {
                                                    if (x(t.pcElms.pointDialogLoadingElm), !e || e.length <= 0) return void x(t.pcElms.pointDialogEmptyElm, !0, "flex");
                                                    t.updateUserList(e), t.pcElms.pointDialogUsersElm && t.pcElms.pointDialogUsersElm.length > 0 && t.updatePointActiveUserElm(t.pcElms.pointDialogUsersElm[0].elm)
                                                }
                                            }))
                                        } else if (!("userTag" === s && t.userList.length <= 0 || "customTag" === s && t.customTags[t.pcElms.customTagDialogTagKey].length <= 0)) {
                                            var h = function () {
                                                "userTag" === s ? t.exitPointDialog() : t.exitCustomTagDialog()
                                            };
                                            if (r < 0 && a < 0) return t.exitPointDialog(), void t.exitCustomTagDialog();
                                            t.startOpenIndex = "userTag" === s ? r + 1 : a + 1;
                                            var u = new RegExp("^([".concat(t.chat.ZERO_WIDTH_KEY).concat(t.chat.VOID_KEY, "])+$"));
                                            if (!n || u.test(n) || i < t.startOpenIndex) h(); else {
                                                var d = n.slice(t.startOpenIndex) || "";
                                                if (/\s/gi.test(d)) h(); else if (d) if ("userTag" === s) {
                                                    var p = t.searchUserList(d);
                                                    p.length > 0 ? t.showPointDialog(p) : h()
                                                } else {
                                                    var m = t.customTags[t.pcElms.customTagDialogTagKey].filter((function (e) {
                                                        return g(e.name, e.pinyin || "", d)
                                                    }));
                                                    m.length > 0 ? t.showCustomTagDialog(t.pcElms.customTagDialogTagKey, m) : h()
                                                } else "userTag" === s ? t.showPointDialog() : t.showCustomTagDialog(t.pcElms.customTagDialogTagKey)
                                            }
                                        }
                                    }
                                };
                                this.debounceEvents.matchPointDialog = i ? p(a, 200) : a;
                                this.debounceEvents.movePointActiveUserElm = m((function (e) {
                                    if (t.pcElms.pointDialogActiveElm) {
                                        var i = 0, n = t.pcElms.pointDialogActiveElm.getAttribute("data-set-id");
                                        t.pcElms.pointDialogUsersElm.some((function (e) {
                                            var t = e.elm.getAttribute("data-set-id");
                                            return i = e.index, n === t
                                        }));
                                        var r, a = t.pcElms.pointDialogUsersElm.filter((function (e) {
                                            return -1 === e.elm.className.indexOf("user-no-match")
                                        })), s = a.map((function (e) {
                                            return e.index
                                        }));
                                        "down" === e ? r = i === a[a.length - 1].index ? a[0] : a[s.indexOf(i) + 1] : "up" === e && (r = i === a[0].index ? a[a.length - 1] : a[s.indexOf(i) - 1]), r && t.updatePointActiveUserElm(r.elm, !0)
                                    }
                                }), 80);
                                this.debounceEvents.moveCustomActiveTagElm = m((function (e) {
                                    if (t.pcElms.customTagDialogActiveElm) {
                                        var i, n = t.customTags[t.pcElms.customTagDialogTagKey].map((function (e) {
                                                return e.id
                                            })), r = t.pcElms.customTagDialogActiveElm.getAttribute("data-set-id"),
                                            a = n.indexOf(r),
                                            s = Array.prototype.map.call(t.pcElms.customTagDialogElms[t.pcElms.customTagDialogTagKey].children[1].children, (function (e, t) {
                                                return {elm: e, index: t}
                                            })).filter((function (e) {
                                                return -1 === e.elm.className.indexOf("tag-no-match")
                                            })), o = s.map((function (e) {
                                                return e.index
                                            }));
                                        "down" === e ? i = a === s[s.length - 1].index ? s[0] : s[o.indexOf(a) + 1] : "up" === e && (i = a === s[0].index ? s[s.length - 1] : s[o.indexOf(a) - 1]), i && t.updateActiveCustomTagElm(i.elm, !0)
                                    }
                                }), 80);
                                var s = {
                                    html: this.richText.innerHTML,
                                    gridIndex: 0,
                                    markIndex: 0,
                                    cursorIndex: this.chat.cursorIndex
                                };
                                this.undoHistory = [s]
                            }
                        }, {
                            key: "richTextInput", value: (A = n(t().mark((function e() {
                                var i, n, r = arguments;
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return i = !(r.length > 0 && void 0 !== r[0]) || r[0], this.chat.upDataNodeOrIndex(), this.deviceInfo.isPc && this.chat.selectRegionMerge(), e.next = 5, u(50);
                                        case 5:
                                            if (this.isComposition || this.chat.updateGrid(), (n = (this.richText.children[0] || {childNodes: []}).childNodes[0]) && n.getAttribute && "richMark" === n.getAttribute("data-set-richType")) {
                                                e.next = 10;
                                                break
                                            }
                                            return this.chat.textInnerHtmlInit(!0), this.showPlaceholder(), this.triggerChatEvent("operate"), e.abrupt("return");
                                        case 10:
                                            void 0 !== this.maxLength && this.ruleMaxLength(), this.showPlaceholder(), this.triggerChatEvent("operate"), i && this.doOverHistory && !this.isComposition && this.debounceEvents.recordHistory(), this.chat.viewIntoPoint();
                                        case 11:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function () {
                                return A.apply(this, arguments)
                            })
                        }, {
                            key: "copyRange", value: function (e) {
                                var t = window.getSelection();
                                if (t.isCollapsed || t.rangeCount <= 0) return e.clipboardData.setData("application/my-custom-format", ""), void e.clipboardData.setData("text/plain", "");
                                var i = t.toString() || "", n = document.createElement("div");
                                n.innerHTML = i;
                                var r = n.innerText.replace(/\n\n/g, "\n");
                                n = null, e.clipboardData.setData("text/plain", r);
                                var a = t.anchorNode, s = t.focusNode;
                                if (a !== s || a.nodeType !== Node.TEXT_NODE) if (a !== this.richText || s !== this.richText) {
                                    var o = this.chat.getWrapNode(a, !0), c = this.chat.getWrapNode(s, !0),
                                        l = this.chat.getMarkNode(a, !0), h = this.chat.getMarkNode(s, !0),
                                        u = "richMark" === l.getAttribute("data-set-richType"),
                                        d = "richMark" === h.getAttribute("data-set-richType"),
                                        p = Array.prototype.indexOf.call(o.childNodes, l),
                                        m = Array.prototype.indexOf.call(c.childNodes, h);
                                    if (o === c && o.parentNode === this.richText) {
                                        var g = p > m, f = Array.prototype.filter.call(o.childNodes, (function (e, t) {
                                                return g ? t < p && t > m : t > p && t < m
                                            })).map((function (e) {
                                                return e.cloneNode(!0)
                                            })),
                                            v = u ? g ? a.textContent.slice(0, t.anchorOffset) : a.textContent.slice(t.anchorOffset) : "",
                                            E = d ? g ? s.textContent.slice(t.focusOffset) : s.textContent.slice(0, t.focusOffset) : "",
                                            y = this.chat.getGridElm(!0), x = this.chat.getGridElm(!0);
                                        v && (y.childNodes[0].innerHTML = v, y.childNodes[0].setAttribute("data-set-empty", "false")), E && (x.childNodes[0].innerHTML = E, x.childNodes[0].setAttribute("data-set-empty", "false")), g ? (f.unshift(x), f.push(y)) : (f.unshift(y), f.push(x));
                                        var b = document.createElement("div"), k = this.chat.setWrapNodeByMark(f);
                                        return b.appendChild(k), e.clipboardData.setData("application/my-custom-format", b.innerHTML), void (b = null)
                                    }
                                    if (o.parentNode === this.richText && c.parentNode === this.richText) {
                                        var T = Array.prototype.indexOf.call(this.richText.childNodes, o),
                                            C = Array.prototype.indexOf.call(this.richText.childNodes, c), D = T > C,
                                            w = Array.prototype.filter.call(this.richText.childNodes, (function (e, t) {
                                                return D ? t < T && t > C : t > T && t < C
                                            })).map((function (e) {
                                                return e.cloneNode(!0)
                                            })),
                                            N = u ? D ? a.textContent.slice(0, t.anchorOffset) : a.textContent.slice(t.anchorOffset) : "",
                                            L = d ? D ? s.textContent.slice(t.focusOffset) : s.textContent.slice(0, t.focusOffset) : "",
                                            A = this.chat.getGridElm(!0), M = this.chat.getGridElm(!0);
                                        N && (A.childNodes[0].innerHTML = N, A.childNodes[0].setAttribute("data-set-empty", "false")), L && (M.childNodes[0].innerHTML = L, M.childNodes[0].setAttribute("data-set-empty", "false"));
                                        var I = Array.prototype.filter.call(o.childNodes, (function (e, t) {
                                            return D ? t < p : t > p
                                        })).map((function (e) {
                                            return e.cloneNode(!0)
                                        })), O = Array.prototype.filter.call(c.childNodes, (function (e, t) {
                                            return D ? t > m : t < m
                                        })).map((function (e) {
                                            return e.cloneNode(!0)
                                        }));
                                        if (D) {
                                            I.push(A), O.unshift(M);
                                            var S = this.chat.setWrapNodeByMark(I), P = this.chat.setWrapNodeByMark(O);
                                            w.push(S), w.unshift(P)
                                        } else {
                                            I.unshift(A), O.push(M);
                                            var H = this.chat.setWrapNodeByMark(I), K = this.chat.setWrapNodeByMark(O);
                                            w.unshift(H), w.push(K)
                                        }
                                        var R = document.createElement("div");
                                        return Array.prototype.forEach.call(w, (function (e) {
                                            R.appendChild(e)
                                        })), e.clipboardData.setData("application/my-custom-format", R.innerHTML), void (R = null)
                                    }
                                } else e.clipboardData.setData("application/my-custom-format", this.richText.innerHTML); else {
                                    var _ = a.textContent.slice(t.anchorOffset, t.focusOffset);
                                    e.clipboardData.setData("application/my-custom-format", _)
                                }
                            }
                        }, {
                            key: "removeRange", value: (L = n(t().mark((function e() {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return window.getSelection().getRangeAt(0).deleteContents(), e.next = 3, u(50);
                                        case 3:
                                            this.chat.updateGrid(), this.showPlaceholder();
                                        case 5:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function () {
                                return L.apply(this, arguments)
                            })
                        }, {
                            key: "updateUserList", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : void 0;
                                e && (this.userList = this.getRuleUserList(e)), this.needDialog && (this.deviceInfo.isPc ? this.updatePCUser() : this.updateH5User())
                            }
                        }, {
                            key: "getRuleUserList", value: function (e) {
                                var t = this;
                                this.targetUserList = JSON.parse(JSON.stringify(e || []));
                                var i = Object.create(null);
                                return i[this.userProps.id] = "isALL", i[this.userProps.name] = this.reviseLabels.callEveryLabel, this.targetUserList.unshift(i), (null == e ? void 0 : e.map((function (e, i) {
                                    var n = e[t.userProps.id];
                                    if (!n && 0 !== n) throw new Error("参数值userList：下标第".concat(i, "项").concat(t.userProps.id, "值异常！"));
                                    return {
                                        id: String(n),
                                        name: String(e[t.userProps.name] || ""),
                                        avatar: String(e[t.userProps.avatar] || ""),
                                        pinyin: String(e[t.userProps.pinyin] || "")
                                    }
                                }))) || []
                            }
                        }, {
                            key: "getUserHtmlTemplate", value: function (e, t) {
                                var i = document.createElement("span");
                                if (i.setAttribute("class", "call-user-dialog-item-sculpture ".concat(t.avatar ? "is-avatar" : "")), t.avatar) {
                                    var n = new Image;
                                    n.alt = "", n.src = String(t.avatar), i.appendChild(n)
                                } else i.innerHTML = '<span style="transform: scale(0.75)">'.concat(t.name.slice(-2), "</span>");
                                e.appendChild(i);
                                var r = document.createElement("span");
                                r.setAttribute("class", "call-user-dialog-item-name"), r.innerHTML = t.name, e.appendChild(r)
                            }
                        }, {
                            key: "hasPc", value: function () {
                                var e = this.richText.parentElement;
                                this.pcElms.containerDialogElm = document.createElement("div"), this.pcElms.containerDialogElm.className = "chat-dialog", e.nextElementSibling ? e.parentElement.insertBefore(this.pcElms.containerDialogElm, e.nextElementSibling) : e.parentElement.appendChild(this.pcElms.containerDialogElm), this.asyncMatch || this.initCheckDialog(), this.initPointDialog(), window.addEventListener("click", this.winClick), window.addEventListener("keydown", this.winKeydown)
                            }
                        }, {
                            key: "updatePCUser", value: function () {
                                var e = this;
                                this.pcElms.pointDialogMainElm.innerHTML = "", this.pcElms.pointDialogActiveElm = void 0;
                                var t = document.createDocumentFragment();
                                if (this.needCallEvery) {
                                    var i = document.createElement("div");
                                    i.setAttribute("class", "call-user-dialog-item"), i.setAttribute("data-set-id", "isALL"), this.userSelectStyleAndEvent(i, {
                                        id: "isALL",
                                        name: this.reviseLabels.callEveryLabel
                                    }), i.innerHTML = '\n          <span class="call-user-dialog-item-sculpture">\n            <span style="transform: scale(0.75)">@</span>\n          </span>\n          <span class="call-user-dialog-item-name">'.concat(this.reviseLabels.callEveryLabel, "(").concat(this.userList.length, ")</span>\n      "), t.appendChild(i)
                                }
                                if (this.userList.forEach((function (i) {
                                    var n = document.createElement("div");
                                    n.setAttribute("class", "call-user-dialog-item"), n.setAttribute("data-set-id", i.id), e.userSelectStyleAndEvent(n, i), e.getUserHtmlTemplate(n, i), t.appendChild(n)
                                })), this.pcElms.pointDialogMainElm.appendChild(t), this.pcElms.pointDialogUsersElm = [], Array.prototype.forEach.call(this.pcElms.pointDialogMainElm.children || [], (function (t, i) {
                                    e.pcElms.pointDialogUsersElm.push({index: i, elm: t})
                                })), !this.asyncMatch) {
                                    this.pcElms.checkDialogUsersElm.innerHTML = '\n                    <div class="checkbox-dialog-check-item" data-set-value="ALL">\n                        <input type="checkbox" value>\n                        <span class="checkbox-dialog-check-item-inner"></span>\n                        <div class="checkbox-dialog-check-item-label">'.concat(this.reviseLabels.checkAllLabel, "</div>\n                    </div>");
                                    var n = document.createDocumentFragment();
                                    this.userList.forEach((function (t) {
                                        var i = document.createElement("div");
                                        i.setAttribute("class", "checkbox-dialog-check-item"), i.setAttribute("data-set-value", t.id), i.innerHTML = '\n          <input type="checkbox" value>\n          <span class="checkbox-dialog-check-item-inner"></span>\n      ', e.getUserHtmlTemplate(i, t), n.appendChild(i)
                                    })), this.pcElms.checkDialogUsersElm.appendChild(n), this.pcElms.checkDialogUsersElm && this.pcElms.checkDialogUsersElm.children.length && Array.prototype.forEach.call(this.pcElms.checkDialogUsersElm.children, (function (t) {
                                        t.onclick = function () {
                                            var i = t.getAttribute("data-set-value") || "",
                                                n = e.userList.find((function (e) {
                                                    return e.id === i
                                                })), r = -1 === t.className.indexOf("checkbox-dialog-check-item-check");
                                            "ALL" === i ? e.checkboxRows = r ? e.userList.map((function (e) {
                                                return e
                                            })) : [] : r ? e.checkboxRows.push(n) : e.checkboxRows = e.checkboxRows.filter((function (e) {
                                                return e.id !== i
                                            })), e.updateCheckDialogTags()
                                        }
                                    }));
                                    var r = document.createDocumentFragment();
                                    this.userList.forEach((function (t) {
                                        var i = document.createElement("div");
                                        i.setAttribute("class", "checkbox-dialog-check-item"), i.setAttribute("data-set-id", t.id);
                                        var n = document.createElement("div");
                                        n.setAttribute("class", "checkbox-dialog-check-item-label"), e.getUserHtmlTemplate(n, t), i.appendChild(n), i.onclick = function () {
                                            x(e.pcElms.checkDialogSearchResultElm);
                                            var t = i.getAttribute("data-set-id") || "";
                                            if (e.pcElms.checkDialogSearchInputElm.value = "", e.pcElms.checkDialogSearchInputElm.focus(), !e.checkboxRows.some((function (e) {
                                                return e.id === t
                                            }))) {
                                                var n = e.userList.find((function (e) {
                                                    return e.id === t
                                                }));
                                                n && e.checkboxRows.push(n), e.updateCheckDialogTags()
                                            }
                                        }, r.appendChild(i)
                                    }));
                                    var a = document.createElement("div");
                                    a.setAttribute("class", "checkbox-dialog-search-empty"), a.innerText = this.reviseLabels.searchEmptyLabel, r.appendChild(a), this.pcElms.checkDialogSearchResultElm.appendChild(r)
                                }
                            }
                        }, {
                            key: "searchUserList", value: function (e) {
                                return this.userList.filter((function (t) {
                                    return g(t.name, t.pinyin || "", e)
                                }))
                            }
                        }, {
                            key: "userSelectStyleAndEvent", value: function (e, i) {
                                var r = this;
                                e.addEventListener("click", function () {
                                    var a = n(t().mark((function n(a) {
                                        var s;
                                        return t().wrap((function (t) {
                                            for (; ;) switch (t.prev = t.next) {
                                                case 0:
                                                    if (a.stopPropagation(), r.updatePointActiveUserElm(e), !r.isPointSearchMode && !r.asyncMatch) {
                                                        t.next = 5;
                                                        break
                                                    }
                                                    return t.next = 3, r.matchSetTag(i);
                                                case 3:
                                                    t.next = 8;
                                                    break;
                                                case 5:
                                                    return s = r.targetUserList.find((function (e) {
                                                        return String(e[r.userProps.id]) === i.id
                                                    })), t.next = 8, r.onceSetTag(s);
                                                case 8:
                                                    r.exitPointDialog();
                                                case 9:
                                                case"end":
                                                    return t.stop()
                                            }
                                        }), n)
                                    })));
                                    return function (e) {
                                        return a.apply(this, arguments)
                                    }
                                }())
                            }
                        }, {
                            key: "initCheckDialog", value: function () {
                                var e = this;
                                this.pcElms.checkDialogElm = document.createElement("div"), this.pcElms.checkDialogElm.setAttribute("class", "checkbox-dialog"), x(this.pcElms.checkDialogElm), this.pcElms.checkDialogElm.innerHTML = '\n      <div class="checkbox-dialog-container">\n        <div class="checkbox-dialog-container-header">\n            <span>选择要@的人</span>\n            <span class="checkbox-dialog-container-header-close">⛌</span>\n        </div>\n        <div class="checkbox-dialog-container-body">\n            <div class="checkbox-dialog-left-box">\n                <div class="checkbox-dialog-search">\n                    <input class="checkbox-dialog-search-input" placeholder="搜索人员名称" type="text">\n                    <div class="checkbox-dialog-search-group"></div>\n                </div>\n                <div class="checkbox-dialog-tags"></div>\n                <div class="checkbox-dialog-option">\n                    <button class="checkbox-dialog-option-btn btn-submit disabled">确定</button>\n                    <button class="checkbox-dialog-option-btn btn-close">取消</button>\n                </div>\n            </div>\n            <div class="checkbox-dialog-right-box">\n                <div class="checkbox-dialog-right-box-title">研讨成员列表</div>\n                <div class="checkbox-dialog-check-group"></div>\n            </div>\n        </div>\n      </div>\n    ', this.pcElms.containerDialogElm.appendChild(this.pcElms.checkDialogElm), this.pcElms.checkDialogUsersElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-check-group"), this.pcElms.checkDialogSearchResultElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-search-group"), this.pcElms.checkDialogSearchInputElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-search-input"), this.pcElms.checkDialogTagsElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-tags");
                                var i = function () {
                                        x(e.pcElms.checkDialogElm), k(document.body, "disable-scroll")
                                    },
                                    r = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-container-header-close"),
                                    a = this.pcElms.checkDialogElm.querySelector(".btn-close");
                                r.onclick = i, a.onclick = i;
                                var s = this.pcElms.checkDialogElm.querySelector(".btn-submit");
                                s.onclick = n(t().mark((function n() {
                                    var r;
                                    return t().wrap((function (t) {
                                        for (; ;) switch (t.prev = t.next) {
                                            case 0:
                                                if (-1 === s.className.indexOf("disabled")) {
                                                    t.next = 2;
                                                    break
                                                }
                                                return t.abrupt("return");
                                            case 2:
                                                return r = e.checkboxRows.map((function (t) {
                                                    var i = Object.create(null);
                                                    return i[e.userProps.id] = t.id, i[e.userProps.name] = t.name, i
                                                })), t.next = 5, e.batchSetTag(r);
                                            case 5:
                                                i();
                                            case 6:
                                            case"end":
                                                return t.stop()
                                        }
                                    }), n)
                                }))), x(this.pcElms.checkDialogSearchResultElm), this.pcElms.checkDialogSearchResultElm.onclick = function (e) {
                                    e.stopPropagation()
                                }, this.pcElms.checkDialogSearchInputElm.onclick = function (e) {
                                    e.stopPropagation()
                                };
                                var o = p((function (t) {
                                    var i = String(t.target.value || "").replace(/'/g, "");
                                    if (i) {
                                        var n = e.searchUserList(i).map((function (e) {
                                            return e.id
                                        }));
                                        Array.prototype.forEach.call(e.pcElms.checkDialogSearchResultElm.children, (function (t, i) {
                                            if (i === e.pcElms.checkDialogSearchResultElm.children.length - 1) x(t, 0 === n.length); else {
                                                var r = t.getAttribute("data-set-id");
                                                x(t, -1 !== n.indexOf(r), "flex")
                                            }
                                        })), x(e.pcElms.checkDialogSearchResultElm, !0)
                                    } else x(e.pcElms.checkDialogSearchResultElm)
                                }), 200);
                                this.pcElms.checkDialogSearchInputElm.oninput = o, this.pcElms.checkDialogSearchInputElm.onfocus = o
                            }
                        }, {
                            key: "updateCheckDialogTags", value: function () {
                                var e = this, t = this.checkboxRows.map((function (e) {
                                    return e.id
                                })), i = [], n = [], r = document.createElement("div");
                                r.className = "check-empty", r.innerHTML = "\n        ".concat('<svg class="check-empty-svg" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse fill="#f5f5f5" cx="32" cy="33" rx="32" ry="7"></ellipse><g fill-rule="nonzero" stroke="#d9d9d9"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" fill="#fafafa"></path></g></g></svg>', '\n        <span class="check-empty-label">').concat(this.reviseLabels.checkEmptyLabel, "</span>\n    "), Array.prototype.forEach.call(this.pcElms.checkDialogTagsElm.children, (function (e) {
                                    var r = e.getAttribute("data-set-value");
                                    -1 === t.indexOf(r) ? n.push(e) : i.push(r)
                                })), Array.prototype.forEach.call(this.pcElms.checkDialogUsersElm.children, (function (i, n) {
                                    if (0 !== n) {
                                        var r = i.getAttribute("data-set-value");
                                        k(i, "checkbox-dialog-check-item-check", -1 !== t.indexOf(r))
                                    } else k(i, "checkbox-dialog-check-item-check", t.length === e.userList.length)
                                })), n.forEach((function (t) {
                                    e.pcElms.checkDialogTagsElm.removeChild(t)
                                })), k(this.pcElms.checkDialogElm.querySelector(".btn-submit"), "disabled", t.length <= 0), t.length || this.pcElms.checkDialogTagsElm.appendChild(r);
                                var a = this.checkboxRows.filter((function (e) {
                                    return -1 === i.indexOf(e.id)
                                }));
                                if (a.length) {
                                    var s = document.createDocumentFragment();
                                    a.forEach((function (t) {
                                        var i = document.createElement("div");
                                        i.setAttribute("class", "checkbox-dialog-tag-item"), i.setAttribute("data-set-value", t.id), i.innerHTML = "\n        <span>".concat(t.name, "</span>\n      ");
                                        var n = document.createElement("span");
                                        n.setAttribute("class", "checkbox-dialog-tag-item-close"), n.innerHTML = "⛌", n.onclick = function () {
                                            var t = i.getAttribute("data-set-value");
                                            e.checkboxRows = e.checkboxRows.filter((function (e) {
                                                return e.id !== t
                                            })), e.updateCheckDialogTags()
                                        }, i.appendChild(n), s.appendChild(i)
                                    })), this.pcElms.checkDialogTagsElm.appendChild(s)
                                }
                            }
                        }, {
                            key: "showPCCheckDialog", value: function () {
                                !this.needDialog || this.asyncMatch || (this.winClick(), this.checkboxRows = [], x(this.pcElms.checkDialogElm, !0), k(document.body, "disable-scroll", !0), this.pcElms.checkDialogTagsElm.scrollTop = 0, this.pcElms.checkDialogUsersElm.scrollTop = 0, this.pcElms.checkDialogSearchInputElm.value = "", this.updateCheckDialogTags(), this.isExternalCallPopup = !0)
                            }
                        }, {
                            key: "initPointDialog", value: function () {
                                var e = this;
                                this.pcElms.pointDialogElm = document.createElement("div"), this.pcElms.pointDialogElm.setAttribute("class", "call-user-dialog"), x(this.pcElms.pointDialogElm);
                                var t = document.createElement("div");
                                t.setAttribute("class", "call-user-dialog-header"), t.innerHTML = '<span class="call-user-dialog-header-title">群成员</span>', this.pcElms.pointDialogCheckElm = document.createElement("span"), this.pcElms.pointDialogCheckElm.setAttribute("class", "call-user-dialog-header-check"), this.pcElms.pointDialogCheckElm.innerText = "多选", this.pcElms.pointDialogCheckElm.onclick = function () {
                                    e.showPCCheckDialog(), e.isExternalCallPopup = !1
                                }, t.appendChild(this.pcElms.pointDialogCheckElm), this.pcElms.pointDialogElm.appendChild(t), this.pcElms.pointDialogMainElm = document.createElement("div"), this.pcElms.pointDialogMainElm.setAttribute("class", "call-user-dialog-main"), this.pcElms.pointDialogElm.appendChild(this.pcElms.pointDialogMainElm), this.asyncMatch && (this.pcElms.pointDialogLoadingElm = document.createElement("div"), this.pcElms.pointDialogLoadingElm.setAttribute("class", "call-user-dialog-loading"), this.pcElms.pointDialogLoadingElm.innerHTML = C, this.pcElms.pointDialogElm.appendChild(this.pcElms.pointDialogLoadingElm), x(this.pcElms.pointDialogLoadingElm), this.pcElms.pointDialogEmptyElm = document.createElement("div"), this.pcElms.pointDialogEmptyElm.setAttribute("class", "call-user-dialog-empty"), this.pcElms.pointDialogEmptyElm.innerHTML = "\n        ".concat('<svg class="match-empty-svg" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse fill="#f5f5f5" cx="32" cy="33" rx="32" ry="7"></ellipse><g fill-rule="nonzero" stroke="#d9d9d9"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" fill="#fafafa"></path></g></g></svg>', '\n        <span class="empty-label">暂无数据</span>\n      '), this.pcElms.pointDialogElm.appendChild(this.pcElms.pointDialogEmptyElm), x(this.pcElms.pointDialogEmptyElm)), this.pcElms.containerDialogElm.appendChild(this.pcElms.pointDialogElm)
                            }
                        }, {
                            key: "ruleShowPointDialog", value: function () {
                                this.needDialog && this.userList.length > 0 && this.chat.showAt() && (this.isExternalCallPopup = !1, this.showPointDialog())
                            }
                        }, {
                            key: "showPointDialog", value: function (e) {
                                this.exitCustomTagDialog(), this.exitPointDialog(), this.isPointSearchMode = !!e;
                                var t = null;
                                this.pcElms.pointDialogUsersElm.forEach((function (i) {
                                    var n = i.elm, r = n.getAttribute("data-set-id"), a = e && e.every((function (e) {
                                        return e.id !== r
                                    }));
                                    !t && !a && (t = n), k(n, "user-no-match", a)
                                })), null !== t && this.updatePointActiveUserElm(t), x(this.pcElms.pointDialogCheckElm, !this.asyncMatch && !this.isPointSearchMode), x(this.pcElms.pointDialogElm, !0), this.debounceEvents.dialogMoveToRange(this.pcElms.pointDialogElm), this.pcElms.pointDialogMainElm.scrollTop = 0
                            }
                        }, {
                            key: "updatePointActiveUserElm", value: function (e) {
                                var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                                if (this.pcElms.pointDialogActiveElm && k(this.pcElms.pointDialogActiveElm, "call-user-dialog-item-active"), this.pcElms.pointDialogActiveElm = e, e && (k(e, "call-user-dialog-item-active", !0), t)) {
                                    var i = Array.prototype.filter.call(this.pcElms.pointDialogMainElm.children, (function (e) {
                                            return -1 === e.className.indexOf("user-no-match")
                                        })), n = e.clientHeight,
                                        r = Array.prototype.indexOf.call(i, e) + 1 - Math.ceil(Math.floor(this.pcElms.pointDialogMainElm.clientHeight / n) / 2);
                                    this.pcElms.pointDialogMainElm.scrollTop = r > 0 ? r * n : 0
                                }
                            }
                        }, {
                            key: "exitPointDialog", value: function () {
                                this.updatePointActiveUserElm(), this.asyncMatch && this.updateUserList([]), x(this.pcElms.pointDialogElm)
                            }
                        }, {
                            key: "showPCPointDialog", value: (N = n(t().mark((function e() {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            if (e.t0 = this.needDialog, !e.t0) {
                                                e.next = 9;
                                                break
                                            }
                                            return this.insertText("@"), window.removeEventListener("click", this.winClick), this.asyncMatch && x(this.pcElms.pointDialogEmptyElm, !0, "flex"), this.showPointDialog(), e.next = 8, u(50);
                                        case 8:
                                            window.addEventListener("click", this.winClick);
                                        case 9:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function () {
                                return N.apply(this, arguments)
                            })
                        }, {
                            key: "bindCustomTrigger", value: function (e) {
                                var t = this;
                                Object.values(this.pcElms.customTagDialogElms).forEach((function (e) {
                                    t.pcElms.containerDialogElm.removeChild(e)
                                })), this.pcElms.customTagDialogElms = {}, this.customTags = {}, e.forEach((function (e) {
                                    e.tagList && e.tagList.length > 0 && (t.customTags[e.prefix] = e.tagList.map((function (e) {
                                        return {id: String(e.id), name: String(e.name), pinyin: String(e.pinyin || "")}
                                    })), t.initCustomTagDialog(e))
                                }))
                            }
                        }, {
                            key: "initCustomTagDialog", value: function (e) {
                                var i = this, r = document.createElement("div");
                                r.setAttribute("class", "call-tag-dialog"), x(r);
                                var a = document.createElement("div");
                                a.setAttribute("class", "call-tag-dialog-header"), a.innerHTML = '<span class="call-tag-dialog-header-title">'.concat(e.dialogTitle || e.prefix, "</span>"), r.appendChild(a);
                                var s = document.createElement("div");
                                s.setAttribute("class", "call-tag-dialog-main"), e.tagList.forEach((function (e) {
                                    var r = document.createElement("div");
                                    r.setAttribute("class", "call-tag-dialog-item"), r.setAttribute("data-set-id", e.id);
                                    var a = document.createElement("span");
                                    a.setAttribute("class", "call-tag-dialog-item-name"), a.innerHTML = e.name, r.appendChild(a), r.addEventListener("click", function () {
                                        var a = n(t().mark((function n(a) {
                                            return t().wrap((function (t) {
                                                for (; ;) switch (t.prev = t.next) {
                                                    case 0:
                                                        if (a.stopPropagation(), i.updateActiveCustomTagElm(r), !i.isPointSearchMode) {
                                                            t.next = 7;
                                                            break
                                                        }
                                                        return t.next = 5, i.matchSetCustomTag(e);
                                                    case 5:
                                                        t.next = 9;
                                                        break;
                                                    case 7:
                                                        return t.next = 9, i.onceSetCustomTag(e);
                                                    case 9:
                                                        i.exitCustomTagDialog();
                                                    case 10:
                                                    case"end":
                                                        return t.stop()
                                                }
                                            }), n)
                                        })));
                                        return function (e) {
                                            return a.apply(this, arguments)
                                        }
                                    }()), s.appendChild(r)
                                })), r.appendChild(s), this.pcElms.containerDialogElm.appendChild(r), this.pcElms.customTagDialogElms[e.prefix] = r
                            }
                        }, {
                            key: "showCustomTagDialog", value: function (e, t) {
                                this.exitCustomTagDialog(), this.exitPointDialog(), this.isPointSearchMode = !!t, this.pcElms.customTagDialogTagKey = e;
                                var i = this.pcElms.customTagDialogElms[e], n = i.children[1], r = null;
                                Array.prototype.forEach.call(n.children, (function (e) {
                                    var i = e.getAttribute("data-set-id"), n = t && t.every((function (e) {
                                        return e.id !== i
                                    }));
                                    !r && !n && (r = e), k(e, "tag-no-match", n)
                                })), null !== r && this.updateActiveCustomTagElm(r), x(i, !0), this.debounceEvents.dialogMoveToRange(i), i.children[1].scrollTop = 0
                            }
                        }, {
                            key: "updateActiveCustomTagElm", value: function (e) {
                                var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
                                if (this.pcElms.customTagDialogActiveElm && k(this.pcElms.customTagDialogActiveElm, "call-tag-dialog-item-active"), this.pcElms.customTagDialogActiveElm = e, e && (k(e, "call-tag-dialog-item-active", !0), t)) {
                                    var i = this.pcElms.customTagDialogElms[this.pcElms.customTagDialogTagKey].children[1],
                                        n = Array.prototype.filter.call(i.children, (function (e) {
                                            return -1 === e.className.indexOf("tag-no-match")
                                        })), r = e.clientHeight,
                                        a = Array.prototype.indexOf.call(n, e) + 1 - Math.ceil(Math.floor(i.clientHeight / r) / 2);
                                    i.scrollTop = a > 0 ? a * r : 0
                                }
                            }
                        }, {
                            key: "exitCustomTagDialog", value: function () {
                                for (var e in this.updateActiveCustomTagElm(), this.pcElms.customTagDialogElms) x(this.pcElms.customTagDialogElms[e])
                            }
                        }, {
                            key: "showPCCustomTagDialog", value: (w = n(t().mark((function e(i) {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            if (e.t0 = !this.needDialog || this.asyncMatch, e.t0) {
                                                e.next = 8;
                                                break
                                            }
                                            return this.insertText(i), window.removeEventListener("click", this.winClick), this.showCustomTagDialog(i), e.next = 7, u(50);
                                        case 7:
                                            window.addEventListener("click", this.winClick);
                                        case 8:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return w.apply(this, arguments)
                            })
                        }, {
                            key: "hasH5", value: function () {
                                var i = this;
                                this.h5Elms.dialogElm = document.createElement("div"), this.h5Elms.dialogElm.setAttribute("class", "call-user-popup"), this.h5Elms.dialogElm.innerHTML = '\n      <div class="call-user-popup-main">\n        <div class="call-user-popup-header">\n            <span class="popup-show">收起</span>\n            <span class="popup-title">选择提醒的人</span>\n            <span class="popup-check">确定</span>\n        </div>\n        <div class="call-user-popup-search">\n            '.concat('<svg class="icon-search" style="vertical-align: middle;fill: currentColor;overflow: hidden;" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M684.8 223.530667a326.272 326.272 0 0 1 24.96 433.621333c2.645333 2.133333 5.290667 4.48 7.850667 7.04L870.4 817.066667c24.789333 24.746667 32.896 56.832 18.133333 71.594666-14.762667 14.805333-46.848 6.656-71.637333-18.090666l-152.789333-152.832a106.282667 106.282667 0 0 1-7.210667-7.936 326.101333 326.101333 0 0 1-433.109333-25.173334c-127.445333-127.445333-127.573333-333.952-0.256-461.269333 127.36-127.36 333.866667-127.232 461.269333 0.213333zM275.328 275.114667a252.885333 252.885333 0 0 0 0.256 357.632 252.885333 252.885333 0 0 0 357.632 0.256 252.885333 252.885333 0 0 0-0.256-357.632 252.885333 252.885333 0 0 0-357.632-0.256z" fill="#9B9B9B"></path></svg>', '\n            <input class="call-user-popup-search-input"\n                   placeholder="搜索人员名称"\n                   type="text">\n        </div>\n        <div class="call-user-popup-body"></div>\n      </div>\n    ');
                                var r = function () {
                                    var e = n(t().mark((function e() {
                                        return t().wrap((function (e) {
                                            for (; ;) switch (e.prev = e.next) {
                                                case 0:
                                                    return i.h5Elms.dialogElm.className = i.h5Elms.dialogElm.className.replace(/ chat-view-show/g, " chat-view-hidden"), i.h5Elms.dialogSearchElm.value = "", e.next = 4, u(260);
                                                case 4:
                                                    x(i.h5Elms.dialogElm), k(document.body, "disable-scroll"), i.asyncMatch && i.updateUserList([]), i.chat.restCursorPos(i.chat.vnode, i.chat.cursorIndex), i.chat.viewIntoPoint();
                                                case 9:
                                                case"end":
                                                    return e.stop()
                                            }
                                        }), e)
                                    })));
                                    return function () {
                                        return e.apply(this, arguments)
                                    }
                                }();
                                this.h5Elms.dialogElm.onclick = r;
                                var a = this.h5Elms.dialogElm.querySelector(".call-user-popup-main");
                                a.onclick = function (e) {
                                    e.stopPropagation()
                                }, this.h5Elms.dialogShowElm = this.h5Elms.dialogElm.querySelector(".popup-show"), this.h5Elms.dialogShowElm.onclick = r, this.h5Elms.dialogCheckElm = this.h5Elms.dialogElm.querySelector(".popup-check"), this.h5Elms.dialogCheckElm.onclick = n(t().mark((function n() {
                                    var a, s, o;
                                    return t().wrap((function (t) {
                                        for (; ;) switch (t.prev = t.next) {
                                            case 0:
                                                if (-1 === i.h5Elms.dialogCheckElm.className.indexOf("disabled")) {
                                                    t.next = 2;
                                                    break
                                                }
                                                return t.abrupt("return");
                                            case 2:
                                                if (0 !== (a = i.h5Elms.dialogElm.querySelectorAll(".user-popup-check-item-check") || []).length) {
                                                    t.next = 7;
                                                    break
                                                }
                                                return t.next = 6, r();
                                            case 6:
                                            case 12:
                                                return t.abrupt("return");
                                            case 7:
                                                if (!Array.prototype.some.call(a, (function (e) {
                                                    return "isALL" === e.getAttribute("data-set-id")
                                                }))) {
                                                    t.next = 13;
                                                    break
                                                }
                                                return t.next = 10, i.onceSetTag(e(e({}, i.userProps.id, "isALL"), i.userProps.name, i.reviseLabels.callEveryLabel));
                                            case 10:
                                                return t.next = 12, r();
                                            case 13:
                                                return s = Array.prototype.map.call(a, (function (e) {
                                                    return e.getAttribute("data-set-id")
                                                })), o = i.targetUserList.filter((function (e) {
                                                    return -1 !== s.indexOf(String(e[i.userProps.id]))
                                                })), t.next = 16, i.batchSetTag(o);
                                            case 16:
                                                return t.next = 18, r();
                                            case 18:
                                            case"end":
                                                return t.stop()
                                        }
                                    }), n)
                                }))), this.h5Elms.dialogMainElm = this.h5Elms.dialogElm.querySelector(".call-user-popup-body"), this.updateUserList(), this.h5Elms.dialogEmptyElm = document.createElement("div"), this.h5Elms.dialogEmptyElm.className = "call-user-popup-empty", this.h5Elms.dialogEmptyElm.innerHTML = "\n        ".concat('<svg class="empty-svg" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse fill="#f5f5f5" cx="32" cy="33" rx="32" ry="7"></ellipse><g fill-rule="nonzero" stroke="#d9d9d9"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" fill="#fafafa"></path></g></g></svg>', '\n        <span class="empty-label">').concat(this.reviseLabels.searchEmptyLabel, "</span>\n    "), x(this.h5Elms.dialogEmptyElm), a.appendChild(this.h5Elms.dialogEmptyElm), this.asyncMatch && (this.h5Elms.dialogLoadingElm = document.createElement("div"), this.h5Elms.dialogLoadingElm.className = "call-user-popup-loading", this.h5Elms.dialogLoadingElm.innerHTML = C, x(this.h5Elms.dialogLoadingElm), a.appendChild(this.h5Elms.dialogLoadingElm)), this.h5Elms.dialogSearchElm = this.h5Elms.dialogElm.querySelector(".call-user-popup-search-input"), this.h5Elms.dialogSearchElm.oninput = p((function (e) {
                                    var t = String(e.target.value || "").replace(/'/g, "");
                                    if (i.asyncMatch) {
                                        i.matchKey++;
                                        var n = i.matchKey;
                                        i.updateUserList([]), x(i.h5Elms.dialogLoadingElm, !0), x(i.h5Elms.dialogEmptyElm);
                                        var r = i.triggerChatEvent("atMatch", t);
                                        r && r instanceof Promise && r.then((function (e) {
                                            if (n === i.matchKey) {
                                                if (x(i.h5Elms.dialogLoadingElm), !e || e.length <= 0) return void x(i.h5Elms.dialogEmptyElm, !0, "flex");
                                                i.updateUserList(e)
                                            }
                                        }))
                                    } else {
                                        var a = [];
                                        Array.prototype.forEach.call(i.h5Elms.dialogMainElm.children, (function (e) {
                                            if (!t) return x(e, !0, "flex"), void a.push(e);
                                            var i = e.getAttribute("data-set-name") || "",
                                                n = e.getAttribute("data-set-pinyin") || "";
                                            g(i, n, t) ? (x(e, !0, "flex"), a.push(e)) : x(e)
                                        })), x(i.h5Elms.dialogEmptyElm, !a.length, "flex")
                                    }
                                }), 200), x(this.h5Elms.dialogElm), document.body.appendChild(this.h5Elms.dialogElm)
                            }
                        }, {
                            key: "updateH5User", value: function () {
                                var e = this;
                                this.h5Elms.dialogMainElm.innerHTML = "";
                                var t = this.userList && this.userList.length > 0,
                                    i = document.createDocumentFragment(), n = document.createElement("span");
                                if (n.innerHTML = '\n        <input type="checkbox" value>\n        <span class="user-popup-check-item-inner"></span>\n    ', t) {
                                    var r = document.createElement("div");
                                    this.needCallEvery && (r.setAttribute("class", "call-user-popup-item"), r.setAttribute("data-set-id", "isALL"), r.innerHTML = '\n          <span class="call-user-dialog-item-sculpture">\n            <span style="transform: scale(0.75)">@</span>\n          </span>\n          <span class="call-user-dialog-item-name">'.concat(this.reviseLabels.callEveryLabel, "(").concat(this.userList.length, ")</span>\n      "), r.appendChild(n.cloneNode(!0)), r.onclick = function () {
                                        var t = -1 === r.className.indexOf("user-popup-check-item-check");
                                        Array.prototype.forEach.call(e.h5Elms.dialogMainElm.children, (function (e) {
                                            k(e, "user-popup-check-item-check", t)
                                        })), k(e.h5Elms.dialogCheckElm, "disabled", !t)
                                    }, i.appendChild(r)), this.userList.forEach((function (t, a) {
                                        var s = document.createElement("div");
                                        s.setAttribute("class", "call-user-popup-item"), s.setAttribute("data-set-id", t.id), s.setAttribute("data-set-name", t.name), s.setAttribute("data-set-pinyin", t.pinyin || ""), e.getUserHtmlTemplate(s, t), s.appendChild(n.cloneNode(!0)), i.appendChild(s), s.onclick = function (t) {
                                            var i = -1 === s.className.indexOf("user-popup-check-item-check");
                                            k(s, "user-popup-check-item-check", i);
                                            var n = Array.prototype.every.call(e.h5Elms.dialogMainElm.children, (function (e) {
                                                return -1 !== e.className.indexOf("user-popup-check-item-check") || "isALL" === e.getAttribute("data-set-id")
                                            }));
                                            k(r, "user-popup-check-item-check", n);
                                            var a = Array.prototype.some.call(e.h5Elms.dialogMainElm.children, (function (e) {
                                                return -1 !== e.className.indexOf("user-popup-check-item-check")
                                            }));
                                            k(e.h5Elms.dialogCheckElm, "disabled", !a)
                                        }
                                    }))
                                }
                                this.h5Elms.dialogMainElm.appendChild(i)
                            }
                        }, {
                            key: "showH5Dialog", value: function () {
                                this.richText && this.richText.blur(), Array.prototype.forEach.call(this.h5Elms.dialogMainElm.children, (function (e) {
                                    e.style.display = "", k(e, "user-popup-check-item-check")
                                })), k(this.h5Elms.dialogCheckElm, "disabled", !0), x(this.h5Elms.dialogElm, !0), k(document.body, "disable-scroll", !0), this.asyncMatch && x(this.h5Elms.dialogEmptyElm, !0, "flex"), this.h5Elms.dialogMainElm.scrollTop = 0, this.isExternalCallPopup = !0
                            }
                        }, {
                            key: "updateConfig", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    t = e.copyType, i = e.userProps, n = e.userList, r = e.uploadImage,
                                    a = e.needCallEvery, s = e.placeholder, o = e.maxLength, c = e.needCallSpace,
                                    l = e.wrapKeyFun, h = e.sendKeyFun, u = e.customTrigger;
                                t && (this.copyType = t), i && (this.userProps = Object.assign({}, {
                                    id: "id",
                                    name: "name",
                                    avatar: "avatar",
                                    pinyin: "pinyin"
                                }, i)), r && (this.uploadImage = r), s && (this.placeholderElm.innerText = s), void 0 !== o && (this.maxLength = o, this.ruleMaxLength()), (this.asyncMatch || void 0 !== a || n) && (this.needCallEvery = !this.asyncMatch && d(a), this.updateUserList(this.asyncMatch ? [] : n)), void 0 !== c && this.chat.setCallSpace(d(c)), l && (this.wrapKeyFun = l), h && (this.sendKeyFun = h), this.needDialog && u && this.deviceInfo.isPc && this.bindCustomTrigger(u)
                            }
                        }, {
                            key: "enterSend", value: function () {
                                this.triggerChatEvent("enterSend")
                            }
                        }, {
                            key: "insertHtml", value: function (e) {
                                var t = document.createElement("span");
                                t.innerHTML = e, t.className = "chat-set-html";
                                var i = this.chat.createNewDom(t);
                                return this.chat.replaceRegContent(i, !1), this.richTextInput(), i
                            }
                        }, {
                            key: "insertInsideHtml", value: function (e) {
                                var t = this, i = document.createElement("div");
                                if (i.innerHTML = e, i.children.length) {
                                    var n = this.chat.vnode, r = this.chat.getWrapNode(n);
                                    if (1 === i.children.length) this.chat.gridMerge(r, i.children[0], !0); else {
                                        this.chat.gridMerge(r, i.children[0], !0);
                                        var a = Array.prototype.slice.call(i.children, 1), s = r;
                                        Array.prototype.forEach.call(a, (function (e, i) {
                                            if (s.parentElement ? t.richText.insertBefore(e, s.nextElementSibling) : t.richText.appendChild(e), s = e, i === a.length - 1) {
                                                var n = e.childNodes[e.childNodes.length - 1].childNodes[0].childNodes[0];
                                                t.chat.restCursorPos(n, n.textContent.length)
                                            }
                                        }))
                                    }
                                    i = null, this.richTextInput()
                                }
                            }
                        }, {
                            key: "insertText", value: function (e) {
                                var t, i, n = this;
                                if (e) {
                                    var r = new RegExp("".concat(this.chat.ZERO_WIDTH_KEY).concat(this.chat.VOID_KEY), "ig"),
                                        a = e.replace(r, "");
                                    if (a) {
                                        var s, o = 0, c = this.chat.vnode;
                                        c && c.parentElement && "richInput" === c.parentElement.getAttribute("data-set-richType") ? (s = c.parentElement, o = c.textContent === this.chat.VOID_KEY ? 1 : this.chat.cursorLeft) : (s = null == (i = null == (t = this.richText) ? void 0 : t.lastChild) ? void 0 : i.lastChild, o = s.childNodes[0].textContent.length);
                                        var l, h = function (e) {
                                            var t = s.childNodes[0], i = t.textContent.split("");
                                            return i.splice(o, 0, e), t.textContent = i.join("").replace(new RegExp(n.chat.VOID_KEY, "g"), (function () {
                                                return o--, ""
                                            })), s.setAttribute("data-set-empty", "false"), s.childNodes[1] && s.removeChild(s.childNodes[1]), n.chat.restCursorPos(t, o + e.length), s.parentElement.parentElement
                                        }, u = a.split("\n");
                                        if (u.length > 1) u.forEach((function (e, t) {
                                            l = 0 === t ? h(e) : function (e) {
                                                var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                                                    i = arguments.length > 2 ? arguments[2] : void 0,
                                                    r = n.chat.getGridElm(), a = r.childNodes[0].childNodes[0], s = 1;
                                                return e && (a.innerHTML = e, a.setAttribute("data-set-empty", "false"), s = e.length), i.nextSibling ? n.richText.insertBefore(r, i.nextSibling) : n.richText.appendChild(r), t && n.chat.restCursorPos(a.childNodes[0], s), r
                                            }(e, t === u.length - 1, l)
                                        })); else h(a);
                                        this.richTextInput()
                                    }
                                }
                            }
                        }, {
                            key: "getCallUserList", value: function () {
                                var e = this.richText.querySelectorAll(".at-user");
                                if (e && e.length > 0) {
                                    var t = Array.prototype.map.call(e, (function (e) {
                                        return e.dataset.userId
                                    }));
                                    return T(this.targetUserList, t, this.userProps.id)
                                }
                                return []
                            }
                        }, {
                            key: "getCallUserTagList", value: function () {
                                var t = this, i = this.richText.querySelectorAll(".at-user");
                                if (i && i.length > 0) {
                                    var n = [];
                                    return Array.prototype.forEach.call(i, (function (i) {
                                        n.some((function (e) {
                                            return e[t.userProps.id] === i.dataset.userId
                                        })) || n.push(e(e({}, t.userProps.id, i.dataset.userId), t.userProps.name, i.innerText.slice(1)))
                                    })), n
                                }
                                return []
                            }
                        }, {
                            key: "getCustomTagList", value: function () {
                                var e = this, t = Object.keys(this.customTags), i = {},
                                    n = this.richText.querySelectorAll(".at-tag");
                                return t.forEach((function (t) {
                                    var r = Array.prototype.filter.call(n, (function (e) {
                                        return e.getAttribute("data-set-prefix") === String(t)
                                    })).map((function (e) {
                                        return e.getAttribute("data-tag-id")
                                    }));
                                    r = r.filter((function (e, t) {
                                        return r.indexOf(e) === t
                                    })), i[t] = T(e.customTags[t], r, "id")
                                })), i
                            }
                        }, {
                            key: "clear", value: function (e) {
                                this.chat.textInnerHtmlInit(!0, e);
                                var t = {
                                    html: this.richText.innerHTML,
                                    gridIndex: 0,
                                    markIndex: 0,
                                    cursorIndex: this.chat.cursorIndex
                                };
                                this.undoHistory = [t], this.redoHistory = [], this.richTextInput(!1)
                            }
                        }, {
                            key: "isEmpty", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                                if ((this.richText.querySelectorAll(".chat-tag") || []).length > 0) return !1;
                                var t = new RegExp("^(".concat(this.chat.ZERO_WIDTH_KEY, "|<br>|").concat(this.chat.VOID_KEY, ")+$")),
                                    i = this.richText.querySelectorAll(".chat-grid-input") || [];
                                return e ? Array.prototype.every.call(i, (function (e) {
                                    return !e.innerHTML || !e.innerText || !e.innerText.trim() || t.test(e.innerHTML)
                                })) : Array.prototype.every.call(i, (function (e) {
                                    return !e.innerHTML || !e.innerText || t.test(e.innerHTML)
                                }))
                            }
                        }, {
                            key: "showPlaceholder", value: function () {
                                x(this.placeholderElm, this.isEmpty())
                            }
                        }, {
                            key: "getReduceNode", value: function () {
                                var e = this, t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                                    i = Object.assign({}, {
                                        needUserId: !1,
                                        needTagId: !1,
                                        wrapClassName: void 0,
                                        rowClassName: void 0,
                                        imgToText: !1,
                                        identifyLink: !1
                                    }, t),
                                    n = /(https?|http|ftp|file):\/\/[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]/g,
                                    r = this.richText.cloneNode(!0).querySelectorAll(".chat-grid-wrap") || [],
                                    a = document.createElement("div");
                                return i.wrapClassName && (a.className = i.wrapClassName), Array.prototype.forEach.call(r, (function (t, r) {
                                    var s = t.querySelectorAll(".chat-stat") || [], o = document.createElement("p");
                                    i.rowClassName && (o.className = i.rowClassName), Array.prototype.forEach.call(s, (function (t) {
                                        e.chat.getNodeEmpty(t) || (t.removeAttribute("data-set-richType"), t.removeAttribute("contenteditable"), t.removeAttribute("data-set-empty"), i.needUserId || t.removeAttribute("data-user-id"), i.needTagId || t.removeAttribute("data-tag-id"), i.imgToText && t.firstChild && "IMG" === t.firstChild.tagName && (t.className += " img-to-text", t.innerHTML = "[".concat(t.firstChild.getAttribute("data-img-text") || "元素data-img-text未定义", "]")), i.identifyLink && -1 !== t.className.indexOf("chat-grid-input") && (t.innerHTML = t.innerHTML.replace(n, (function (e) {
                                            return '<a class="chat-grid-link" href="'.concat(e, '" target="_blank">').concat(e, "</a>")
                                        }))), o.appendChild(t))
                                    })), o.innerHTML || (o.innerHTML = "<br>"), a.appendChild(o)
                                })), a
                            }
                        }, {
                            key: "getText", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {}, t = "",
                                    i = this.getReduceNode(e);
                                return Array.prototype.forEach.call(i.children, (function (e, i) {
                                    t = t + (i > 0 ? "\n" : "") + e.textContent
                                })), t
                            }
                        }, {
                            key: "getHtml", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                return this.getReduceNode(e).innerHTML
                            }
                        }, {
                            key: "dispose", value: function () {
                                var e = this.richText.parentElement;
                                if (e && (e.removeChild(this.richText), e.removeChild(this.placeholderElm)), this.needDialog) if (this.deviceInfo.isPc) {
                                    var t = this.pcElms.containerDialogElm.parentElement;
                                    t && t.removeChild(this.pcElms.containerDialogElm), window.removeEventListener("click", this.winClick), window.removeEventListener("keydown", this.winKeydown)
                                } else document.body.removeChild(this.h5Elms.dialogElm);
                                for (var i in this) delete this[i];
                                Object.setPrototypeOf(this, Object)
                            }
                        }, {
                            key: "setUserTag", value: (D = n(t().mark((function e(i) {
                                var n;
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return n = this.chat.createChatTagElm({
                                                id: i[this.userProps.id],
                                                name: i[this.userProps.name]
                                            }, "@", "at-user", "user-id"), this.chat.replaceRegContent(n, !1), e.next = 4, this.richTextInput();
                                        case 4:
                                            this.triggerChatEvent("atCheck", [i]);
                                        case 5:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return D.apply(this, arguments)
                            })
                        }, {
                            key: "onceSetTag", value: (y = n(t().mark((function e(i) {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.chat.onceCall({
                                                id: i[this.userProps.id],
                                                name: i[this.userProps.name]
                                            });
                                        case 2:
                                            return e.next = 4, this.richTextInput();
                                        case 4:
                                            this.triggerChatEvent("atCheck", [i]);
                                        case 5:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return y.apply(this, arguments)
                            })
                        }, {
                            key: "batchSetTag", value: (E = n(t().mark((function e(i) {
                                var n, r;
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            for (n = [], r = 0; r <= i.length - 1;) n.push({
                                                id: i[r][this.userProps.id],
                                                name: i[r][this.userProps.name]
                                            }), r++;
                                            return e.next = 4, this.chat.batchReplaceRegContent(n, !this.isExternalCallPopup);
                                        case 4:
                                            return e.next = 6, this.richTextInput();
                                        case 6:
                                            this.triggerChatEvent("atCheck", i);
                                        case 7:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return E.apply(this, arguments)
                            })
                        }, {
                            key: "matchSetTag", value: (v = n(t().mark((function e(i) {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.chat.onceSearchCall(i, this.startOpenIndex);
                                        case 2:
                                            return e.next = 4, this.richTextInput();
                                        case 4:
                                            this.triggerChatEvent("atCheck", [i]);
                                        case 5:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return v.apply(this, arguments)
                            })
                        }, {
                            key: "onceSetCustomTag", value: (f = n(t().mark((function e(i) {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.chat.onceCustomCall(i, !0, this.pcElms.customTagDialogTagKey);
                                        case 2:
                                            return e.next = 4, this.richTextInput();
                                        case 4:
                                            this.triggerChatEvent("tagCheck", i);
                                        case 5:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return f.apply(this, arguments)
                            })
                        }, {
                            key: "matchSetCustomTag", value: (h = n(t().mark((function e(i) {
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.chat.onceCustomCall(i, this.startOpenIndex, this.pcElms.customTagDialogTagKey);
                                        case 2:
                                            return e.next = 4, this.richTextInput();
                                        case 4:
                                            this.triggerChatEvent("tagCheck", i);
                                        case 5:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return h.apply(this, arguments)
                            })
                        }, {
                            key: "disabled", value: function () {
                                this.richText.setAttribute("contenteditable", "false"), k(this.richText, "chat-rich-text-disabled", !0)
                            }
                        }, {
                            key: "enable", value: function () {
                                this.richText.setAttribute("contenteditable", "true"), this.richText.className = this.richText.className.replace(/ chat-rich-text-disabled/g, ""), this.chat.setRangeLastText()
                            }
                        }, {
                            key: "undo", value: (c = n(t().mark((function e() {
                                var i, n;
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            if (this.doOverHistory && this.undoHistory && !(this.undoHistory.length <= 1)) {
                                                e.next = 2;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 2:
                                            return i = this.undoHistory[this.undoHistory.length - 2], n = this.undoHistory[this.undoHistory.length - 1], this.redoHistory.push(n), this.undoHistory.pop(), e.next = 7, this.setChatHistory(i);
                                        case 7:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function () {
                                return c.apply(this, arguments)
                            })
                        }, {
                            key: "redo", value: (a = n(t().mark((function e() {
                                var i;
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            if (this.doOverHistory && this.redoHistory && !(this.redoHistory.length < 1)) {
                                                e.next = 2;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 2:
                                            return i = this.redoHistory[this.redoHistory.length - 1], this.redoHistory.pop(), this.undoHistory.push(i), e.next = 7, this.setChatHistory(i);
                                        case 7:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function () {
                                return a.apply(this, arguments)
                            })
                        }, {
                            key: "ruleMaxLength", value: function () {
                                var e = this;
                                if (this.isEmpty() || void 0 === this.maxLength) this.textLength = 0; else {
                                    var t = 0, i = 0, n = [];
                                    Array.prototype.some.call(this.richText.children, (function (r, a) {
                                        var s = e.getGirdNodeTextInfo(r), o = s.nodeInfos, c = s.nodeTextLength;
                                        if (t += c, n.push(o), i = a, t >= e.maxLength) return !0
                                    }));
                                    var r = [];
                                    Array.prototype.forEach.call(this.richText.children, (function (e, t) {
                                        t > i && r.push(e)
                                    })), r.forEach((function (t) {
                                        return e.richText.removeChild(t)
                                    })), this.deepDelGirdText(n, t)
                                }
                            }
                        }, {
                            key: "getGirdNodeTextInfo", value: function (e) {
                                var t = this, i = [], n = 0;
                                if (1 === e.children.length && e !== e.parentElement.children[0]) {
                                    var r = e.children[0],
                                        a = (r.textContent || "").replace(new RegExp(this.chat.VOID_KEY, "g"), "");
                                    n += a.length || 1, i[0] = {node: r, textLength: a.length || 1, type: "richMark"}
                                } else Array.prototype.forEach.call(e.children, (function (e, r) {
                                    if ("richMark" === e.getAttribute("data-set-richType")) {
                                        var a = (e.textContent || "").replace(new RegExp(t.chat.VOID_KEY, "g"), "");
                                        n += a.length, i[r] = {node: e, textLength: a.length, type: "richMark"}
                                    } else {
                                        var s = (e.textContent || "").replace(new RegExp(t.chat.VOID_KEY, "g"), "");
                                        n += s.length || 1, i[r] = {node: e, textLength: s.length || 1, type: "chatTag"}
                                    }
                                }));
                                return {nodeInfos: i, nodeTextLength: n}
                            }
                        }, {
                            key: "deepDelGirdText", value: function (e, t) {
                                if (t > this.maxLength) {
                                    var i = e[e.length - 1];
                                    e.pop(), this.deepDelNode(i, e, t)
                                } else this.textLength = t
                            }
                        }, {
                            key: "deepDelNode", value: function (e, t, i) {
                                var n = e[0].node.parentElement;
                                if (i > this.maxLength) {
                                    var r = i - this.maxLength, a = e[e.length - 1];
                                    if ("richMark" === a.type) if (0 === a.textLength || r >= a.textLength) n.removeChild(a.node), e.pop(), r -= a.textLength, a = e[e.length - 1], n.removeChild(a.node), e.pop(), r -= a.textLength; else {
                                        var s = a.node.childNodes[0];
                                        s.textContent = s.textContent.slice(0, a.textLength - r), 0 === s.textContent && (s.setAttribute("data-set-empty", "true"), s.innerHTML = "".concat(this.chat.VOID_KEY, "<br>")), r = 0
                                    } else n.removeChild(a.node), e.pop(), r -= a.textLength;
                                    r > 0 ? e.length > 0 ? this.deepDelNode(e, t, r + this.maxLength) : (this.richText.appendChild(n), this.deepDelGirdText(t, r + this.maxLength)) : (this.textLength = this.maxLength + r, this.enable())
                                }
                            }
                        }, {
                            key: "setChatHistory", value: (i = n(t().mark((function e(i) {
                                var n, r, a, s, o;
                                return t().wrap((function (e) {
                                    for (; ;) switch (e.prev = e.next) {
                                        case 0:
                                            return this.doOverHistory = !1, n = i.html, r = i.gridIndex, a = i.markIndex, s = i.cursorIndex, this.richText.innerHTML = n, o = this.richText.childNodes[r].childNodes[a].childNodes[0].childNodes[0], this.chat.restCursorPos(o, s), e.next = 7, this.richTextInput();
                                        case 7:
                                            this.doOverHistory = !0;
                                        case 8:
                                        case"end":
                                            return e.stop()
                                    }
                                }), e, this)
                            }))), function (e) {
                                return i.apply(this, arguments)
                            })
                        }, {
                            key: "revisePCPointDialogLabel", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                if (this.needDialog) {
                                    var t = Object.assign({}, {
                                        title: "群成员",
                                        checkLabel: "多选",
                                        emptyLabel: "暂无数据"
                                    }, e);
                                    this.pcElms.pointDialogElm.querySelector(".call-user-dialog-header-title").innerText = t.title, this.pcElms.pointDialogCheckElm.innerText = t.checkLabel, this.pcElms.pointDialogEmptyElm && (this.pcElms.pointDialogEmptyElm.children[1].innerText = t.emptyLabel)
                                }
                            }
                        }, {
                            key: "revisePCCheckDialogLabel", value: function () {
                                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                                if (this.needDialog && !this.asyncMatch) {
                                    var t = Object.assign({}, {
                                        title: "选择要@的人",
                                        searchPlaceholder: "搜素人员名称",
                                        searchEmptyLabel: "没有匹配到任何结果",
                                        userTagTitle: "研讨成员列表",
                                        checkAllLabel: "全选",
                                        checkEmptyLabel: "请选择需要@的成员",
                                        confirmLabel: "确定",
                                        cancelLabel: "取消"
                                    }, e);
                                    this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-container-header").children[0].innerText = t.title, this.pcElms.checkDialogSearchInputElm.setAttribute("placeholder", t.searchPlaceholder || ""), this.reviseLabels.searchEmptyLabel = t.searchEmptyLabel || "", this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-search-empty").innerText = this.reviseLabels.searchEmptyLabel, this.reviseLabels.checkEmptyLabel = t.checkEmptyLabel || "", this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-right-box-title").innerText = t.userTagTitle, this.reviseLabels.checkAllLabel = t.checkAllLabel || "", this.pcElms.checkDialogUsersElm.children[0].children[2].innerText = this.reviseLabels.checkAllLabel, this.pcElms.checkDialogElm.querySelector(".btn-submit").innerText = t.confirmLabel, this.pcElms.checkDialogElm.querySelector(".btn-close").innerText = t.cancelLabel
                                }
                            }
                        }, {
                            key: "reviseH5DialogLabel", value: function (e) {
                                if (this.needDialog) {
                                    var t = Object.assign({}, {
                                        title: "选择提醒的人",
                                        searchPlaceholder: "搜素人员名称",
                                        searchEmptyLabel: "没有匹配到任何结果",
                                        confirmLabel: "确定",
                                        cancelLabel: "收起"
                                    }, e);
                                    this.h5Elms.dialogElm.querySelector(".popup-title").innerText = t.title, this.h5Elms.dialogSearchElm.setAttribute("placeholder", t.searchPlaceholder || ""), this.reviseLabels.searchEmptyLabel = t.searchEmptyLabel || "", this.h5Elms.dialogEmptyElm.children[1].innerText = this.reviseLabels.searchEmptyLabel, this.h5Elms.dialogCheckElm.innerText = t.confirmLabel, this.h5Elms.dialogShowElm.innerText = t.cancelLabel
                                }
                            }
                        }, {
                            key: "reverseAnalysis", value: function (e, t) {
                                var i = this, n = document.createElement("div");
                                n.innerHTML = e;
                                var r = n.children;
                                Array.prototype.forEach.call(r, (function (e) {
                                    e.className = "chat-grid-wrap", e.setAttribute("data-set-richType", "richBox");
                                    var t = e.children, n = {}, r = [];
                                    for (var a in Array.prototype.forEach.call(t, (function (a, s) {
                                        if (-1 !== a.className.indexOf("chat-grid-input")) {
                                            var o = a.innerText;
                                            return a.className = "", a.setAttribute("data-set-richType", "richMark"), void (a.innerHTML = '<span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="false">'.concat(o, "</span>"))
                                        }
                                        if ("BR" === a.tagName) {
                                            var c = i.chat.getGridElm(!0);
                                            return e.removeChild(a), void e.appendChild(c)
                                        }
                                        var l = a.cloneNode(!0);
                                        l.setAttribute("contenteditable", "false");
                                        var h = document.createElement("span");
                                        h.className = "chat-tag", h.setAttribute("contenteditable", "false"), h.setAttribute("data-set-richType", "chatTag"), h.appendChild(l), n[s] = h, s !== t.length - 1 ? -1 === t[s + 1].className.indexOf("chat-grid-input") && r.push(s) : r.push(s), 0 === s && r.push(-1)
                                    })), n) {
                                        var s = Number(a);
                                        s === t.length - 1 ? (e.removeChild(t[s]), e.appendChild(n[a])) : (e.insertBefore(n[a], t[s + 1]), e.removeChild(t[s]))
                                    }
                                    var o = [], c = e.children;
                                    r.forEach((function (e) {
                                        e === c.length - 1 ? o.push("isEnd") : o.push(c[e + 1])
                                    })), o.forEach((function (t) {
                                        var n = i.chat.getGridElm(!0);
                                        if ("isEnd" === t) e.appendChild(n); else {
                                            var r = n.children[0];
                                            r.removeChild(r.childNodes[1]), e.insertBefore(n, t)
                                        }
                                    }))
                                })), t ? (this.enable(), this.insertInsideHtml(n.innerHTML)) : (this.richText.innerHTML = n.innerHTML, this.enable(), this.richTextInput())
                            }
                        }, {
                            key: "addEventListener", value: function (e, t) {
                                this.chatEventModule[e].push(t)
                            }
                        }, {
                            key: "removeEventListener", value: function (e, t) {
                                var i = this.chatEventModule[e], n = i.indexOf(t);
                                -1 !== n && i.splice(n, 1)
                            }
                        }, {
                            key: "triggerChatEvent", value: function (e) {
                                for (var t = arguments.length, i = new Array(t > 1 ? t - 1 : 0), n = 1; n < t; n++) i[n - 1] = arguments[n];
                                var r;
                                return this.chatEventModule[e].forEach((function (e) {
                                    e && (r ? e.apply(void 0, i) : r = e.apply(void 0, i))
                                })), r
                            }
                        }, {
                            key: "ruleChatEvent", value: function (e, t) {
                                for (var i = arguments.length, n = new Array(i > 2 ? i - 2 : 0), r = 2; r < i; r++) n[r - 2] = arguments[r];
                                "PREVENT" !== (this.triggerChatEvent.apply(this, [t].concat(n)) + "").toUpperCase() && (e && e.bind(this)(), e = null)
                            }
                        }]);
                        var i, a, c, h, f, v, E, y, D, w, N, L, A
                    }();
                if (!window) throw new Error("非web环境！");
                window.console && window.console.log && console.log(" %c ".concat("ChatArea", " %c v5.1.2 "), "background: #269AFF; color: #FFFFFF; padding: 4px 0; border-radius: 4px 0px 0px 4px; font-style: italic;", "background: #FFFFFF; color: #269AFF; padding: 2px 0; border-radius: 0px 4px 4px 0px; font-style: italic; border: 2px solid #269AFF;"), window.ChatArea = D;
                i("C", window.ChatArea)
            }
        }
    }))
}();
