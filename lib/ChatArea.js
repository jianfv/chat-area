var Y = Object.defineProperty;
var B = (m, t, e) => t in m ? Y(m, t, {enumerable: !0, configurable: !0, writable: !0, value: e}) : m[t] = e;
var E = (m, t, e) => (B(m, typeof t != "symbol" ? t + "" : t, e), e);
(function () {
    const t = document.createElement("link").relList;
    if (t && t.supports && t.supports("modulepreload")) return;
    for (const a of document.querySelectorAll('link[rel="modulepreload"]')) i(a);
    new MutationObserver(a => {
        for (const s of a) if (s.type === "childList") for (const n of s.addedNodes) n.tagName === "LINK" && n.rel === "modulepreload" && i(n)
    }).observe(document, {childList: !0, subtree: !0});

    function e(a) {
        const s = {};
        return a.integrity && (s.integrity = a.integrity), a.referrerPolicy && (s.referrerPolicy = a.referrerPolicy), a.crossOrigin === "use-credentials" ? s.credentials = "include" : a.crossOrigin === "anonymous" ? s.credentials = "omit" : s.credentials = "same-origin", s
    }

    function i(a) {
        if (a.ep) return;
        a.ep = !0;
        const s = e(a);
        fetch(a.href, s)
    }
})();

class ${constructor(t){E(this,"richText");E(this,"vnode");E(this,"cursorIndex");E(this,"cursorLeft");E(this,"needCallSpace",!1);E(this,"VOID_KEY","\uFEFF");E(this,"ZERO_WIDTH_KEY","​");this.richText=t,this.textInnerHtmlInit(),this.richText.focus()}
textInnerHtmlInit

(t = !1, e)
{
    if (t || this.getNodeEmpty(this.richText)) {
        this.richText.innerHTML = "";
        const i = this.getGridElm();
        this.richText.appendChild(i);
        const a = i.children[0].children[0];
        e && (a.innerHTML = e, a.setAttribute("data-set-empty", "false"));
        const s = a.childNodes[0];
        this.restCursorPos(s, s.textContent === this.VOID_KEY ? 1 : s.textContent.length)
    }
}
onceCall(t)
{
    return new Promise(e => {
        const i = this.createChatTagElm(t, "@", "at-user", "user-id");
        this.replaceRegContent(i), e()
    })
}
onceSearchCall(t, e)
{
    return new Promise(i => {
        const a = this.createChatTagElm(t, "@", "at-user", "user-id");
        this.replaceRegContent(a, e), i()
    })
}
onceCustomCall(t, e, i)
{
    return new Promise(a => {
        const s = this.createChatTagElm(t, i, "at-tag", "tag-id");
        s.children[0].setAttribute("data-set-prefix", i), this.replaceRegContent(s, e), a()
    })
}
upDataNodeOrIndex()
{
    var n, l, o;
    const {focusNode: t, focusOffset: e, anchorOffset: i} = window.getSelection(),
        a = (t == null ? void 0 : t.parentNode) || void 0;
    !a || !a.getAttribute || a.getAttribute("data-set-richType") !== "richInput" || ((o = (l = (n = t == null ? void 0 : t.parentNode) == null ? void 0 : n.parentNode) == null ? void 0 : l.parentNode) == null ? void 0 : o.parentNode) !== this.richText || (this.vnode = t, this.cursorIndex = e, this.cursorLeft = i < e ? i : e)
}
showAt()
{
    if (this.upDataNodeOrIndex(), !this.vnode || this.vnode.nodeType !== Node.TEXT_NODE) return !1;
    const t = this.vnode.textContent || "", e = /@([^@\s]*)$/, i = t.slice(0, this.cursorIndex), a = e.exec(i);
    return a && a.length === 2 && i[i.length - 1] === "@"
}
getRangeRect()
{
    let t = 0, e = 0;
    const i = window.getSelection();
    if (i.focusNode.nodeType !== Node.TEXT_NODE) return null;
    const a = i.getRangeAt(0).getClientRects()[0];
    return a && (t = a.x, e = a.y), {x: t, y: e}
}
createChatTagElm(t, e, i, a)
{
    const s = document.createElement("span");
    return s.className = i, s.setAttribute(`data-${a}`, String(t.id)), s.contentEditable = "false", s.textContent = `${e}${t.name}${this.needCallSpace ? " " : ""}`, this.createNewDom(s)
}
createNewDom(t)
{
    const e = document.createElement("span");
    return e.className = "chat-tag", e.setAttribute("contenteditable", "false"), e.setAttribute("data-set-richType", "chatTag"), t.className += " chat-stat", e.appendChild(t), e
}
restCursorPos(t, e)
{
    e == null ? e = t.textContent === this.VOID_KEY ? 1 : 0 : e > t.textContent.length && (e = t.textContent.length);
    const i = new Range;
    i.setStart(t, e), i.setEnd(t, e);
    const a = window.getSelection();
    a && (this.vnode = t, this.cursorIndex = e, this.cursorLeft = e, a.removeAllRanges(), a.addRange(i))
}
replaceRegContent(t, e = !0)
{
    const i = this.vnode.textContent;
    let a;
    typeof e == "boolean" ? a = i.slice(0, e ? this.cursorIndex - 1 : this.cursorIndex) : a = i.slice(0, e - 1), a.length === 0 ? (this.vnode.parentElement.setAttribute("data-set-empty", !0), this.vnode.textContent = this.VOID_KEY) : this.vnode.textContent = a;
    let s = i.slice(this.cursorIndex);
    const n = this.vnode.parentNode.parentNode, l = n.nextSibling;
    l ? n.parentNode.insertBefore(t, l) : n.parentNode.appendChild(t);
    const r = t.previousSibling.childNodes[0], d = r.childNodes[1];
    d && r.removeChild(d);
    const h = this.getGridElm(!0), c = h.childNodes[0];
    s && s !== this.VOID_KEY && (c.setAttribute("data-set-empty", "false"), c.innerHTML = s);
    const u = c.childNodes[1];
    t.nextSibling ? (u && c.removeChild(u), n.parentNode.insertBefore(h, t.nextSibling)) : n.parentNode.appendChild(h), this.restCursorPos(c.childNodes[0])
}
batchReplaceRegContent(t = [], e = !0)
{
    return new Promise(i => {
        const a = this.vnode.textContent, s = a.slice(0, e ? this.cursorIndex - 1 : this.cursorIndex);
        s.length === 0 ? (this.vnode.parentElement.setAttribute("data-set-empty", !0), this.vnode.textContent = this.VOID_KEY) : this.vnode.textContent = s;
        let n = a.slice(this.cursorIndex), l = "";
        t.forEach((u, p) => {
            const g = p === t.length - 1 && n && n !== this.VOID_KEY;
            l += `
          <span class="chat-tag" contenteditable="false" data-set-richType="chatTag"><span class="at-user chat-stat" data-user-id="${u.id}" contentEditable="false">@${u.name}${this.needCallSpace ? " " : ""}</span></span>
          ${g ? `<span data-set-richType="richMark"><span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="false">${n}</span></span>` : `<span data-set-richType="richMark"><span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="true">${this.VOID_KEY}</span></span>`}
        `
        });
        const o = document.createElement("div");
        o.innerHTML = l;
        const r = [];
        Array.from(o.children).forEach(u => {
            r.push(u)
        });
        const d = this.vnode.parentNode.parentNode, h = d.nextSibling, c = r[r.length - 1];
        if (h) r.forEach(u => {
            d.parentNode.insertBefore(u, h)
        }); else {
            const u = c.childNodes[0];
            u.getAttribute("data-set-empty") === "true" && (u.innerHTML = `${this.VOID_KEY}<br>`), r.forEach(p => {
                d.parentNode.appendChild(p)
            })
        }
        this.restCursorPos(c.childNodes[0].childNodes[0]), i()
    })
}
switchRange(t)
{
    var n, l;
    let {focusNode: e, focusOffset: i} = window.getSelection();
    e.getAttribute && e.getAttribute("data-set-richType") === "richInput" && (e = e.childNodes[0]);
    let a, s;
    if (e.nodeType === Node.TEXT_NODE) {
        const o = e.textContent.length, r = e.parentNode.parentNode;
        switch (t) {
            case"ArrowLeft":
                if (i > 0 && e.textContent !== this.VOID_KEY) {
                    s = i - 1, a = e;
                    break
                }
                const d = (n = r == null ? void 0 : r.previousSibling) == null ? void 0 : n.previousSibling;
                if (d) a = d.childNodes[0].childNodes[0], s = a.textContent.length; else {
                    const c = r.parentNode.previousSibling;
                    c && (a = c.lastChild.childNodes[0].childNodes[0], s = a.textContent.length)
                }
                break;
            case"ArrowRight":
                if (i < o && e.textContent !== this.VOID_KEY) {
                    s = i + 1, a = e;
                    break
                }
                const h = (l = r == null ? void 0 : r.nextSibling) == null ? void 0 : l.nextSibling;
                if (h) a = h.childNodes[0].childNodes[0], s = a.textContent === this.VOID_KEY ? 1 : 0; else {
                    const c = r.parentNode.nextSibling;
                    c && (a = c.childNodes[0].childNodes[0].childNodes[0], s = a.textContent === this.VOID_KEY ? 1 : 0)
                }
                break
        }
    }
    (s || s === 0) && this.restCursorPos(a, s)
}
getGridElm(t = !1)
{
    const e = document.createElement("span");
    if (e.setAttribute("data-set-richType", "richMark"), e.innerHTML = `<span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="true">${this.VOID_KEY}<br></span>`, t) return e;
    const i = document.createElement("p");
    return i.className = "chat-grid-wrap", i.setAttribute("data-set-richType", "richBox"), i.appendChild(e), i
}
updateGrid()
{
    const t = window.getSelection(), e = t.focusNode, i = e.parentNode, a = i.getAttribute("data-set-richType");
    let s, n, l, o;
    switch (a) {
        case"richAllBox":
            if (s = e.childNodes[t.focusOffset], !s || s.getAttribute("data-set-richType") === "chatTag") {
                const p = this.getGridElm(!0), g = p.children[0];
                s ? (g.removeChild(g.childNodes[1]), e.insertBefore(p, s)) : e.appendChild(p), this.restCursorPos(g.childNodes[0]);
                break
            }
            if (s.tagName === "BR") {
                const p = this.getGridElm(!0), g = p.children[0];
                e.insertBefore(p, s), e.removeChild(s), this.restCursorPos(g.childNodes[0], g.childNodes[0].textContent.length)
            }
            break;
        case"richMark":
            const r = i.parentNode, d = Array.prototype.indexOf.call(r.childNodes, i);
            if (d === -1) break;
            if (d === 0) {
                const p = t.focusNode;
                p.setAttribute("data-set-empty", "true"), p.innerHTML = `${this.VOID_KEY}<br>`, s = p.childNodes[0], this.restCursorPos(s, s.textContent.length);
                break
            }
            let h = i.previousSibling, c;
            h.getAttribute("data-set-richType") === "chatTag" ? (c = h.previousSibling, r.removeChild(h), r.removeChild(i)) : (c = i.previousSibling, r.removeChild(i)), s = c.childNodes[0].childNodes[0], s.textContent === this.VOID_KEY && s.parentNode.appendChild(document.createElement("br")), this.restCursorPos(s, s.textContent.length);
            break;
        case"richInput":
            if (o = i.parentNode, l = o.parentNode, this.getNodeEmpty(i)) {
                i.setAttribute("data-set-empty", "true"), l.childNodes[l.childNodes.length - 1] === o && (i.innerHTML = `${this.VOID_KEY}<br>`), s = i.childNodes[0], this.restCursorPos(s, s.textContent.length);
                break
            }
            if (String(i.getAttribute("data-set-empty")) === "true" && (i.setAttribute("data-set-empty", "false"), i.innerHTML = i.childNodes[0].textContent.replace(new RegExp(this.VOID_KEY, "g"), ""), s = i.childNodes[0], this.restCursorPos(s, s.textContent.length)), n = i.parentNode.nextSibling, n && n.nodeType === Node.TEXT_NODE) {
                let p = n.textContent, g = this.getGridElm(!0);
                g.childNodes[0].textContent = p, g.childNodes[0].setAttribute("data-set-empty", "false"), n.parentNode.insertBefore(g, n), n.parentNode.removeChild(n), n = g
            }
            n && n.getAttribute("data-set-richType") === "richMark" && this.markMerge(i.parentNode, n);
            break
    }
}
getNodeEmpty(t)
{
    const e = new RegExp(`^(${this.ZERO_WIDTH_KEY}|<br>|${this.VOID_KEY})+$`);
    return !t.innerHTML || e.test(t.innerHTML)
}
setWrap()
{
    const t = window.getSelection();
    let {focusNode: e, focusOffset: i} = t;
    if (e.nodeType !== Node.TEXT_NODE) {
        if (!e.getAttribute || e.getAttribute("data-set-richType") !== "richInput") return;
        e = e.childNodes[0]
    }
    const a = e.textContent.slice(i), s = e.parentNode.parentNode, n = s.parentNode,
        l = Array.prototype.indexOf.call(n.childNodes, s), o = Array.prototype.slice.call(n.childNodes, l + 1),
        r = this.getGridElm();
    let d = r.children[0].children[0].childNodes[0], h = 1;
    (a || o.length > 0) && d.parentNode.removeChild(d.parentNode.childNodes[1]), a && a !== this.VOID_KEY && (e.textContent = e.textContent.slice(0, i), d.textContent = (d.textContent + a).replace(new RegExp(this.VOID_KEY, "g"), () => (h--, "")), d.parentElement.setAttribute("data-set-empty", "false")), o.forEach(p => {
        n.removeChild(p), r.appendChild(p)
    });
    const c = n.lastChild.childNodes[0], u = r.lastChild.childNodes[0];
    if (c.childNodes.length <= 1) {
        const p = c.childNodes[0];
        (!p.textContent || p.textContent === this.VOID_KEY) && (c.innerHTML = `${this.VOID_KEY}<br>`, c.setAttribute("data-set-empty", "true"))
    }
    if (u.parentElement.getAttribute("data-set-richType") !== "richMark") r.appendChild(this.getGridElm(!0)); else if (u.childNodes.length <= 1) {
        const p = u.childNodes[0];
        (!p.textContent || p.textContent === this.VOID_KEY) && (u.innerHTML = `${this.VOID_KEY}<br>`, u.setAttribute("data-set-empty", "true"), d = r.children[0].children[0].childNodes[0])
    }
    n.nextSibling ? this.richText.insertBefore(r, n.nextSibling) : this.richText.appendChild(r), this.restCursorPos(d, d.textContent === this.VOID_KEY ? 1 : h), this.viewIntoPoint()
}
selectRegionMerge()
{
    const t = window.getSelection();
    if (t.isCollapsed || t.rangeCount <= 0) return;
    const e = t.getRangeAt(0);
    if (e.startContainer.nodeType === Node.TEXT_NODE && e.startContainer === e.endContainer) {
        const i = e.startContainer;
        if (i.length === e.endOffset - e.startOffset) {
            const a = i.parentNode, s = a.parentNode === a.parentNode.parentNode.lastChild;
            a.setAttribute("data-set-empty", "true"), a.innerHTML = `\uFEFF${s ? "<br>" : ""}`, this.restCursorPos(a.childNodes[0])
        } else e.deleteContents()
    } else if (e.commonAncestorContainer && e.commonAncestorContainer.getAttribute("data-set-richType") === "richBox") {
        const i = e.startContainer.nodeType === Node.TEXT_NODE ? e.startContainer.parentNode.parentNode : e.startContainer,
            a = e.endContainer.nodeType === Node.TEXT_NODE ? e.endContainer.parentNode.parentNode : e.endContainer;
        e.deleteContents(), i.getAttribute("data-set-richType") === a.getAttribute("data-set-richType") && this.markMerge(i, a)
    } else if (e.commonAncestorContainer === e.startContainer && e.startContainer === e.endContainer) this.textInnerHtmlInit(!0); else {
        const i = n => {
            if (n.nodeType === Node.TEXT_NODE) return n.parentNode.parentNode.parentNode;
            switch (n.getAttribute("data-set-richType")) {
                case"richInput":
                    return n.parentNode.parentNode;
                case"richMark":
                    return n.parentNode;
                case"richBox":
                    return n;
                default:
                    return null
            }
        }, a = i(e.startContainer), s = i(e.endContainer);
        if (!a || !s) return;
        e.deleteContents(), this.gridMerge(a, s)
    }
    return !0
}
gridElmMerge()
{
    const t = window.getSelection(), {focusNode: e, focusOffset: i, isCollapsed: a} = t;
    if (i > 1 || !a) return !1;
    const s = (o, r) => o.parentNode !== this.richText && o !== o.parentNode.childNodes[0] ? !1 : Array.prototype.indexOf.call(this.richText.childNodes, o) !== -1 ? o : r >= 6 ? !1 : s(o.parentNode, r + 1),
        n = s(e, 0);
    if (!n || n === this.richText.childNodes[0] || i === 1 && n.children[0].children[0].getAttribute("data-set-empty") === "false") return !1;
    const l = n.previousSibling;
    return this.gridMerge(l, n), !0
}
delMarkRule()
{
    const t = window.getSelection(), e = t.focusNode, i = e.textContent, a = e.parentNode, s = a.parentNode,
        n = s.parentNode;
    if (!t.isCollapsed || a.getAttribute("data-set-richType") !== "richInput") return !1;
    if (i && i.length === 1 && s !== n.childNodes[0] && (t.focusOffset !== 0 || i === this.VOID_KEY)) {
        if (i === this.VOID_KEY) {
            const l = s.previousSibling.previousSibling;
            n.removeChild(s.previousSibling), n.removeChild(s);
            const o = l.childNodes[0], r = o.childNodes[0];
            r.textContent === this.VOID_KEY && l === n.lastChild && o.appendChild(document.createElement("br")), this.restCursorPos(r, r.textContent.length)
        } else {
            a.innerHTML = s === n.lastChild ? `${this.VOID_KEY}<br>` : this.VOID_KEY, a.setAttribute("data-set-empty", "true");
            const l = a.childNodes[0];
            this.restCursorPos(l, 1)
        }
        return !0
    } else if (t.focusOffset === 0) {
        const l = a.parentNode, o = l == null ? void 0 : l.previousSibling;
        return !o || o.getAttribute("data-set-richType") !== "chatTag" ? !1 : (this.delTag(o), !0)
    }
}
delTag(t)
{
    const e = t.previousSibling, i = t.nextSibling;
    t.parentNode.removeChild(t), this.markMerge(e, i)
}
gridMerge(t, e, i = !1)
{
    t.lastChild.getAttribute("data-set-richType") !== "richMark" && t.appendChild(this.getGridElm(!0)), e.childNodes[0].getAttribute("data-set-richType") !== "richMark" && e.insertBefore(this.getGridElm(!0), e.childNodes[0]);
    const a = t.lastChild.childNodes[0], s = a.childNodes[0];
    let n = s.textContent.length;
    Array.prototype.forEach.call(e.childNodes, o => {
        t.appendChild(o.cloneNode(!0))
    }), e.childNodes.length > 1 && a.childNodes[1] && a.removeChild(a.childNodes[1]);
    const l = a.parentNode.nextSibling;
    if (l) {
        const r = l.children[0].childNodes[0];
        r && r.textContent !== this.VOID_KEY && (a.childNodes[1] && a.removeChild(a.childNodes[1]), s.textContent = (s.textContent + r.textContent).replace(new RegExp(this.VOID_KEY, "g"), () => (n--, "")), s.parentElement.setAttribute("data-set-empty", "false")), t.removeChild(l)
    }
    if (s.textContent === "" && (s.textContent = this.VOID_KEY, s.parentNode.setAttribute("data-set-empty", "true"), n = 1), i) {
        const r = t.childNodes[t.childNodes.length - 1].childNodes[0].childNodes[0];
        this.restCursorPos(r, r.textContent.length)
    } else this.richText.removeChild(e), this.restCursorPos(s, n);
    this.viewIntoPoint()
}
markMerge(t, e)
{
    const a = t.children[0].childNodes[0];
    let s = a.textContent.length;
    if (e) {
        const o = e.children[0].childNodes[0];
        o && o.textContent !== this.VOID_KEY && (a.textContent = (a.textContent + o.textContent).replace(new RegExp(this.VOID_KEY, "g"), () => (s--, "")), a.parentElement.setAttribute("data-set-empty", "false")), e.parentNode.removeChild(e)
    }
    a.textContent === "" && (a.textContent = this.VOID_KEY, a.parentNode.setAttribute("data-set-empty", "true"), s = 1);
    const n = t.parentNode;
    a.textContent === this.VOID_KEY && t === n.lastChild && (a.parentNode.appendChild(document.createElement("br")), a.parentNode.setAttribute("data-set-empty", "true"), s = 1), this.restCursorPos(a, s)
}
setCallSpace(t)
{
    this.needCallSpace = t
}
getWrapNode(t, e = !1)
{
    if (t.nodeType === Node.TEXT_NODE) return t.parentNode.parentNode.parentNode;
    const i = t.getAttribute("data-set-richType");
    if (e && i === "chatTag") return t.parentNode;
    switch (i) {
        case"richInput":
            return t.parentNode.parentNode;
        case"richMark":
            return t.parentNode;
        case"richBox":
            return t
    }
}
getMarkNode(t, e = !1)
{
    if (t.nodeType === Node.TEXT_NODE) return t.parentNode.parentNode;
    const i = t.getAttribute("data-set-richType");
    if (e && i === "chatTag") return t;
    switch (i) {
        case"richInput":
            return t.parentNode;
        case"richMark":
            return t
    }
}
getRichTextNodeIndex(t)
{
    const e = this.getMarkNode(t), i = e.parentNode;
    return !e || !i ? {
        gridIndex: null,
        markIndex: null
    } : {
        gridIndex: Array.prototype.indexOf.call(this.richText.childNodes, i),
        markIndex: Array.prototype.indexOf.call(i.childNodes, e)
    }
}
setWrapNodeByMark(t)
{
    const e = document.createElement("p");
    return e.className = "chat-grid-wrap", e.setAttribute("data-set-richType", "richBox"), Array.prototype.forEach.call(t, i => {
        e.appendChild(i)
    }), e
}
setRangeLastText()
{
    const t = this.richText.childNodes[this.richText.childNodes.length - 1],
        a = t.childNodes[t.childNodes.length - 1].children[0].childNodes[0];
    this.restCursorPos(a, a.textContent === this.VOID_KEY ? 1 : a.textContent.length), this.viewIntoPoint()
}
viewIntoPoint()
{
    const t = window.getSelection();
    if (t.rangeCount > 0) {
        const e = t.getRangeAt(0), i = this.getWrapNode(e.endContainer);
        if (!i) return;
        const a = this.richText.parentElement, {scrollHeight: s, clientHeight: n, scrollTop: l} = a;
        if (s <= n) return;
        const o = i.getBoundingClientRect().top - a.getBoundingClientRect().top + i.clientHeight + l, r = l, d = n + l;
        if (o < r || o > d) {
            const h = o - n;
            a.scrollTo(0, h)
        }
    }
}
}
const w = (m = 50) => new Promise(t => {
    setTimeout(t, m)
}), O = m => String(m) !== "false" && String(m) !== "null" && String(m) !== "0", P = (m, t, e = !1) => {
    let i;
    return function (...a) {
        const s = this, n = () => {
            i = null, e || m.apply(s, a)
        }, l = e && !i;
        clearTimeout(i), i = setTimeout(n, t), l && m.apply(s, a)
    }
}, K = (m, t) => {
    let e;
    return function (...i) {
        const a = this;
        e || (m.apply(a, i), e = !0, setTimeout(function () {
            e = !1
        }, t))
    }
}, R = (m, t, e) => {
    if (m = m.toLowerCase(), t = t.toLowerCase(), e = e.toLowerCase(), e = e.replace(/\s/g, ""), !/^[\u4E00-\u9FFF]+$/.test(m) || !t || t.indexOf(" ") === -1) {
        const s = q(m, t, e, {continuous: !1, space: "ignore", insensitive: !0});
        return !!(s && s.length > 0)
    }
    return V(m, t, e)
}, V = (m, t, e) => {
    m = m.replace(/\s/g, "");
    const i = m.split("").map(l => [l, !1]), a = t.split(" ").map(l => [l, !1, ""]), s = [];
    let n = 0;
    for (let l = 0; l < e.length; l++) {
        const o = e[l];
        /^[\u4E00-\u9FFF]+$/.test(o) ? i.some((d, h) => {
            const c = d[1] || a[h][1];
            if (h >= n && !c && d[0] === o) return d[1] = !0, n = h, s.push(h), !0
        }) : a.some((d, h) => {
            const c = d[1] || a[h][1], u = d[2] + o;
            if (h >= n && !c && F(d[0], u)) return d[2] = u, n = h, d[0] === d[2] && (d[1] = !0), s.push(h), !0
        })
    }
    return s.length === e.length
}, F = (m, t) => {
    let e = !1, i = -1;
    for (let a = 0; a < t.length; a++) {
        const s = m.indexOf(t[a]);
        if (s > i) i = s, e = !0; else {
            e = !1;
            break
        }
    }
    return e
}, q = (m, t, e, i) => {
    let a = [];
    for (let s = 0; s < m.length; s++) {
        if (i.space === "ignore" && m[s] === " ") {
            a.push(s);
            continue
        }
        if (m[s] === e[0]) {
            e = e.slice(1), a.push(s);
            continue
        }
        const n = t.split(" ");
        let l = 0;
        if (n.forEach(o => {
            const r = W(o, e);
            r > l && (l = r)
        }), l && (e = e.slice(l), a.push(s)), !e) break
    }
    if (e) return null;
    if (i.continuous) {
        const s = a;
        if (a.some((l, o) => o > 0 && l !== s[o - 1] + 1)) return null
    }
    return i.space === "ignore" && (a = a.filter(s => m[s] !== " ")), a.length ? a : null
}, W = (m, t) => {
    let e = 0;
    for (let i = 0; i < m.length; i++) m[i] === t[e] && e++;
    return e
};

function G() {
    const m = navigator.userAgent, t = /(?:Windows Phone)/.test(m), e = /(?:SymbianOS)/.test(m) || t,
        i = /(?:Android)/.test(m), a = /(?:Firefox)/.test(m),
        s = /(?:iPad|PlayBook)/.test(m) || i && !/(?:Mobile)/.test(m) || a && /(?:Tablet)/.test(m),
        n = /(?:iPhone)/.test(m) && !s;
    return {isTablet: s, isPhone: n, isAndroid: i, isPc: !n && !i && !e}
}

function f(m, t = !1, e = "block") {
    m && (m.className = m.className.replace(/ chat-view-show| chat-view-hidden/g, ""), t ? (m.style.display = e, m.className += " chat-view-show") : (m.className += " chat-view-hidden", m.style.display = "none"))
}

function S(m, t = "block") {
    return m && m.style.display === t
}

function y(m, t, e) {
    m.classList[e ? "add" : "remove"](t)
}

const _ = function (m, t, e) {
        return m.forEach(i => {
            if (e in i) {
                const a = t.indexOf(String(i[e]));
                a !== -1 && (t[a] = i)
            }
        }), t.filter(i => i[e])
    },
    Z = '<svg class="check-empty-svg" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse fill="#f5f5f5" cx="32" cy="33" rx="32" ry="7"></ellipse><g fill-rule="nonzero" stroke="#d9d9d9"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" fill="#fafafa"></path></g></g></svg>',
    j = '<svg class="empty-svg" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse fill="#f5f5f5" cx="32" cy="33" rx="32" ry="7"></ellipse><g fill-rule="nonzero" stroke="#d9d9d9"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" fill="#fafafa"></path></g></g></svg>',
    z = '<svg class="icon-search" style="vertical-align: middle;fill: currentColor;overflow: hidden;" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M684.8 223.530667a326.272 326.272 0 0 1 24.96 433.621333c2.645333 2.133333 5.290667 4.48 7.850667 7.04L870.4 817.066667c24.789333 24.746667 32.896 56.832 18.133333 71.594666-14.762667 14.805333-46.848 6.656-71.637333-18.090666l-152.789333-152.832a106.282667 106.282667 0 0 1-7.210667-7.936 326.101333 326.101333 0 0 1-433.109333-25.173334c-127.445333-127.445333-127.573333-333.952-0.256-461.269333 127.36-127.36 333.866667-127.232 461.269333 0.213333zM275.328 275.114667a252.885333 252.885333 0 0 0 0.256 357.632 252.885333 252.885333 0 0 0 357.632 0.256 252.885333 252.885333 0 0 0-0.256-357.632 252.885333 252.885333 0 0 0-357.632-0.256z" fill="#9B9B9B"></path></svg>',
    U = '<div class="ant-spin ant-spin-spinning" aria-live="polite" aria-busy="true"><span class="ant-spin-dot ant-spin-dot-spin"><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i><i class="ant-spin-dot-item"></i></span></div>',
    X = '<svg class="match-empty-svg" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse fill="#f5f5f5" cx="32" cy="33" rx="32" ry="7"></ellipse><g fill-rule="nonzero" stroke="#d9d9d9"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" fill="#fafafa"></path></g></g></svg>';

class J {
    constructor(t) {
        E(this, "chat");
        E(this, "deviceInfo", G());
        E(this, "isComposition", !1);
        E(this, "undoHistory", []);
        E(this, "redoHistory", []);
        E(this, "doOverHistory", !0);
        E(this, "isExternalCallPopup", !1);
        E(this, "isIMEModel", !1);
        E(this, "matchKey", 0);
        E(this, "chatEventModule", {
            enterSend: [],
            operate: [],
            defaultAction: [],
            atMatch: [],
            atCheck: [],
            tagCheck: []
        });
        E(this, "debounceEvents", {
            recordHistory: () => {
            }, dialogMoveToRange: t => {
            }, matchPointDialog: () => {
            }, movePointActiveUserElm: t => {
            }, moveCustomActiveTagElm: t => {
            }
        });
        E(this, "richText");
        E(this, "placeholderElm");
        E(this, "userList", []);
        E(this, "userProps", {id: "id", name: "name", avatar: "avatar", pinyin: "pinyin"});
        E(this, "targetUserList", []);
        E(this, "checkboxRows", []);
        E(this, "needCallEvery", !0);
        E(this, "maxLength");
        E(this, "textLength", 0);
        E(this, "needDialog", !0);
        E(this, "asyncMatch", !1);
        E(this, "copyType", ["text"]);
        E(this, "uploadImage");
        E(this, "customTags", {});
        E(this, "wrapKeyFun", t => t.ctrlKey && ["Enter"].includes(t.key));
        E(this, "sendKeyFun", t => !t.ctrlKey && ["Enter"].includes(t.key));
        E(this, "startOpenIndex", 0);
        E(this, "isPointSearchMode", !1);
        E(this, "pcElms", {
            containerDialogElm: null,
            pointDialogElm: null,
            pointDialogCheckElm: null,
            pointDialogMainElm: null,
            pointDialogUsersElm: [],
            pointDialogActiveElm: null,
            pointDialogLoadingElm: null,
            pointDialogEmptyElm: null,
            checkDialogElm: null,
            checkDialogSearchResultElm: null,
            checkDialogUsersElm: null,
            checkDialogSearchInputElm: null,
            checkDialogTagsElm: null,
            customTagDialogElms: {},
            customTagDialogTagKey: "",
            customTagDialogActiveElm: null
        });
        E(this, "h5Elms", {
            dialogElm: null,
            dialogMainElm: null,
            dialogCheckElm: null,
            dialogShowElm: null,
            dialogSearchElm: null,
            dialogEmptyElm: null,
            dialogLoadingElm: null
        });
        E(this, "reviseLabels", {
            callEveryLabel: "所有人",
            searchEmptyLabel: "没有匹配到任何结果",
            checkAllLabel: "全选",
            checkEmptyLabel: "请选择需要@的成员"
        });
        E(this, "winClick", () => {
            S(this.pcElms.pointDialogElm) && this.exitPointDialog(), this.pcElms.checkDialogSearchResultElm && f(this.pcElms.checkDialogSearchResultElm), this.pcElms.customTagDialogTagKey && S(this.pcElms.customTagDialogElms[this.pcElms.customTagDialogTagKey]) && this.exitCustomTagDialog()
        });
        E(this, "winKeydown", async t => {
            if (t.ctrlKey && t.code === "KeyZ" && t.preventDefault(), !this.isComposition) {
                if (S(this.pcElms.pointDialogElm)) {
                    if (t.code === "ArrowDown") {
                        t.preventDefault(), this.debounceEvents.movePointActiveUserElm("down");
                        return
                    }
                    if (t.code === "ArrowUp") {
                        t.preventDefault(), this.debounceEvents.movePointActiveUserElm("up");
                        return
                    }
                    if ((t.code === "Enter" || t.code === "NumpadEnter") && this.pcElms.pointDialogActiveElm) {
                        t.preventDefault();
                        const e = this.pcElms.pointDialogActiveElm.getAttribute("data-set-id");
                        if (await w(100), this.isPointSearchMode || this.asyncMatch) await this.matchSetTag(this.userList.find(i => i.id === e)); else {
                            const i = this.targetUserList.find(a => String(a[this.userProps.id]) === e);
                            await this.onceSetTag(i)
                        }
                        this.exitPointDialog()
                    }
                } else if (this.pcElms.customTagDialogTagKey && S(this.pcElms.customTagDialogElms[this.pcElms.customTagDialogTagKey])) {
                    if (t.code === "ArrowDown") {
                        t.preventDefault(), this.debounceEvents.moveCustomActiveTagElm("down");
                        return
                    }
                    if (t.code === "ArrowUp") {
                        t.preventDefault(), this.debounceEvents.moveCustomActiveTagElm("up");
                        return
                    }
                    if ((t.code === "Enter" || t.code === "NumpadEnter") && this.pcElms.customTagDialogActiveElm) {
                        t.preventDefault();
                        const e = this.pcElms.customTagDialogActiveElm.getAttribute("data-set-id");
                        await w(100);
                        const a = this.customTags[this.pcElms.customTagDialogTagKey].find(s => s.id === e);
                        this.isPointSearchMode ? await this.matchSetCustomTag(a) : await this.onceSetCustomTag(a), this.exitCustomTagDialog()
                    }
                }
            }
        });
        const {elm: e, callEveryLabel: i, needDialog: a, asyncMatch: s} = t;
        y(e, `chat-area-${this.deviceInfo.isPc ? "pc" : "h5"}`, !0), this.richText = document.createElement("div"), this.richText.setAttribute("class", "chat-rich-text"), this.richText.setAttribute("data-set-richType", "richAllBox"), this.richText.setAttribute("contenteditable", "true"), e.appendChild(this.richText), this.placeholderElm = document.createElement("div"), this.placeholderElm.setAttribute("class", "chat-placeholder-wrap"), f(this.placeholderElm, !0), e.appendChild(this.placeholderElm), this.chat = new $(this.richText), i && (this.reviseLabels.callEveryLabel = i), this.needDialog = O(a), s !== void 0 && (this.asyncMatch = O(s)), this.needDialog && (this.deviceInfo.isPc ? this.hasPc() : this.hasH5()), this.registerEvent(), this.updateConfig(t), this.otherInit(t)
    }

    registerEvent() {
        this.richText.addEventListener("keyup", t => {
            if (!this.needDialog) return;
            if (t.stopPropagation(), this.deviceInfo.isPc) {
                t.keyCode === 50 || t.code === "Digit2" || t.key === "@" ? this.ruleShowPointDialog() : Object.keys(this.pcElms.customTagDialogElms).indexOf(t.key) !== -1 && this.showCustomTagDialog(t.key);
                return
            }
            const e = t.key === "Unidentified" ? "android" : "ios";
            let i = !1;
            switch (e) {
                case"android":
                    i = t.keyCode === 229;
                    break;
                case"ios":
                    i = t.keyCode === 50 || t.code === "Digit2" || t.key === "@";
                    break
            }
            i && (this.userList.length > 0 || this.asyncMatch) && this.chat.showAt() && (this.showH5Dialog(), this.isExternalCallPopup = !1)
        }), this.richText.addEventListener("keydown", async t => {
            if (!this.deviceInfo.isPc && t.key === "Unidentified" && t.keyCode === 229) {
                this.isIMEModel = !0;
                return
            }
            if (this.isIMEModel) return;
            if (S(this.pcElms.pointDialogElm)) {
                ["ArrowUp", "ArrowDown", "Enter", "NumpadEnter"].includes(t.code) ? t.preventDefault() : ["ArrowLeft", "ArrowRight"].includes(t.code) && this.exitPointDialog();
                return
            }
            if (this.pcElms.customTagDialogTagKey && S(this.pcElms.customTagDialogElms[this.pcElms.customTagDialogTagKey])) {
                ["ArrowUp", "ArrowDown", "Enter", "NumpadEnter"].includes(t.code) ? t.preventDefault() : ["ArrowLeft", "ArrowRight"].includes(t.code) && this.exitCustomTagDialog();
                return
            }
            t.code === "Backspace" || t.key === "Backspace" ? (this.chat.selectRegionMerge() || this.chat.gridElmMerge() || this.chat.delMarkRule()) && (t.preventDefault(), await this.richTextInput()) : this.wrapKeyFun(t) || !this.deviceInfo.isPc && t.key === "Enter" ? (t.preventDefault(), this.chat.setWrap(), await this.richTextInput()) : this.sendKeyFun(t) ? (t.preventDefault(), await w(100), this.enterSend()) : ["ArrowLeft", "ArrowRight"].includes(t.code) ? (t.preventDefault(), this.chat.switchRange(t.code)) : t.ctrlKey && t.code === "KeyA" ? this.isEmpty() && t.preventDefault() : t.ctrlKey && t.code === "KeyZ" ? (t.preventDefault(), this.ruleChatEvent(this.undo, "defaultAction", "UNDO")) : t.ctrlKey && t.code === "KeyY" && (t.preventDefault(), this.ruleChatEvent(this.redo, "defaultAction", "REDO")), ["Backspace", "Shift", "Tab", "CapsLock", "Control", "Meta", "Alt", "ContextMenu", "Enter", "NumpadEnter", "Escape", "ArrowLeft", "ArrowUp", "ArrowRight", "ArrowDown", "Home", "End", "PageUp", "PageDown", "Insert", "Delete", "NumLock"].indexOf(t.key) === -1 && !t.ctrlKey && !t.altKey && !t.metaKey && this.chat.selectRegionMerge()
        }), this.richText.addEventListener("input", async () => {
            if (this.isIMEModel) {
                this.isIMEModel = !1, this.chat.upDataNodeOrIndex(), this.isComposition || this.chat.updateGrid(), this.maxLength !== void 0 && this.ruleMaxLength(), this.showPlaceholder(), this.triggerChatEvent("operate");
                return
            }
            await this.richTextInput(), this.deviceInfo.isPc && !this.isComposition && this.debounceEvents.matchPointDialog()
        }), this.richText.addEventListener("copy", t => {
            t.preventDefault(), this.ruleChatEvent(() => {
                this.copyRange(t)
            }, "defaultAction", "COPY")
        }), this.richText.addEventListener("cut", t => {
            t.preventDefault(), this.ruleChatEvent(() => {
                this.copyRange(t), this.removeRange()
            }, "defaultAction", "CUT")
        }), this.richText.addEventListener("paste", t => {
            t.preventDefault(), this.ruleChatEvent(() => {
                const e = t.clipboardData.getData("text/plain");
                if (typeof e == "string" && e !== "") {
                    if (this.copyType.indexOf("text") === -1) return;
                    let i = document.createElement("div");
                    i.innerHTML = t.clipboardData.getData("application/my-custom-format") || "", this.chat.selectRegionMerge(), i.children[0] && i.children[0].getAttribute("data-set-richType") === "richBox" ? this.insertInsideHtml(i.innerHTML) : (i.innerHTML = e, this.insertText(i.innerText)), i = null
                } else {
                    if (this.copyType.indexOf("image") === -1) return;
                    const a = (t.clipboardData || t.originalEvent.clipboardData).items || [];
                    Array.prototype.forEach.call(a, async s => {
                        if (s.type.indexOf("image") === -1) return;
                        const n = s.getAsFile();
                        if (this.uploadImage) {
                            const l = await this.uploadImage(n);
                            this.insertHtml(`<img class="chat-img" src="${l}" alt="" />`)
                        } else {
                            const l = new FileReader;
                            l.onload = o => {
                                this.insertHtml(`<img class="chat-img" src="${o.target.result}" alt="" />`)
                            }, l.readAsDataURL(n)
                        }
                    })
                }
            }, "defaultAction", "PASTE")
        }), this.richText.addEventListener("blur", t => {
            this.chat.upDataNodeOrIndex()
        }), this.richText.addEventListener("focus", t => {
            this.chat.upDataNodeOrIndex()
        }), this.richText.addEventListener("click", () => {
            this.chat.upDataNodeOrIndex()
        }), this.richText.addEventListener("dragstart", t => {
            t.stopPropagation(), t.preventDefault()
        }), this.richText.addEventListener("dragover", t => {
            t.stopPropagation(), t.preventDefault()
        }), this.richText.addEventListener("drop", t => {
            t.stopPropagation(), t.preventDefault()
        }), this.richText.addEventListener("compositionstart", () => {
            this.isComposition = !0
        }), this.richText.addEventListener("compositionend", () => {
            this.isComposition = !1
        })
    }

    otherInit(t) {
        let e = O(t.needDebounce);
        const i = () => {
            const {gridIndex: r, markIndex: d} = this.chat.getRichTextNodeIndex(this.chat.vnode);
            if (r === null || d == null) return;
            const h = {html: this.richText.innerHTML, gridIndex: r, markIndex: d, cursorIndex: this.chat.cursorIndex};
            this.undoHistory.push(h), this.undoHistory.length > 50 && this.undoHistory.shift()
        };
        this.debounceEvents.recordHistory = e ? P(i, 200) : i;
        const a = r => {
            let d = "0", h = "100%";
            const c = this.chat.getRangeRect();
            if (!c) return;
            const u = this.pcElms.containerDialogElm.getBoundingClientRect();
            let p = c.x - u.x, g = u.y - c.y;
            const {clientWidth: x, clientHeight: T} = r;
            c.x > window.innerWidth - x - 30 && (p = c.x - x - u.x - 16, d = "100%"), c.y < T && (g = g - T, h = "0"), r.style.transform = "translate(0, 0)", r.style.transformOrigin = `${d} ${h}`, r.style.left = p + 6 + "px", r.style.bottom = `${g}px`, r.style.opacity = "1"
        };
        this.debounceEvents.dialogMoveToRange = e ? P(a, 120, !0) : a;
        const s = () => {
            if (!this.needDialog) return;
            const r = this.chat.vnode.textContent || "", d = this.chat.cursorIndex, h = r.slice(0, d);
            let c = -1, u = -1, p = "userTag";
            if (h.lastIndexOf("@") !== -1 && (c = h.lastIndexOf("@")), this.pcElms.customTagDialogTagKey && h.lastIndexOf(this.pcElms.customTagDialogTagKey) !== -1 && (u = h.lastIndexOf(this.pcElms.customTagDialogTagKey)), u > c && (p = "customTag"), p === "userTag" && this.asyncMatch) {
                if (c < 0) {
                    this.exitPointDialog();
                    return
                }
                this.matchKey++;
                const b = this.matchKey;
                this.startOpenIndex = c + 1;
                const k = h.slice(this.startOpenIndex) || "";
                if (/\s/ig.test(k)) {
                    this.exitPointDialog();
                    return
                }
                this.updateUserList([]), f(this.pcElms.pointDialogLoadingElm, !0, "flex"), f(this.pcElms.pointDialogEmptyElm), this.showPointDialog();
                const v = this.triggerChatEvent("atMatch", k);
                v && v instanceof Promise && v.then(A => {
                    if (b === this.matchKey) {
                        if (f(this.pcElms.pointDialogLoadingElm), !A || A.length <= 0) {
                            f(this.pcElms.pointDialogEmptyElm, !0, "flex");
                            return
                        }
                        this.updateUserList(A), this.pcElms.pointDialogUsersElm && this.pcElms.pointDialogUsersElm.length > 0 && this.updatePointActiveUserElm(this.pcElms.pointDialogUsersElm[0].elm)
                    }
                });
                return
            }
            if (p === "userTag" && this.userList.length <= 0 || p === "customTag" && this.customTags[this.pcElms.customTagDialogTagKey].length <= 0) return;
            const g = () => {
                p === "userTag" ? this.exitPointDialog() : this.exitCustomTagDialog()
            }, x = () => {
                p === "userTag" ? this.showPointDialog() : this.showCustomTagDialog(this.pcElms.customTagDialogTagKey)
            };
            if (c < 0 && u < 0) {
                this.exitPointDialog(), this.exitCustomTagDialog();
                return
            }
            this.startOpenIndex = p === "userTag" ? c + 1 : u + 1;
            const T = new RegExp(`^([${this.chat.ZERO_WIDTH_KEY}${this.chat.VOID_KEY}])+$`);
            if (!h || T.test(h) || d < this.startOpenIndex) {
                g();
                return
            }
            const C = h.slice(this.startOpenIndex) || "";
            if (/\s/ig.test(C)) {
                g();
                return
            }
            if (!C) {
                x();
                return
            }
            if (p === "userTag") {
                const b = this.searchUserList(C);
                b.length > 0 ? this.showPointDialog(b) : g()
            } else {
                const k = this.customTags[this.pcElms.customTagDialogTagKey].filter(v => R(v.name, v.pinyin || "", C));
                k.length > 0 ? this.showCustomTagDialog(this.pcElms.customTagDialogTagKey, k) : g()
            }
        };
        this.debounceEvents.matchPointDialog = e ? P(s, 200) : s;
        const n = r => {
            if (!this.pcElms.pointDialogActiveElm) return;
            let d = 0;
            const h = this.pcElms.pointDialogActiveElm.getAttribute("data-set-id");
            this.pcElms.pointDialogUsersElm.some(g => {
                const x = g.elm.getAttribute("data-set-id");
                return d = g.index, h === x
            });
            const c = this.pcElms.pointDialogUsersElm.filter(g => g.elm.className.indexOf("user-no-match") === -1),
                u = c.map(g => g.index);
            let p;
            r === "down" ? d === c[c.length - 1].index ? p = c[0] : p = c[u.indexOf(d) + 1] : r === "up" && (d === c[0].index ? p = c[c.length - 1] : p = c[u.indexOf(d) - 1]), p && this.updatePointActiveUserElm(p.elm, !0)
        };
        this.debounceEvents.movePointActiveUserElm = K(n, 80);
        const l = r => {
            if (!this.pcElms.customTagDialogActiveElm) return;
            const d = this.customTags[this.pcElms.customTagDialogTagKey].map(x => x.id),
                h = this.pcElms.customTagDialogActiveElm.getAttribute("data-set-id"), c = d.indexOf(h),
                u = Array.prototype.map.call(this.pcElms.customTagDialogElms[this.pcElms.customTagDialogTagKey].children[1].children, (x, T) => ({
                    elm: x,
                    index: T
                })).filter(x => x.elm.className.indexOf("tag-no-match") === -1), p = u.map(x => x.index);
            let g;
            r === "down" ? c === u[u.length - 1].index ? g = u[0] : g = u[p.indexOf(c) + 1] : r === "up" && (c === u[0].index ? g = u[u.length - 1] : g = u[p.indexOf(c) - 1]), g && this.updateActiveCustomTagElm(g.elm, !0)
        };
        this.debounceEvents.moveCustomActiveTagElm = K(l, 80);
        const o = {html: this.richText.innerHTML, gridIndex: 0, markIndex: 0, cursorIndex: this.chat.cursorIndex};
        this.undoHistory = [o]
    }

    async richTextInput(t = !0) {
        this.chat.upDataNodeOrIndex(), this.deviceInfo.isPc && this.chat.selectRegionMerge(), await w(50), this.isComposition || this.chat.updateGrid();
        const i = (this.richText.children[0] || {childNodes: []}).childNodes[0];
        if (!i || !i.getAttribute || i.getAttribute("data-set-richType") !== "richMark") {
            this.chat.textInnerHtmlInit(!0), this.showPlaceholder(), this.triggerChatEvent("operate");
            return
        }
        this.maxLength !== void 0 && this.ruleMaxLength(), this.showPlaceholder(), this.triggerChatEvent("operate"), t && this.doOverHistory && !this.isComposition && this.debounceEvents.recordHistory(), this.chat.viewIntoPoint()
    }

    copyRange(t) {
        const e = window.getSelection();
        if (e.isCollapsed || e.rangeCount <= 0) {
            t.clipboardData.setData("application/my-custom-format", ""), t.clipboardData.setData("text/plain", "");
            return
        }
        const i = e.toString() || "";
        let a = document.createElement("div");
        a.innerHTML = i;
        const s = a.innerText.replace(/\n\n/g, `
`);
        a = null, t.clipboardData.setData("text/plain", s);
        const n = e.anchorNode, l = e.focusNode;
        if (n === l && n.nodeType === Node.TEXT_NODE) {
            const x = n.textContent.slice(e.anchorOffset, e.focusOffset);
            t.clipboardData.setData("application/my-custom-format", x);
            return
        }
        if (n === this.richText && l === this.richText) {
            t.clipboardData.setData("application/my-custom-format", this.richText.innerHTML);
            return
        }
        const o = this.chat.getWrapNode(n, !0), r = this.chat.getWrapNode(l, !0), d = this.chat.getMarkNode(n, !0),
            h = this.chat.getMarkNode(l, !0), c = d.getAttribute("data-set-richType") === "richMark",
            u = h.getAttribute("data-set-richType") === "richMark", p = Array.prototype.indexOf.call(o.childNodes, d),
            g = Array.prototype.indexOf.call(r.childNodes, h);
        if (o === r && o.parentNode === this.richText) {
            const x = p > g,
                T = Array.prototype.filter.call(o.childNodes, (M, L) => x ? L < p && L > g : L > p && L < g).map(M => M.cloneNode(!0)),
                C = c ? x ? n.textContent.slice(0, e.anchorOffset) : n.textContent.slice(e.anchorOffset) : "",
                b = u ? x ? l.textContent.slice(e.focusOffset) : l.textContent.slice(0, e.focusOffset) : "",
                k = this.chat.getGridElm(!0), v = this.chat.getGridElm(!0);
            C && (k.childNodes[0].innerHTML = C, k.childNodes[0].setAttribute("data-set-empty", "false")), b && (v.childNodes[0].innerHTML = b, v.childNodes[0].setAttribute("data-set-empty", "false")), x ? (T.unshift(v), T.push(k)) : (T.unshift(k), T.push(v));
            let A = document.createElement("div");
            const I = this.chat.setWrapNodeByMark(T);
            A.appendChild(I), t.clipboardData.setData("application/my-custom-format", A.innerHTML), A = null;
            return
        }
        if (o.parentNode === this.richText && r.parentNode === this.richText) {
            const x = Array.prototype.indexOf.call(this.richText.childNodes, o),
                T = Array.prototype.indexOf.call(this.richText.childNodes, r), C = x > T,
                b = Array.prototype.filter.call(this.richText.childNodes, (D, N) => C ? N < x && N > T : N > x && N < T).map(D => D.cloneNode(!0)),
                k = c ? C ? n.textContent.slice(0, e.anchorOffset) : n.textContent.slice(e.anchorOffset) : "",
                v = u ? C ? l.textContent.slice(e.focusOffset) : l.textContent.slice(0, e.focusOffset) : "",
                A = this.chat.getGridElm(!0), I = this.chat.getGridElm(!0);
            k && (A.childNodes[0].innerHTML = k, A.childNodes[0].setAttribute("data-set-empty", "false")), v && (I.childNodes[0].innerHTML = v, I.childNodes[0].setAttribute("data-set-empty", "false"));
            const M = Array.prototype.filter.call(o.childNodes, (D, N) => C ? N < p : N > p).map(D => D.cloneNode(!0)),
                L = Array.prototype.filter.call(r.childNodes, (D, N) => C ? N > g : N < g).map(D => D.cloneNode(!0));
            if (C) {
                M.push(A), L.unshift(I);
                const D = this.chat.setWrapNodeByMark(M), N = this.chat.setWrapNodeByMark(L);
                b.push(D), b.unshift(N)
            } else {
                M.unshift(A), L.push(I);
                const D = this.chat.setWrapNodeByMark(M), N = this.chat.setWrapNodeByMark(L);
                b.unshift(D), b.push(N)
            }
            let H = document.createElement("div");
            Array.prototype.forEach.call(b, D => {
                H.appendChild(D)
            }), t.clipboardData.setData("application/my-custom-format", H.innerHTML), H = null;

        }
    }

    async removeRange() {
        window.getSelection().getRangeAt(0).deleteContents(), await w(50), this.chat.updateGrid(), this.showPlaceholder()
    }

    updateUserList(t = void 0) {
        t && (this.userList = this.getRuleUserList(t)), this.needDialog && (this.deviceInfo.isPc ? this.updatePCUser() : this.updateH5User())
    }

    getRuleUserList(t) {
        this.targetUserList = JSON.parse(JSON.stringify(t || []));
        const e = Object.create(null);
        return e[this.userProps.id] = "isALL", e[this.userProps.name] = this.reviseLabels.callEveryLabel, this.targetUserList.unshift(e), (t == null ? void 0 : t.map((i, a) => {
            const s = i[this.userProps.id];
            if (!s && s !== 0) throw new Error(`参数值userList：下标第${a}项${this.userProps.id}值异常！`);
            return {
                id: String(s),
                name: String(i[this.userProps.name] || ""),
                avatar: String(i[this.userProps.avatar] || ""),
                pinyin: String(i[this.userProps.pinyin] || "")
            }
        })) || []
    }

    getUserHtmlTemplate(t, e) {
        const i = document.createElement("span");
        if (i.setAttribute("class", `call-user-dialog-item-sculpture ${e.avatar ? "is-avatar" : ""}`), e.avatar) {
            const s = new Image;
            s.alt = "", s.src = String(e.avatar), i.appendChild(s)
        } else i.innerHTML = `<span style="transform: scale(0.75)">${e.name.slice(-2)}</span>`;
        t.appendChild(i);
        const a = document.createElement("span");
        a.setAttribute("class", "call-user-dialog-item-name"), a.innerHTML = e.name, t.appendChild(a)
    }

    hasPc() {
        const t = this.richText.parentElement;
        this.pcElms.containerDialogElm = document.createElement("div"), this.pcElms.containerDialogElm.className = "chat-dialog", t.nextElementSibling ? t.parentElement.insertBefore(this.pcElms.containerDialogElm, t.nextElementSibling) : t.parentElement.appendChild(this.pcElms.containerDialogElm), this.asyncMatch || this.initCheckDialog(), this.initPointDialog(), window.addEventListener("click", this.winClick), window.addEventListener("keydown", this.winKeydown)
    }

    updatePCUser() {
        this.pcElms.pointDialogMainElm.innerHTML = "", this.pcElms.pointDialogActiveElm = void 0;
        const t = document.createDocumentFragment();
        if (this.needCallEvery) {
            const s = document.createElement("div");
            s.setAttribute("class", "call-user-dialog-item"), s.setAttribute("data-set-id", "isALL"), this.userSelectStyleAndEvent(s, {
                id: "isALL",
                name: this.reviseLabels.callEveryLabel
            }), s.innerHTML = `
          <span class="call-user-dialog-item-sculpture">
            <span style="transform: scale(0.75)">@</span>
          </span>
          <span class="call-user-dialog-item-name">${this.reviseLabels.callEveryLabel}(${this.userList.length})</span>
      `, t.appendChild(s)
        }
        if (this.userList.forEach(s => {
            const n = document.createElement("div");
            n.setAttribute("class", "call-user-dialog-item"), n.setAttribute("data-set-id", s.id), this.userSelectStyleAndEvent(n, s), this.getUserHtmlTemplate(n, s), t.appendChild(n)
        }), this.pcElms.pointDialogMainElm.appendChild(t), this.pcElms.pointDialogUsersElm = [], Array.prototype.forEach.call(this.pcElms.pointDialogMainElm.children || [], (s, n) => {
            this.pcElms.pointDialogUsersElm.push({index: n, elm: s})
        }), this.asyncMatch) return;
        this.pcElms.checkDialogUsersElm.innerHTML = `
                    <div class="checkbox-dialog-check-item" data-set-value="ALL">
                        <input type="checkbox" value>
                        <span class="checkbox-dialog-check-item-inner"></span>
                        <div class="checkbox-dialog-check-item-label">${this.reviseLabels.checkAllLabel}</div>
                    </div>`;
        const e = document.createDocumentFragment();
        this.userList.forEach(s => {
            const n = document.createElement("div");
            n.setAttribute("class", "checkbox-dialog-check-item"), n.setAttribute("data-set-value", s.id), n.innerHTML = `
          <input type="checkbox" value>
          <span class="checkbox-dialog-check-item-inner"></span>
      `, this.getUserHtmlTemplate(n, s), e.appendChild(n)
        }), this.pcElms.checkDialogUsersElm.appendChild(e), this.pcElms.checkDialogUsersElm && this.pcElms.checkDialogUsersElm.children.length && Array.prototype.forEach.call(this.pcElms.checkDialogUsersElm.children, s => {
            s.onclick = () => {
                const n = s.getAttribute("data-set-value") || "", l = this.userList.find(r => r.id === n),
                    o = s.className.indexOf("checkbox-dialog-check-item-check") === -1;
                n === "ALL" ? this.checkboxRows = o ? this.userList.map(r => r) : [] : o ? this.checkboxRows.push(l) : this.checkboxRows = this.checkboxRows.filter(r => r.id !== n), this.updateCheckDialogTags()
            }
        });
        const i = document.createDocumentFragment();
        this.userList.forEach(s => {
            const n = document.createElement("div");
            n.setAttribute("class", "checkbox-dialog-check-item"), n.setAttribute("data-set-id", s.id);
            const l = document.createElement("div");
            l.setAttribute("class", "checkbox-dialog-check-item-label"), this.getUserHtmlTemplate(l, s), n.appendChild(l), n.onclick = () => {
                f(this.pcElms.checkDialogSearchResultElm);
                const o = n.getAttribute("data-set-id") || "";
                if (this.pcElms.checkDialogSearchInputElm.value = "", this.pcElms.checkDialogSearchInputElm.focus(), this.checkboxRows.some(d => d.id === o)) return;
                const r = this.userList.find(d => d.id === o);
                r && this.checkboxRows.push(r), this.updateCheckDialogTags()
            }, i.appendChild(n)
        });
        const a = document.createElement("div");
        a.setAttribute("class", "checkbox-dialog-search-empty"), a.innerText = this.reviseLabels.searchEmptyLabel, i.appendChild(a), this.pcElms.checkDialogSearchResultElm.appendChild(i)
    }

    searchUserList(t) {
        return this.userList.filter(e => R(e.name, e.pinyin || "", t))
    }

    userSelectStyleAndEvent(t, e) {
        t.addEventListener("click", async i => {
            if (i.stopPropagation(), this.updatePointActiveUserElm(t), this.isPointSearchMode || this.asyncMatch) await this.matchSetTag(e); else {
                const a = this.targetUserList.find(s => String(s[this.userProps.id]) === e.id);
                await this.onceSetTag(a)
            }
            this.exitPointDialog()
        })
    }

    initCheckDialog() {
        this.pcElms.checkDialogElm = document.createElement("div"), this.pcElms.checkDialogElm.setAttribute("class", "checkbox-dialog"), f(this.pcElms.checkDialogElm), this.pcElms.checkDialogElm.innerHTML = `
      <div class="checkbox-dialog-container">
        <div class="checkbox-dialog-container-header">
            <span>选择要@的人</span>
            <span class="checkbox-dialog-container-header-close">⛌</span>
        </div>
        <div class="checkbox-dialog-container-body">
            <div class="checkbox-dialog-left-box">
                <div class="checkbox-dialog-search">
                    <input class="checkbox-dialog-search-input" placeholder="搜索人员名称" type="text">
                    <div class="checkbox-dialog-search-group"></div>
                </div>
                <div class="checkbox-dialog-tags"></div>
                <div class="checkbox-dialog-option">
                    <button class="checkbox-dialog-option-btn btn-submit disabled">确定</button>
                    <button class="checkbox-dialog-option-btn btn-close">取消</button>
                </div>
            </div>
            <div class="checkbox-dialog-right-box">
                <div class="checkbox-dialog-right-box-title">研讨成员列表</div>
                <div class="checkbox-dialog-check-group"></div>
            </div>
        </div>
      </div>
    `, this.pcElms.containerDialogElm.appendChild(this.pcElms.checkDialogElm), this.pcElms.checkDialogUsersElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-check-group"), this.pcElms.checkDialogSearchResultElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-search-group"), this.pcElms.checkDialogSearchInputElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-search-input"), this.pcElms.checkDialogTagsElm = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-tags");
        const t = () => {
                f(this.pcElms.checkDialogElm), y(document.body, "disable-scroll")
            }, e = this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-container-header-close"),
            i = this.pcElms.checkDialogElm.querySelector(".btn-close");
        e.onclick = t, i.onclick = t;
        const a = this.pcElms.checkDialogElm.querySelector(".btn-submit");
        a.onclick = async () => {
            if (a.className.indexOf("disabled") !== -1) return;
            const n = this.checkboxRows.map(l => {
                const o = Object.create(null);
                return o[this.userProps.id] = l.id, o[this.userProps.name] = l.name, o
            });
            await this.batchSetTag(n), t()
        }, f(this.pcElms.checkDialogSearchResultElm), this.pcElms.checkDialogSearchResultElm.onclick = n => {
            n.stopPropagation()
        }, this.pcElms.checkDialogSearchInputElm.onclick = n => {
            n.stopPropagation()
        };
        const s = P(n => {
            const l = String(n.target.value || "").replace(/'/g, "");
            if (!l) {
                f(this.pcElms.checkDialogSearchResultElm);
                return
            }
            const o = this.searchUserList(l).map(r => r.id);
            Array.prototype.forEach.call(this.pcElms.checkDialogSearchResultElm.children, (r, d) => {
                if (d === this.pcElms.checkDialogSearchResultElm.children.length - 1) f(r, o.length === 0); else {
                    const h = r.getAttribute("data-set-id");
                    f(r, o.indexOf(h) !== -1, "flex")
                }
            }), f(this.pcElms.checkDialogSearchResultElm, !0)
        }, 200);
        this.pcElms.checkDialogSearchInputElm.oninput = s, this.pcElms.checkDialogSearchInputElm.onfocus = s
    }

    updateCheckDialogTags() {
        const t = this.checkboxRows.map(o => o.id), e = [], i = [], a = document.createElement("div");
        a.className = "check-empty", a.innerHTML = `
        ${Z}
        <span class="check-empty-label">${this.reviseLabels.checkEmptyLabel}</span>
    `, Array.prototype.forEach.call(this.pcElms.checkDialogTagsElm.children, o => {
            const r = o.getAttribute("data-set-value");
            t.indexOf(r) === -1 ? i.push(o) : e.push(r)
        }), Array.prototype.forEach.call(this.pcElms.checkDialogUsersElm.children, (o, r) => {
            if (r === 0) {
                y(o, "checkbox-dialog-check-item-check", t.length === this.userList.length);
                return
            }
            const d = o.getAttribute("data-set-value");
            y(o, "checkbox-dialog-check-item-check", t.indexOf(d) !== -1)
        }), i.forEach(o => {
            this.pcElms.checkDialogTagsElm.removeChild(o)
        });
        const s = this.pcElms.checkDialogElm.querySelector(".btn-submit");
        y(s, "disabled", t.length <= 0), t.length || this.pcElms.checkDialogTagsElm.appendChild(a);
        const n = this.checkboxRows.filter(o => e.indexOf(o.id) === -1);
        if (!n.length) return;
        const l = document.createDocumentFragment();
        n.forEach(o => {
            const r = document.createElement("div");
            r.setAttribute("class", "checkbox-dialog-tag-item"), r.setAttribute("data-set-value", o.id), r.innerHTML = `
        <span>${o.name}</span>
      `;
            const d = document.createElement("span");
            d.setAttribute("class", "checkbox-dialog-tag-item-close"), d.innerHTML = "⛌", d.onclick = () => {
                const h = r.getAttribute("data-set-value");
                this.checkboxRows = this.checkboxRows.filter(c => c.id !== h), this.updateCheckDialogTags()
            }, r.appendChild(d), l.appendChild(r)
        }), this.pcElms.checkDialogTagsElm.appendChild(l)
    }

    showPCCheckDialog() {
        !this.needDialog || this.asyncMatch || (this.winClick(), this.checkboxRows = [], f(this.pcElms.checkDialogElm, !0), y(document.body, "disable-scroll", !0), this.pcElms.checkDialogTagsElm.scrollTop = 0, this.pcElms.checkDialogUsersElm.scrollTop = 0, this.pcElms.checkDialogSearchInputElm.value = "", this.updateCheckDialogTags(), this.isExternalCallPopup = !0)
    }

    initPointDialog() {
        this.pcElms.pointDialogElm = document.createElement("div"), this.pcElms.pointDialogElm.setAttribute("class", "call-user-dialog"), f(this.pcElms.pointDialogElm);
        const t = document.createElement("div");
        t.setAttribute("class", "call-user-dialog-header"), t.innerHTML = '<span class="call-user-dialog-header-title">群成员</span>', this.pcElms.pointDialogCheckElm = document.createElement("span"), this.pcElms.pointDialogCheckElm.setAttribute("class", "call-user-dialog-header-check"), this.pcElms.pointDialogCheckElm.innerText = "多选", this.pcElms.pointDialogCheckElm.onclick = () => {
            this.showPCCheckDialog(), this.isExternalCallPopup = !1
        }, t.appendChild(this.pcElms.pointDialogCheckElm), this.pcElms.pointDialogElm.appendChild(t), this.pcElms.pointDialogMainElm = document.createElement("div"), this.pcElms.pointDialogMainElm.setAttribute("class", "call-user-dialog-main"), this.pcElms.pointDialogElm.appendChild(this.pcElms.pointDialogMainElm), this.asyncMatch && (this.pcElms.pointDialogLoadingElm = document.createElement("div"), this.pcElms.pointDialogLoadingElm.setAttribute("class", "call-user-dialog-loading"), this.pcElms.pointDialogLoadingElm.innerHTML = U, this.pcElms.pointDialogElm.appendChild(this.pcElms.pointDialogLoadingElm), f(this.pcElms.pointDialogLoadingElm), this.pcElms.pointDialogEmptyElm = document.createElement("div"), this.pcElms.pointDialogEmptyElm.setAttribute("class", "call-user-dialog-empty"), this.pcElms.pointDialogEmptyElm.innerHTML = `
        ${X}
        <span class="empty-label">暂无数据</span>
      `, this.pcElms.pointDialogElm.appendChild(this.pcElms.pointDialogEmptyElm), f(this.pcElms.pointDialogEmptyElm)), this.pcElms.containerDialogElm.appendChild(this.pcElms.pointDialogElm)
    }

    ruleShowPointDialog() {
        this.needDialog && this.userList.length > 0 && this.chat.showAt() && (this.isExternalCallPopup = !1, this.showPointDialog())
    }

    showPointDialog(t) {
        this.exitCustomTagDialog(), this.exitPointDialog(), this.isPointSearchMode = !!t;
        let e = null;
        this.pcElms.pointDialogUsersElm.forEach(i => {
            const a = i.elm, s = a.getAttribute("data-set-id"), n = t && t.every(l => l.id !== s);
            !e && !n && (e = a), y(a, "user-no-match", n)
        }), e !== null && this.updatePointActiveUserElm(e), f(this.pcElms.pointDialogCheckElm, !this.asyncMatch && !this.isPointSearchMode), f(this.pcElms.pointDialogElm, !0), this.debounceEvents.dialogMoveToRange(this.pcElms.pointDialogElm), this.pcElms.pointDialogMainElm.scrollTop = 0
    }

    updatePointActiveUserElm(t, e = !1) {
        if (this.pcElms.pointDialogActiveElm && y(this.pcElms.pointDialogActiveElm, "call-user-dialog-item-active"), this.pcElms.pointDialogActiveElm = t, t && (y(t, "call-user-dialog-item-active", !0), e)) {
            const i = Array.prototype.filter.call(this.pcElms.pointDialogMainElm.children, o => o.className.indexOf("user-no-match") === -1),
                a = t.clientHeight, s = Array.prototype.indexOf.call(i, t),
                n = Math.ceil(Math.floor(this.pcElms.pointDialogMainElm.clientHeight / a) / 2), l = s + 1 - n;
            l > 0 ? this.pcElms.pointDialogMainElm.scrollTop = l * a : this.pcElms.pointDialogMainElm.scrollTop = 0
        }
    }

    exitPointDialog() {
        this.updatePointActiveUserElm(), this.asyncMatch && this.updateUserList([]), f(this.pcElms.pointDialogElm)
    }

    async showPCPointDialog() {
        this.needDialog && (this.insertText("@"), window.removeEventListener("click", this.winClick), this.asyncMatch && f(this.pcElms.pointDialogEmptyElm, !0, "flex"), this.showPointDialog(), await w(50), window.addEventListener("click", this.winClick))
    }

    bindCustomTrigger(t) {
        Object.values(this.pcElms.customTagDialogElms).forEach(i => {
            this.pcElms.containerDialogElm.removeChild(i)
        }), this.pcElms.customTagDialogElms = {}, this.customTags = {}, t.forEach(i => {
            i.tagList && i.tagList.length > 0 && (this.customTags[i.prefix] = i.tagList.map(a => ({
                id: String(a.id),
                name: String(a.name),
                pinyin: String(a.pinyin || "")
            })), this.initCustomTagDialog(i))
        })
    }

    initCustomTagDialog(t) {
        const e = document.createElement("div");
        e.setAttribute("class", "call-tag-dialog"), f(e);
        const i = document.createElement("div");
        i.setAttribute("class", "call-tag-dialog-header"), i.innerHTML = `<span class="call-tag-dialog-header-title">${t.dialogTitle || t.prefix}</span>`, e.appendChild(i);
        const a = document.createElement("div");
        a.setAttribute("class", "call-tag-dialog-main"), t.tagList.forEach(s => {
            const n = document.createElement("div");
            n.setAttribute("class", "call-tag-dialog-item"), n.setAttribute("data-set-id", s.id);
            const l = document.createElement("span");
            l.setAttribute("class", "call-tag-dialog-item-name"), l.innerHTML = s.name, n.appendChild(l), n.addEventListener("click", async o => {
                o.stopPropagation(), this.updateActiveCustomTagElm(n), this.isPointSearchMode ? await this.matchSetCustomTag(s) : await this.onceSetCustomTag(s), this.exitCustomTagDialog()
            }), a.appendChild(n)
        }), e.appendChild(a), this.pcElms.containerDialogElm.appendChild(e), this.pcElms.customTagDialogElms[t.prefix] = e
    }

    showCustomTagDialog(t, e) {
        this.exitCustomTagDialog(), this.exitPointDialog(), this.isPointSearchMode = !!e, this.pcElms.customTagDialogTagKey = t;
        const i = this.pcElms.customTagDialogElms[t], a = i.children[1];
        let s = null;
        Array.prototype.forEach.call(a.children, n => {
            const l = n.getAttribute("data-set-id"), o = e && e.every(r => r.id !== l);
            !s && !o && (s = n), y(n, "tag-no-match", o)
        }), s !== null && this.updateActiveCustomTagElm(s), f(i, !0), this.debounceEvents.dialogMoveToRange(i), i.children[1].scrollTop = 0
    }

    updateActiveCustomTagElm(t, e = !1) {
        if (this.pcElms.customTagDialogActiveElm && y(this.pcElms.customTagDialogActiveElm, "call-tag-dialog-item-active"), this.pcElms.customTagDialogActiveElm = t, t && (y(t, "call-tag-dialog-item-active", !0), e)) {
            const i = this.pcElms.customTagDialogElms[this.pcElms.customTagDialogTagKey].children[1],
                a = Array.prototype.filter.call(i.children, r => r.className.indexOf("tag-no-match") === -1),
                s = t.clientHeight, n = Array.prototype.indexOf.call(a, t),
                l = Math.ceil(Math.floor(i.clientHeight / s) / 2), o = n + 1 - l;
            o > 0 ? i.scrollTop = o * s : i.scrollTop = 0
        }
    }

    exitCustomTagDialog() {
        this.updateActiveCustomTagElm();
        for (const t in this.pcElms.customTagDialogElms) f(this.pcElms.customTagDialogElms[t])
    }

    async showPCCustomTagDialog(t) {
        !this.needDialog || this.asyncMatch || (this.insertText(t), window.removeEventListener("click", this.winClick), this.showCustomTagDialog(t), await w(50), window.addEventListener("click", this.winClick))
    }

    hasH5() {
        this.h5Elms.dialogElm = document.createElement("div"), this.h5Elms.dialogElm.setAttribute("class", "call-user-popup"), this.h5Elms.dialogElm.innerHTML = `
      <div class="call-user-popup-main">
        <div class="call-user-popup-header">
            <span class="popup-show">收起</span>
            <span class="popup-title">选择提醒的人</span>
            <span class="popup-check">确定</span>
        </div>
        <div class="call-user-popup-search">
            ${z}
            <input class="call-user-popup-search-input"
                   placeholder="搜索人员名称"
                   type="text">
        </div>
        <div class="call-user-popup-body"></div>
      </div>
    `;
        const t = async () => {
            this.h5Elms.dialogElm.className = this.h5Elms.dialogElm.className.replace(/ chat-view-show/g, " chat-view-hidden"), this.h5Elms.dialogSearchElm.value = "", await w(260), f(this.h5Elms.dialogElm), y(document.body, "disable-scroll"), this.asyncMatch && this.updateUserList([]), this.chat.restCursorPos(this.chat.vnode, this.chat.cursorIndex), this.chat.viewIntoPoint()
        };
        this.h5Elms.dialogElm.onclick = t;
        const e = this.h5Elms.dialogElm.querySelector(".call-user-popup-main");
        e.onclick = i => {
            i.stopPropagation()
        }, this.h5Elms.dialogShowElm = this.h5Elms.dialogElm.querySelector(".popup-show"), this.h5Elms.dialogShowElm.onclick = t, this.h5Elms.dialogCheckElm = this.h5Elms.dialogElm.querySelector(".popup-check"), this.h5Elms.dialogCheckElm.onclick = async () => {
            if (this.h5Elms.dialogCheckElm.className.indexOf("disabled") !== -1) return;
            const i = this.h5Elms.dialogElm.querySelectorAll(".user-popup-check-item-check") || [];
            if (i.length === 0) {
                await t();
                return
            }
            if (Array.prototype.some.call(i, n => n.getAttribute("data-set-id") === "isALL")) {
                await this.onceSetTag({
                    [this.userProps.id]: "isALL",
                    [this.userProps.name]: this.reviseLabels.callEveryLabel
                }), await t();
                return
            }
            const a = Array.prototype.map.call(i, n => n.getAttribute("data-set-id")),
                s = this.targetUserList.filter(n => a.indexOf(String(n[this.userProps.id])) !== -1);
            await this.batchSetTag(s), await t()
        }, this.h5Elms.dialogMainElm = this.h5Elms.dialogElm.querySelector(".call-user-popup-body"), this.updateUserList(), this.h5Elms.dialogEmptyElm = document.createElement("div"), this.h5Elms.dialogEmptyElm.className = "call-user-popup-empty", this.h5Elms.dialogEmptyElm.innerHTML = `
        ${j}
        <span class="empty-label">${this.reviseLabels.searchEmptyLabel}</span>
    `, f(this.h5Elms.dialogEmptyElm), e.appendChild(this.h5Elms.dialogEmptyElm), this.asyncMatch && (this.h5Elms.dialogLoadingElm = document.createElement("div"), this.h5Elms.dialogLoadingElm.className = "call-user-popup-loading", this.h5Elms.dialogLoadingElm.innerHTML = U, f(this.h5Elms.dialogLoadingElm), e.appendChild(this.h5Elms.dialogLoadingElm)), this.h5Elms.dialogSearchElm = this.h5Elms.dialogElm.querySelector(".call-user-popup-search-input"), this.h5Elms.dialogSearchElm.oninput = P(i => {
            const a = String(i.target.value || "").replace(/'/g, "");
            if (this.asyncMatch) {
                this.matchKey++;
                const n = this.matchKey;
                this.updateUserList([]), f(this.h5Elms.dialogLoadingElm, !0), f(this.h5Elms.dialogEmptyElm);
                const l = this.triggerChatEvent("atMatch", a);
                l && l instanceof Promise && l.then(o => {
                    if (n === this.matchKey) {
                        if (f(this.h5Elms.dialogLoadingElm), !o || o.length <= 0) {
                            f(this.h5Elms.dialogEmptyElm, !0, "flex");
                            return
                        }
                        this.updateUserList(o)
                    }
                });
                return
            }
            const s = [];
            Array.prototype.forEach.call(this.h5Elms.dialogMainElm.children, n => {
                if (!a) {
                    f(n, !0, "flex"), s.push(n);
                    return
                }
                const l = n.getAttribute("data-set-name") || "", o = n.getAttribute("data-set-pinyin") || "";
                R(l, o, a) ? (f(n, !0, "flex"), s.push(n)) : f(n)
            }), f(this.h5Elms.dialogEmptyElm, !s.length, "flex")
        }, 200), f(this.h5Elms.dialogElm), document.body.appendChild(this.h5Elms.dialogElm)
    }

    updateH5User() {
        this.h5Elms.dialogMainElm.innerHTML = "";
        const t = this.userList && this.userList.length > 0, e = document.createDocumentFragment(),
            i = document.createElement("span");
        if (i.innerHTML = `
        <input type="checkbox" value>
        <span class="user-popup-check-item-inner"></span>
    `, t) {
            const a = document.createElement("div");
            this.needCallEvery && (a.setAttribute("class", "call-user-popup-item"), a.setAttribute("data-set-id", "isALL"), a.innerHTML = `
          <span class="call-user-dialog-item-sculpture">
            <span style="transform: scale(0.75)">@</span>
          </span>
          <span class="call-user-dialog-item-name">${this.reviseLabels.callEveryLabel}(${this.userList.length})</span>
      `, a.appendChild(i.cloneNode(!0)), a.onclick = () => {
                const s = a.className.indexOf("user-popup-check-item-check") === -1;
                Array.prototype.forEach.call(this.h5Elms.dialogMainElm.children, n => {
                    y(n, "user-popup-check-item-check", s)
                }), y(this.h5Elms.dialogCheckElm, "disabled", !s)
            }, e.appendChild(a)), this.userList.forEach((s, n) => {
                const l = document.createElement("div");
                l.setAttribute("class", "call-user-popup-item"), l.setAttribute("data-set-id", s.id), l.setAttribute("data-set-name", s.name), l.setAttribute("data-set-pinyin", s.pinyin || ""), this.getUserHtmlTemplate(l, s), l.appendChild(i.cloneNode(!0)), e.appendChild(l), l.onclick = o => {
                    const r = l.className.indexOf("user-popup-check-item-check") === -1;
                    y(l, "user-popup-check-item-check", r);
                    const d = Array.prototype.every.call(this.h5Elms.dialogMainElm.children, c => c.className.indexOf("user-popup-check-item-check") !== -1 || c.getAttribute("data-set-id") === "isALL");
                    y(a, "user-popup-check-item-check", d);
                    const h = Array.prototype.some.call(this.h5Elms.dialogMainElm.children, c => c.className.indexOf("user-popup-check-item-check") !== -1);
                    y(this.h5Elms.dialogCheckElm, "disabled", !h)
                }
            })
        }
        this.h5Elms.dialogMainElm.appendChild(e)
    }

    showH5Dialog() {
        this.richText && this.richText.blur(), Array.prototype.forEach.call(this.h5Elms.dialogMainElm.children, t => {
            t.style.display = "", y(t, "user-popup-check-item-check")
        }), y(this.h5Elms.dialogCheckElm, "disabled", !0), f(this.h5Elms.dialogElm, !0), y(document.body, "disable-scroll", !0), this.asyncMatch && f(this.h5Elms.dialogEmptyElm, !0, "flex"), this.h5Elms.dialogMainElm.scrollTop = 0, this.isExternalCallPopup = !0
    }

    updateConfig(t = {}) {
        const {
            copyType: e,
            userProps: i,
            userList: a,
            uploadImage: s,
            needCallEvery: n,
            placeholder: l,
            maxLength: o,
            needCallSpace: r,
            wrapKeyFun: d,
            sendKeyFun: h,
            customTrigger: c
        } = t;
        e && (this.copyType = e), i && (this.userProps = Object.assign({}, {
            id: "id",
            name: "name",
            avatar: "avatar",
            pinyin: "pinyin"
        }, i)), s && (this.uploadImage = s), l && (this.placeholderElm.innerText = l), o !== void 0 && (this.maxLength = o, this.ruleMaxLength()), (this.asyncMatch || n !== void 0 || a) && (this.needCallEvery = this.asyncMatch ? !1 : O(n), this.updateUserList(this.asyncMatch ? [] : a)), r !== void 0 && this.chat.setCallSpace(O(r)), d && (this.wrapKeyFun = d), h && (this.sendKeyFun = h), this.needDialog && c && this.deviceInfo.isPc && this.bindCustomTrigger(c)
    }

    enterSend() {
        this.triggerChatEvent("enterSend")
    }

    insertHtml(t) {
        const e = document.createElement("span");
        e.innerHTML = t, e.className = "chat-set-html";
        const i = this.chat.createNewDom(e);
        return this.chat.replaceRegContent(i, !1), this.richTextInput(), i
    }

    insertInsideHtml(t) {
        let e = document.createElement("div");
        if (e.innerHTML = t, !e.children.length) return;
        const i = this.chat.vnode, a = this.chat.getWrapNode(i);
        if (e.children.length === 1) this.chat.gridMerge(a, e.children[0], !0); else {
            this.chat.gridMerge(a, e.children[0], !0);
            const s = Array.prototype.slice.call(e.children, 1);
            let n = a;
            Array.prototype.forEach.call(s, (l, o) => {
                if (n.parentElement ? this.richText.insertBefore(l, n.nextElementSibling) : this.richText.appendChild(l), n = l, o === s.length - 1) {
                    const d = l.childNodes[l.childNodes.length - 1].childNodes[0].childNodes[0];
                    this.chat.restCursorPos(d, d.textContent.length)
                }
            })
        }
        e = null, this.richTextInput()
    }

    insertText(t) {
        var d, h;
        if (!t) return;
        const e = new RegExp(`${this.chat.ZERO_WIDTH_KEY}${this.chat.VOID_KEY}`, "ig"), i = t.replace(e, "");
        if (!i) return;
        let a, s = 0;
        const n = this.chat.vnode;
        n && n.parentElement && n.parentElement.getAttribute("data-set-richType") === "richInput" ? (a = n.parentElement, s = n.textContent === this.chat.VOID_KEY ? 1 : this.chat.cursorLeft) : (a = (h = (d = this.richText) == null ? void 0 : d.lastChild) == null ? void 0 : h.lastChild, s = a.childNodes[0].textContent.length);
        const l = c => {
            const u = a.childNodes[0], p = u.textContent.split("");
            return p.splice(s, 0, c), u.textContent = p.join("").replace(new RegExp(this.chat.VOID_KEY, "g"), () => (s--, "")), a.setAttribute("data-set-empty", "false"), a.childNodes[1] && a.removeChild(a.childNodes[1]), this.chat.restCursorPos(u, s + c.length), a.parentElement.parentElement
        }, o = (c, u = !1, p) => {
            const g = this.chat.getGridElm(), x = g.childNodes[0].childNodes[0];
            let T = 1;
            return c && (x.innerHTML = c, x.setAttribute("data-set-empty", "false"), T = c.length), p.nextSibling ? this.richText.insertBefore(g, p.nextSibling) : this.richText.appendChild(g), u && this.chat.restCursorPos(x.childNodes[0], T), g
        }, r = i.split(`
`);
        if (r.length > 1) {
            let c;
            r.forEach((u, p) => {
                p === 0 ? c = l(u) : c = o(u, p === r.length - 1, c)
            })
        } else l(i);
        this.richTextInput()
    }

    getCallUserList() {
        const t = this.richText.querySelectorAll(".at-user");
        if (t && t.length > 0) {
            const e = Array.prototype.map.call(t, i => i.dataset.userId);
            return _(this.targetUserList, e, this.userProps.id)
        } else return []
    }

    getCallUserTagList() {
        const t = this.richText.querySelectorAll(".at-user");
        if (t && t.length > 0) {
            const e = [];
            return Array.prototype.forEach.call(t, i => {
                e.some(a => a[this.userProps.id] === i.dataset.userId) || e.push({
                    [this.userProps.id]: i.dataset.userId,
                    [this.userProps.name]: i.innerText.slice(1)
                })
            }), e
        } else return []
    }

    getCustomTagList() {
        const t = Object.keys(this.customTags), e = {}, i = this.richText.querySelectorAll(".at-tag");
        return t.forEach(a => {
            let s = Array.prototype.filter.call(i, n => n.getAttribute("data-set-prefix") === String(a)).map(n => n.getAttribute("data-tag-id"));
            s = s.filter((n, l) => s.indexOf(n) === l), e[a] = _(this.customTags[a], s, "id")
        }), e
    }

    clear(t) {
        this.chat.textInnerHtmlInit(!0, t);
        const e = {html: this.richText.innerHTML, gridIndex: 0, markIndex: 0, cursorIndex: this.chat.cursorIndex};
        this.undoHistory = [e], this.redoHistory = [], this.richTextInput(!1)
    }

    isEmpty(t = !1) {
        if ((this.richText.querySelectorAll(".chat-tag") || []).length > 0) return !1;
        const i = new RegExp(`^(${this.chat.ZERO_WIDTH_KEY}|<br>|${this.chat.VOID_KEY})+$`),
            a = this.richText.querySelectorAll(".chat-grid-input") || [];
        return t ? Array.prototype.every.call(a, s => !s.innerHTML || !s.innerText || !s.innerText.trim() || i.test(s.innerHTML)) : Array.prototype.every.call(a, s => !s.innerHTML || !s.innerText || i.test(s.innerHTML))
    }

    showPlaceholder() {
        f(this.placeholderElm, this.isEmpty())
    }

    getReduceNode(t = {}) {
        const e = Object.assign({}, {
                needUserId: !1,
                needTagId: !1,
                wrapClassName: void 0,
                rowClassName: void 0,
                imgToText: !1,
                identifyLink: !1
            }, t), i = /(https?|http|ftp|file):\/\/[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]/g,
            s = this.richText.cloneNode(!0).querySelectorAll(".chat-grid-wrap") || [],
            n = document.createElement("div");
        return e.wrapClassName && (n.className = e.wrapClassName), Array.prototype.forEach.call(s, (l, o) => {
            const r = l.querySelectorAll(".chat-stat") || [], d = document.createElement("p");
            e.rowClassName && (d.className = e.rowClassName), Array.prototype.forEach.call(r, h => {
                this.chat.getNodeEmpty(h) || (h.removeAttribute("data-set-richType"), h.removeAttribute("contenteditable"), h.removeAttribute("data-set-empty"), e.needUserId || h.removeAttribute("data-user-id"), e.needTagId || h.removeAttribute("data-tag-id"), e.imgToText && h.firstChild && h.firstChild.tagName === "IMG" && (h.className += " img-to-text", h.innerHTML = `[${h.firstChild.getAttribute("data-img-text") || "元素data-img-text未定义"}]`), e.identifyLink && h.className.indexOf("chat-grid-input") !== -1 && (h.innerHTML = h.innerHTML.replace(i, c => `<a class="chat-grid-link" href="${c}" target="_blank">${c}</a>`)), d.appendChild(h))
            }), d.innerHTML || (d.innerHTML = "<br>"), n.appendChild(d)
        }), n
    }

    getText(t = {}) {
        let e = "";
        const i = this.getReduceNode(t);
        return Array.prototype.forEach.call(i.children, (a, s) => {
            e = e + (s > 0 ? `
` : "") + a.textContent
        }), e
    }

    getHtml(t = {}) {
        return this.getReduceNode(t).innerHTML
    }

    dispose() {
        const t = this.richText.parentElement;
        if (t && (t.removeChild(this.richText), t.removeChild(this.placeholderElm)), this.needDialog) if (this.deviceInfo.isPc) {
            const e = this.pcElms.containerDialogElm.parentElement;
            e && e.removeChild(this.pcElms.containerDialogElm), window.removeEventListener("click", this.winClick), window.removeEventListener("keydown", this.winKeydown)
        } else document.body.removeChild(this.h5Elms.dialogElm);
        for (const e in this) delete this[e];
        Object.setPrototypeOf(this, Object)
    }

    async setUserTag(t) {
        const e = this.chat.createChatTagElm({
            id: t[this.userProps.id],
            name: t[this.userProps.name]
        }, "@", "at-user", "user-id");
        this.chat.replaceRegContent(e, !1), await this.richTextInput(), this.triggerChatEvent("atCheck", [t])
    }

    async onceSetTag(t) {
        await this.chat.onceCall({
            id: t[this.userProps.id],
            name: t[this.userProps.name]
        }), await this.richTextInput(), this.triggerChatEvent("atCheck", [t])
    }

    async batchSetTag(t) {
        const e = [];
        for (let i = 0; i <= t.length - 1;) e.push({id: t[i][this.userProps.id], name: t[i][this.userProps.name]}), i++;
        await this.chat.batchReplaceRegContent(e, !this.isExternalCallPopup), await this.richTextInput(), this.triggerChatEvent("atCheck", t)
    }

    async matchSetTag(t) {
        await this.chat.onceSearchCall(t, this.startOpenIndex), await this.richTextInput(), this.triggerChatEvent("atCheck", [t])
    }

    async onceSetCustomTag(t) {
        await this.chat.onceCustomCall(t, !0, this.pcElms.customTagDialogTagKey), await this.richTextInput(), this.triggerChatEvent("tagCheck", t)
    }

    async matchSetCustomTag(t) {
        await this.chat.onceCustomCall(t, this.startOpenIndex, this.pcElms.customTagDialogTagKey), await this.richTextInput(), this.triggerChatEvent("tagCheck", t)
    }

    disabled() {
        this.richText.setAttribute("contenteditable", "false"), y(this.richText, "chat-rich-text-disabled", !0)
    }

    enable() {
        this.richText.setAttribute("contenteditable", "true"), this.richText.className = this.richText.className.replace(/ chat-rich-text-disabled/g, ""), this.chat.setRangeLastText()
    }

    async undo() {
        if (!this.doOverHistory || !this.undoHistory || this.undoHistory.length <= 1) return;
        const t = this.undoHistory[this.undoHistory.length - 2], e = this.undoHistory[this.undoHistory.length - 1];
        this.redoHistory.push(e), this.undoHistory.pop(), await this.setChatHistory(t)
    }

    async redo() {
        if (!this.doOverHistory || !this.redoHistory || this.redoHistory.length < 1) return;
        const t = this.redoHistory[this.redoHistory.length - 1];
        this.redoHistory.pop(), this.undoHistory.push(t), await this.setChatHistory(t)
    }

    ruleMaxLength() {
        if (this.isEmpty() || this.maxLength === void 0) {
            this.textLength = 0;
            return
        }
        let t = 0, e = 0;
        const i = [];
        Array.prototype.some.call(this.richText.children, (s, n) => {
            const {nodeInfos: l, nodeTextLength: o} = this.getGirdNodeTextInfo(s);
            if (t += o, i.push(l), e = n, t >= this.maxLength) return !0
        });
        const a = [];
        Array.prototype.forEach.call(this.richText.children, (s, n) => {
            n > e && a.push(s)
        }), a.forEach(s => this.richText.removeChild(s)), this.deepDelGirdText(i, t)
    }

    getGirdNodeTextInfo(t) {
        const e = [];
        let i = 0;
        if (t.children.length === 1 && t !== t.parentElement.children[0]) {
            const a = t.children[0], s = (a.textContent || "").replace(new RegExp(this.chat.VOID_KEY, "g"), "");
            i += s.length || 1, e[0] = {node: a, textLength: s.length || 1, type: "richMark"}
        } else Array.prototype.forEach.call(t.children, (a, s) => {
            if (a.getAttribute("data-set-richType") === "richMark") {
                const l = (a.textContent || "").replace(new RegExp(this.chat.VOID_KEY, "g"), "");
                i += l.length, e[s] = {node: a, textLength: l.length, type: "richMark"}
            } else {
                const l = (a.textContent || "").replace(new RegExp(this.chat.VOID_KEY, "g"), "");
                i += l.length || 1, e[s] = {node: a, textLength: l.length || 1, type: "chatTag"}
            }
        });
        return {nodeInfos: e, nodeTextLength: i}
    }

    deepDelGirdText(t, e) {
        if (e > this.maxLength) {
            const i = t[t.length - 1];
            t.pop(), this.deepDelNode(i, t, e)
        } else this.textLength = e
    }

    deepDelNode(t, e, i) {
        const a = t[0].node.parentElement;
        if (i > this.maxLength) {
            let s = i - this.maxLength, n = t[t.length - 1];
            if (n.type === "richMark") if (n.textLength === 0 || s >= n.textLength) a.removeChild(n.node), t.pop(), s = s - n.textLength, n = t[t.length - 1], a.removeChild(n.node), t.pop(), s = s - n.textLength; else {
                const l = n.node.childNodes[0];
                l.textContent = l.textContent.slice(0, n.textLength - s), l.textContent === 0 && (l.setAttribute("data-set-empty", "true"), l.innerHTML = `${this.chat.VOID_KEY}<br>`), s = 0
            } else a.removeChild(n.node), t.pop(), s = s - n.textLength;
            s > 0 ? t.length > 0 ? this.deepDelNode(t, e, s + this.maxLength) : (this.richText.appendChild(a), this.deepDelGirdText(e, s + this.maxLength)) : (this.textLength = this.maxLength + s, this.enable())
        }
    }

    async setChatHistory(t) {
        this.doOverHistory = !1;
        const {html: e, gridIndex: i, markIndex: a, cursorIndex: s} = t;
        this.richText.innerHTML = e;
        const n = this.richText.childNodes[i].childNodes[a].childNodes[0].childNodes[0];
        this.chat.restCursorPos(n, s), await this.richTextInput(), this.doOverHistory = !0
    }

    revisePCPointDialogLabel(t = {}) {
        if (!this.needDialog) return;
        const e = Object.assign({}, {title: "群成员", checkLabel: "多选", emptyLabel: "暂无数据"}, t);
        this.pcElms.pointDialogElm.querySelector(".call-user-dialog-header-title").innerText = e.title, this.pcElms.pointDialogCheckElm.innerText = e.checkLabel, this.pcElms.pointDialogEmptyElm && (this.pcElms.pointDialogEmptyElm.children[1].innerText = e.emptyLabel)
    }

    revisePCCheckDialogLabel(t = {}) {
        if (!this.needDialog || this.asyncMatch) return;
        const e = Object.assign({}, {
            title: "选择要@的人",
            searchPlaceholder: "搜素人员名称",
            searchEmptyLabel: "没有匹配到任何结果",
            userTagTitle: "研讨成员列表",
            checkAllLabel: "全选",
            checkEmptyLabel: "请选择需要@的成员",
            confirmLabel: "确定",
            cancelLabel: "取消"
        }, t);
        this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-container-header").children[0].innerText = e.title, this.pcElms.checkDialogSearchInputElm.setAttribute("placeholder", e.searchPlaceholder || ""), this.reviseLabels.searchEmptyLabel = e.searchEmptyLabel || "", this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-search-empty").innerText = this.reviseLabels.searchEmptyLabel, this.reviseLabels.checkEmptyLabel = e.checkEmptyLabel || "", this.pcElms.checkDialogElm.querySelector(".checkbox-dialog-right-box-title").innerText = e.userTagTitle, this.reviseLabels.checkAllLabel = e.checkAllLabel || "", this.pcElms.checkDialogUsersElm.children[0].children[2].innerText = this.reviseLabels.checkAllLabel, this.pcElms.checkDialogElm.querySelector(".btn-submit").innerText = e.confirmLabel, this.pcElms.checkDialogElm.querySelector(".btn-close").innerText = e.cancelLabel
    }

    reviseH5DialogLabel(t) {
        if (!this.needDialog) return;
        const e = Object.assign({}, {
            title: "选择提醒的人",
            searchPlaceholder: "搜素人员名称",
            searchEmptyLabel: "没有匹配到任何结果",
            confirmLabel: "确定",
            cancelLabel: "收起"
        }, t);
        this.h5Elms.dialogElm.querySelector(".popup-title").innerText = e.title, this.h5Elms.dialogSearchElm.setAttribute("placeholder", e.searchPlaceholder || ""), this.reviseLabels.searchEmptyLabel = e.searchEmptyLabel || "", this.h5Elms.dialogEmptyElm.children[1].innerText = this.reviseLabels.searchEmptyLabel, this.h5Elms.dialogCheckElm.innerText = e.confirmLabel, this.h5Elms.dialogShowElm.innerText = e.cancelLabel
    }

    reverseAnalysis(t, e) {
        const i = document.createElement("div");
        i.innerHTML = t;
        const a = i.children;
        Array.prototype.forEach.call(a, s => {
            s.className = "chat-grid-wrap", s.setAttribute("data-set-richType", "richBox");
            const n = s.children, l = {}, o = [];
            Array.prototype.forEach.call(n, (h, c) => {
                if (h.className.indexOf("chat-grid-input") !== -1) {
                    const g = h.innerText;
                    h.className = "", h.setAttribute("data-set-richType", "richMark"), h.innerHTML = `<span class="chat-grid-input chat-stat" data-set-richType="richInput" data-set-empty="false">${g}</span>`;
                    return
                }
                if (h.tagName === "BR") {
                    const g = this.chat.getGridElm(!0);
                    s.removeChild(h), s.appendChild(g);
                    return
                }
                const u = h.cloneNode(!0);
                u.setAttribute("contenteditable", "false");
                const p = document.createElement("span");
                p.className = "chat-tag", p.setAttribute("contenteditable", "false"), p.setAttribute("data-set-richType", "chatTag"), p.appendChild(u), l[c] = p, c !== n.length - 1 ? n[c + 1].className.indexOf("chat-grid-input") === -1 && o.push(c) : o.push(c), c === 0 && o.push(-1)
            });
            for (const h in l) {
                const c = Number(h);
                c === n.length - 1 ? (s.removeChild(n[c]), s.appendChild(l[h])) : (s.insertBefore(l[h], n[c + 1]), s.removeChild(n[c]))
            }
            const r = [], d = s.children;
            o.forEach(h => {
                h === d.length - 1 ? r.push("isEnd") : r.push(d[h + 1])
            }), r.forEach(h => {
                const c = this.chat.getGridElm(!0);
                if (h === "isEnd") s.appendChild(c); else {
                    const u = c.children[0];
                    u.removeChild(u.childNodes[1]), s.insertBefore(c, h)
                }
            })
        }), e ? (this.enable(), this.insertInsideHtml(i.innerHTML)) : (this.richText.innerHTML = i.innerHTML, this.enable(), this.richTextInput())
    }

    addEventListener(t, e) {
        this.chatEventModule[t].push(e)
    }

    removeEventListener(t, e) {
        const i = this.chatEventModule[t], a = i.indexOf(e);
        a !== -1 && i.splice(a, 1)
    }

    triggerChatEvent(t, ...e) {
        let i;
        return this.chatEventModule[t].forEach(a => {
            a && (i ? a(...e) : i = a(...e))
        }), i
    }

    ruleChatEvent(t, e, ...i) {
        (this.triggerChatEvent(e, ...i) + "").toUpperCase() !== "PREVENT" && (t && t.bind(this)(), t = null)
    }
}

if (!window) throw new Error("非web环境！");
window.console && window.console.log && console.log(" %c ".concat("ChatArea", " %c v5.1.2 "), "background: #269AFF; color: #FFFFFF; padding: 4px 0; border-radius: 4px 0px 0px 4px; font-style: italic;", "background: #FFFFFF; color: #269AFF; padding: 2px 0; border-radius: 0px 4px 4px 0px; font-style: italic; border: 2px solid #269AFF;");
window.ChatArea = J;
